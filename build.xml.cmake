<project name="LibProteomicSvg" default="compile"
	basedir=".">
	<description>
		LibProteomicSvg ant build file
	</description>

	<!-- set global properties for this build -->

	<property name="usr.dir" location="@CMAKE_INSTALL_PREFIX@" />
	<property name="sys.dir" location="/usr/share/java" />
	<property name="share.dest" location="@CMAKE_BINARY_DIR@/share" />
	<property name="share" location="@CMAKE_SOURCE_DIR@/share" />
	<property name="resources" location="src/resources" />
	<property name="src" location="@CMAKE_SOURCE_DIR@/src" />
	<property name="lib" location="@CMAKE_SOURCE_DIR@/lib" />
	<property name="build" location="@CMAKE_BINARY_DIR@/build" />
	<property name="dist" location="@CMAKE_BINARY_DIR@/dist" />
	<property name="jnlpDir" location="@CMAKE_BINARY_DIR@/jnlp" />
	<property name="confDir" location="conf" />
	<property name="docDir" location="doc" />


	<!-- sign informations -->
	<property name="keystore" value="/home/langella/.keystore" />
	<property name="alias" value="your_alias_in_keystore" />
	<property name="storepass" value="your_password_for_storepass" />
	<property name="keypass" value="your_password_for_keypass" />


	<target name="clean">
		<!-- Create the time stamp -->
		<delete dir="${build}" />
		<delete dir="${dist}" />
	</target>

	<target name="init">
		<!-- Create the time stamp -->
		<tstamp />
		<mkdir dir="${build}" />
		<mkdir dir="${dist}" />
	</target>



	<path id="classpathDebian">
		<!--xml-apis-ext.jar xml-apis.jar xercesImpl.jar xalan2.jar swt.jar batik.jar 
			liblog4j1.2-java -->
		<fileset dir="${sys.dir}"
			includes="log4j-1.2.jar" />
	</path>


	<target name="compileDebian" depends="init"
		description="compile the source and put in build directory">
		<!-- Compile the java code from ${src} into ${build} -->
		<javac srcdir="${src}" destdir="${build}" target="1.6" source="1.6"
			classpathref="classpathDebian" includeantruntime="false">
			<compilerarg line="-encoding utf-8" />
			<!-- <compilerarg line="-encoding utf-8 -Xlint" /> -->
		</javac>
	</target>



	<path id="classpath">
		<fileset dir="${lib}" includes="**/*.jar" />
		<pathelement location="${lib}" />
	</path>


	<target name="compile" depends="init"
		description="compile the source and put in build directory">
		<!-- Compile the java code from ${src} into ${build} -->
		<javac srcdir="${src}" destdir="${build}" target="1.6" source="1.6"
			classpathref="classpath" includeantruntime="false">
			<compilerarg line="-encoding utf-8" />
		</javac>
	</target>

	<target name="jar" depends="compile"
		description="generates the jar file of the build directory">
		<mkdir dir="${build}/conf" />
		<copy todir="${build}/conf" overwrite="true">
			<fileset dir="${confDir}" includes="*" />
		</copy>

		<jar jarfile="${dist}/XtandemPipeline.jar" basedir="${build}">
			<!--<fileset file=".classpath" /> -->
			<manifest>
				<attribute name="Class-Path"
					value="${lib}/log4j-1.2.jar" />
			</manifest>
		</jar>


		<!-- cd bin; jar -cf client.jar; -->
		<!-- <jar jarfile="client.jar" basedir="bin"/> -->
	</target>

	<target name="run" depends="jar">
		<java fork="true" maxmemory="3000m" classpathref="classpath"
			jar="${dist}/XtandemPipeline.jar">
			<classpath>
				<pathelement
					location="${build}/fr/inra/pappso/xtandempipeline/XtandemPipelineMain.class" />
					
					
			</classpath>
		</java>
	</target>




	<target name="debianBuild" depends="compileDebian"
		description="generates the jar file for Debian">


		<mkdir dir="${build}/conf" />
		<copy todir="${build}/conf" overwrite="true">
			<fileset dir="${confDir}" includes="*" />
		</copy>
		<copy todir="${build}/fr" overwrite="true">
			<fileset dir="${src}/fr" includes="**" />
		</copy>

		<jar jarfile="${share.dest}/java/libproteomicsvg.jar"
			basedir="${build}">
			<!--<fileset dir="${confDir}" /> -->
			<!--<fileset file=".classpath" /> -->
			<manifest>
				<attribute name="Class-Path"
					value="${sys.dir}/commons-logging.jar ${sys.dir}/log4j-1.2.jar" />
			</manifest>
		</jar>

		<!-- cd bin; jar -cf client.jar; -->
		<!-- <jar jarfile="client.jar" basedir="bin"/> -->
	</target>
</project>