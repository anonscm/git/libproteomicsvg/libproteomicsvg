package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.FragmentationTypeCID;
import fr.inra.moulon.svgutils.svgspectrum.FragmentationTypeETD;
import fr.inra.moulon.svgutils.svgspectrum.SvgAaMod;
import fr.inra.moulon.svgutils.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgPeptideSequence;
import fr.inra.moulon.svgutils.svgspectrum.DynamicSvgSpectrumDocument;
import fr.inra.moulon.xhtmlutils.Html5Document;
import fr.inra.moulon.xhtmlutils.HtmlDocument;
import fr.inra.moulon.xhtmlutils.Xhtml5Document;
import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.XhtmlIonTable.XhtmlIonModificationsTable;
import fr.inra.moulon.xhtmlutils.XhtmlIonTable.XhtmlIonTable;
import fr.inra.moulon.xhtmlutils.elements.XhtmlDiv;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;
import fr.inra.pappso.phospredictor.PhosphoTypes;

public class testXhtmlIonTable {

	public static void main(String[] args) throws IOException {
		/*
		 * // Get a DOMImplementation. DOMImplementation domImpl =
		 * GenericDOMImplementation.getDOMImplementation();
		 * 
		 * // Create an instance of org.w3c.dom.Document. String svgNS =
		 * "http://www.w3.org/2000/svg"; Document document =
		 * domImpl.createDocument(svgNS, "svg", null);
		 * 
		 * // Create an instance of the SVG Generator. SVGGraphics2D
		 * svgGenerator = new SVGGraphics2D(document);
		 * 
		 * // Ask the test to render into the SVG Graphics2D implementation.
		 * TestSVGGen test = new TestSVGGen(); test.paint(svgGenerator);
		 */

		try {

			DynamicSvgSpectrumDocument mon_svg = new DynamicSvgSpectrumDocument(
					800, 600);

			mon_svg.setFitPrecision((float) 0.5); //
			// mon_svg.setCssClass(".classeligne", "stroke:red;");
			Vector<Float> mz = new Vector<Float>(0);
			Vector<Float> intensity = new Vector<Float>(0);

			mz.add((float) (71.04 + 1.00794));
			intensity.add((float) 38045);
			mz.add((float) (81.04 + 1.00794));
			intensity.add((float) 38045);
			mz.add((float) 100.0);
			intensity.add((float) 70000);
			mz.add((float) 110.0);
			intensity.add((float) 60000); //
			// mon_svg.line(10, 20, 100, 50,"classeligne");
			mz.add((float) (142.71));
			intensity.add((float) 1000);
			mz.add((float) 328.0);
			intensity.add((float) 26587);
			mz.add((float) 328.01);
			intensity.add((float) 2658);
			mz.add((float) (921.08));
			intensity.add((float) 5000);

			mz.add((float) 808.04);
			intensity.add((float) 26587);

			mon_svg.setMzIntensity(mz, intensity);

			// mon_svg.setSpectrumRangeMz((float) 98.0, (float) 600.0);

			SvgIon iony1 = new SvgIon();
			iony1.setIonY1();
			iony1.setMz((float) 110.01);
			iony1.setNumber(8);

			SvgIon iona1 = new SvgIon();
			iona1.setIonA1();
			iona1.setMz((float) 99.999);

			SvgIon ionystar1 = new SvgIon();
			ionystar1.setIonYstar1();
			ionystar1.setMz((float) 328.03);
			ionystar1.setNumber(8);

			SvgIon ionyP1 = new SvgIon();
			ionyP1.setIonYP1(1);
			ionyP1.setMz((float) 328.03);
			ionyP1.setNumber(8);

			ArrayList<SvgIon> ion_table = new ArrayList<SvgIon>();
			ion_table.add(iony1);
			ion_table.add(iona1);
			ion_table.add(ionystar1);
			ion_table.add(ionyP1);

			mon_svg.setIonTable(ion_table);

			SvgPeptideSequence mon_peptide = new SvgPeptideSequence("ATGCKARTY");
			SvgAaMod themod = new SvgAaMod((float) 27.656);
			themod.setPsiModAccession("MOD:00397");
			mon_peptide.getAA(1).addModification(10); //
			mon_peptide.get(6).addModification(PhosphoTypes.phosphorylation.getSvgAaMod());
			mon_peptide.getAA(1).addModification(15);
			mon_peptide.getAA(1).addModification(themod);
			mon_peptide.getAA(5).addModification(themod);
			// mon_svg.setSequence(FragmentationTypeCID.getInstance(),
			// mon_peptide);
			// mon_svg.setSequence(FragmentationTypeETD.getInstance(),
			// mon_peptide);
			mon_svg.setSequence(FragmentationTypeCID.getInstance(), mon_peptide);

			// mon_svg.setSequence("ATGCKARTY");
			mon_svg.setTitle("super beau spectreb");

			mon_svg.setSpectrumRangeMz(0, 1100);

			// mon_svg.toOutputStream(System.out);
			File fichiersvg = new File("test_spectrum.svg");
			mon_svg.toFile(fichiersvg);

			// XhtmlDocument xhtmlDoc = new XhtmlDocument();
			//XhtmlDocument xhtmlDoc = new Xhtml5Document();
			XhtmlDocument xhtmlDoc = new Html5Document();
			// xhtmlDoc.setTitle("test");
			// xhtmlDoc.setWriteDocType(true);

			// XhtmlTable table = xhtmlDoc.getBody().newTable();

			XhtmlDiv div = xhtmlDoc.getBody().newDiv("super cool2");
			XhtmlIonModificationsTable modTable = new XhtmlIonModificationsTable(
					div, mon_svg);
			modTable.setId("modiontable");
			modTable.setCssClass("modificationtable");

			// xhtmlDoc.setCssClass(".modificationtable", "color", "red");
			// xhtmlDoc.setCssClass(".modificationtable", "color", "yellow");
			xhtmlDoc.getBody().newDiv("").includeSvgDocument(mon_svg);
			xhtmlDoc.setOnLoad("initSvg()");
			
			XhtmlIonTable xhtmlIonTable = new XhtmlIonTable(xhtmlDoc.getBody(),
					mon_svg);
			xhtmlIonTable.setId("iontable");
			xhtmlIonTable.setCssClass("iontable");
			File fichierXhtml = new File("test_ion_table.html");
			xhtmlDoc.toFile(fichierXhtml);

			// mon_svg.toString();
			// System.out.println(xhtmlDoc.toString());
			/*
			 * Document doc = SvgGraphics.getDocument();
			 * 
			 * // get the root element (the svg element) Element svgRoot =
			 * SvgGraphics.getRootNode(doc, 400,450);
			 * 
			 * // create the rectangle SvgGraphics.svgRect(doc, svgRoot, 10, 20,
			 * 100, 50,"nomduneclasse"); SvgGraphics.svgLine(doc, svgRoot, 10,
			 * 20, 100, 50,"classeligne"); // /////////////// // Output the XML
			 * 
			 * // Write the DOM document to the file
			 * SvgGraphics.toSystemOut(doc);
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
