package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import fr.inra.moulon.svgutils.svgspectrum.FragmentationTypeCID;
//import fr.inra.moulon.svgutils.svgspectrum.SvgAaModPhospho;
//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgPeptideSequence;
import fr.inra.moulon.svgutils.svgspectrum.SvgSpectrumDocument;
import fr.inra.pappso.phospredictor.PhosCalcScore;
import fr.inra.pappso.phospredictor.PhosPredictor;
import fr.inra.pappso.phospredictor.PhosSpectrum;
import fr.inra.pappso.phospredictor.PhosphoTypes;

public class testPhosPredictor2 {
	private static Logger logger = Logger.getLogger(testPhosPredictor2.class);

	public static void main(String[] args) throws IOException {

		//http://pappso.moulon.inra.fr/protic/proticprod/angular/#/peptide_hits/11266638   => 5
		//http://pappso.moulon.inra.fr/protic/proticprod/web_view/proticport/spectrums/23736140/peaks/dta
		//http://pappso.moulon.inra.fr/protic/proticprod/angular/#/peptide_hits/11266637   => 4
		//http://pappso.moulon.inra.fr/protic/proticprod/web_view/proticport/spectrums/23736140/peaks/dta

		try {
			File log4jPropertyFile = new File("conf"
					+ System.getProperty("file.separator") + "log4j.properties");
			if (log4jPropertyFile.exists()) {
				PropertyConfigurator.configure(log4jPropertyFile
						.getAbsolutePath());
				logger.info("loading log4j proterties from "
						+ log4jPropertyFile.getAbsolutePath());
			}
			SvgPeptideSequence peptide = new SvgPeptideSequence(
					"SRLSSAPAKPVAA");


			peptide.getAA(5).addModification(PhosphoTypes.phosphorylation.getSvgAaMod());
			//peptide.getAA(7).addModification(
			//		PhosphoTypes.dehydratedResidue.getSvgAaMod());

			peptide.setExp_z(2);

			PhosSpectrum spectrum = new PhosSpectrum(new FileInputStream(
					"./doc/samples/spectrum_sample_23736140.dta"));


			logger.debug("spectrum size:" + spectrum.size());
			PhosPredictor predictor = new PhosPredictor(peptide,
					PhosphoTypes.phosphorylation, spectrum);

			TreeSet<PhosCalcScore> results = predictor.computePhosCalc(
					FragmentationTypeCID.getInstance(), (float) 0.02);

			for (PhosCalcScore result : results) {
				SvgPeptideSequence peptideSeq = result.getSvgPeptideSequence();
				logger.debug("results for :" + peptideSeq.getStringWithModifs());
				logger.debug("proba : " + result.getProba());
				logger.debug("score : " + result.getProbaScore());
				logger.debug("possible ions : "
						+ result.getNumberOfPossibleIons());
				logger.debug("assigned ions : "
						+ result.getNumberOfAssignedIons());
			}

			
			
			SvgPeptideSequence peptideb = new SvgPeptideSequence(
					"SRLSSAPAKPVAA");


			peptideb.getAA(4).addModification(
					PhosphoTypes.phosphorylation.getSvgAaMod());
			//peptide.getAA(7).addModification(
			//		PhosphoTypes.dehydratedResidue.getSvgAaMod());

			peptideb.setExp_z(2);


			logger.debug("bspectrum size:" + spectrum.size());
			PhosPredictor predictorb = new PhosPredictor(peptideb,
					PhosphoTypes.phosphorylation, spectrum);

			results = predictorb.computePhosCalc(
					FragmentationTypeCID.getInstance(), (float) 0.02);

			for (PhosCalcScore result : results) {
				SvgPeptideSequence peptideSeq = result.getSvgPeptideSequence();
				logger.debug("bresults for :" + peptideSeq.getStringWithModifs());
				logger.debug("bproba : " + result.getProba());
				logger.debug("bscore : " + result.getProbaScore());
				logger.debug("bpossible ions : "
						+ result.getNumberOfPossibleIons());
				logger.debug("bassigned ions : "
						+ result.getNumberOfAssignedIons());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
