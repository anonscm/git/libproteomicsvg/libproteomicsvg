package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import fr.inra.moulon.svgutils.svgspectrum.FragmentationTypeCID;
//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgPeptideSequence;
import fr.inra.moulon.svgutils.svgspectrum.SvgSpectrumDocument;
import fr.inra.pappso.phospredictor.PhosCalcScore;
import fr.inra.pappso.phospredictor.PhosPredictor;
import fr.inra.pappso.phospredictor.PhosSpectrum;
import fr.inra.pappso.phospredictor.PhosphoTypes;

public class testPhosPredictor {
	private static Logger logger = Logger.getLogger(testPhosPredictor.class);

	public static void main(String[] args) throws IOException {

		try {
			File log4jPropertyFile = new File("conf"
					+ System.getProperty("file.separator") + "log4j.properties");
			if (log4jPropertyFile.exists()) {
				PropertyConfigurator.configure(log4jPropertyFile
						.getAbsolutePath());
				logger.info("loading log4j proterties from "
						+ log4jPropertyFile.getAbsolutePath());
			}
			// http://pappso.inra.fr/protic/proticprod/web_view/index.php?file=spectrum__summary&peptide_id=1084308
			/*
			 * SvgPeptideSequence peptide = new SvgPeptideSequence(
			 * "ANTLAISEGEGSLGR"); peptide.getAA(1).addModification((float)
			 * 32.00); peptide.getAA(12).addModification(
			 * PhosphoTypes.phosphorylation.getSvgAaMod());
			 * peptide.getAA(3).addModification(
			 * PhosphoTypes.phosphorylation.getSvgAaMod());
			 */
			// KDIGS#ES#TEDQAMEDIKQ sample.dta
			//SvgPeptideSequence peptide = new SvgPeptideSequence(
			//		"KDIGSESTEDQAMEDIKQ");
			SvgPeptideSequence peptide = new SvgPeptideSequence(
					"LVSYPSSENLDSSFFQDDEDVQKNEVLR");


			peptide.getAA(3).addModification(
					PhosphoTypes.dehydratedResidue.getSvgAaMod());
			//peptide.getAA(7).addModification(
			//		PhosphoTypes.dehydratedResidue.getSvgAaMod());

			peptide.setExp_z(1);

			PhosSpectrum spectrum = new PhosSpectrum(new FileInputStream(
					"./doc/samples/spectrum_sample.dta"));


			logger.debug("spectrum size:" + spectrum.size());
			PhosPredictor predictor = new PhosPredictor(peptide,
					PhosphoTypes.dehydratedResidue, spectrum);

			TreeSet<PhosCalcScore> results = predictor.computePhosCalc(
					FragmentationTypeCID.getInstance(), (float) 0.4);

			for (PhosCalcScore result : results) {
				SvgPeptideSequence peptideSeq = result.getSvgPeptideSequence();
				logger.debug("results for :" + peptideSeq.getStringWithModifs());
				logger.debug("proba : " + result.getProba());
				logger.debug("score : " + result.getProbaScore());
				logger.debug("possible ions : "
						+ result.getNumberOfPossibleIons());
				logger.debug("assigned ions : "
						+ result.getNumberOfAssignedIons());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
