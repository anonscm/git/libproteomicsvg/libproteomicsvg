package test;

import java.io.File;
import java.io.IOException;

//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.xhtmlutils.Html5Document;
import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlDiv;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlForm;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlFormDiv;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputText;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlLabel;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlOptgroup;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlSelect;
import fr.inra.moulon.xhtmlutils.jquery.XhtmlJqueryShowHide;

public class testXhtmlForm {

	public static void main(String[] args) throws IOException {
		try {

			// XhtmlDocument xhtmlDoc = new XhtmlDocument();
			XhtmlDocument xhtmlDoc = new Html5Document();
			xhtmlDoc.setTitle("test formulaire");
			// xhtmlDoc.setWriteDocType(true);

			// XhtmlTable table = xhtmlDoc.getBody().newTable();
			XhtmlDiv divt = xhtmlDoc.getBody().newDiv("test");

			XhtmlJqueryShowHide show = new XhtmlJqueryShowHide(divt,
					"liste à cacher", null);

			show.getContainer().appendText("contenu caché");

			show.getContainer().addHtmlContent(
					"truc<br/>reto\nur" + "avec des <a>liens</a>");

			XhtmlDiv div = xhtmlDoc.getBody().newDiv("super cool2");

			XhtmlForm form = div.newForm("post", "pouet.php");
			XhtmlFormDiv divF = form.newDiv("Dans le formulaire");
			XhtmlInputText input = divF
					.newInputText("firstname", "olivier", 30);
			XhtmlLabel label = divF.newLabel("Firstname");
			input.setId("firstname");
			input.assigneLabel(label);
			divF.newInputSubmit("ok");

			XhtmlLabel ville = divF.newLabel("ville liste");
			XhtmlSelect liste = ville.newSelect("liste");
			XhtmlOptgroup group = liste.newOptgroup("Antony");

			group.newOption("bien", "Bien").setSelected(true);

			group.newOption("moyen", "Moyen").setSelected(true);

			File fichierXhtml = new File("test_form.html");
			xhtmlDoc.toFile(fichierXhtml);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());
		}

	}
}
