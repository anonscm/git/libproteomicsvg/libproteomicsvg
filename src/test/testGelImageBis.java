package test;

import java.io.File;
import java.io.IOException;
import java.net.URI;

//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svggelimage.SvgGelImageDocument;
import fr.inra.moulon.svgutils.svggelimage.SvgSpot;

public class testGelImageBis {

	public static void main(String[] args) throws IOException {
		/*
		 * // Get a DOMImplementation. DOMImplementation domImpl =
		 * GenericDOMImplementation.getDOMImplementation();
		 * 
		 * // Create an instance of org.w3c.dom.Document. String svgNS =
		 * "http://www.w3.org/2000/svg"; Document document =
		 * domImpl.createDocument(svgNS, "svg", null);
		 * 
		 * // Create an instance of the SVG Generator. SVGGraphics2D
		 * svgGenerator = new SVGGraphics2D(document);
		 * 
		 * // Ask the test to render into the SVG Graphics2D implementation.
		 * TestSVGGen test = new TestSVGGen(); test.paint(svgGenerator);
		 */

		try {
			//SvgDocument SvgDoc = new SvgDocument();

			SvgGelImageDocument mon_gel = new SvgGelImageDocument();
			// File gel_image_file = new File("/home/langella/Desktop/spectra.jpg");
			// mon_gel.embedGelImage(gel_image_file);

			mon_gel.setGelImageUrl(new URI(
					"file:///home/olivier/test.png"), 2979, 2518);

			// mon_gel.setWidth(200);
			
			//mon_gel.setXcenter(900);
			//mon_gel.setYcenter(1500);
			//mon_gel.setXcenter(10);
			//mon_gel.setYcenter(10);
			//mon_gel.setXcenter(10000);
			//mon_gel.setYcenter(101000);
			
			//mon_gel.setWidth((float)1500);
			mon_gel.setHeight((float)1500);
			mon_gel.setZoomRatio((float) 2);
			//mon_gel.setZoomW((float)500, null);
			// mon_svg.toOutputStream(System.out);
			File fichiergelsvg = new File("testgel.svg");

			mon_gel.addSvgSpot(10, 10);
			SvgSpot un_spot = mon_gel.addSvgSpot(50, 50);
			un_spot = mon_gel.addSvgSpot(100, 50);
			un_spot = mon_gel.addSvgSpot(150, 50);
			un_spot = mon_gel.addSvgSpot(200, 50);
			un_spot = mon_gel.addSvgSpot(900, 900);
			un_spot = mon_gel.addSvgSpot(900, 1500);
			un_spot = mon_gel.addSvgSpot(1500, 50);
			un_spot = mon_gel.addSvgSpot(1500, 900);
			un_spot = mon_gel.addSvgSpot(1000, 1000);

			un_spot.setLabel("toto");

			un_spot.setHighLight(true);

			// PrintWriter out = new PrintWriter(new
			// OutputStreamWriter(System.out, UTF8), true);
			// mon_gel.toOutputStream(System.out);
			mon_gel.toFile(fichiergelsvg);
			/*
			 * // Create a JPEGTranscoder and set its quality hint.
			 * JPEGTranscoder t = new JPEGTranscoder();
			 * t.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float(1));
			 * 
			 * // Set the transcoder input and output. TranscoderInput input =
			 * new TranscoderInput(mon_gel.getDocument()); OutputStream ostream
			 * = new FileOutputStream("out.jpg"); TranscoderOutput output = new
			 * TranscoderOutput(ostream);
			 * 
			 * // Perform the transcoding. t.transcode(input, output);
			 * ostream.flush(); ostream.close();
			 * 
			 * javax.imageio.ImageIO.write(mon_gel.getEmbededGelBufferedImage(),
			 * "jpeg", new FileOutputStream("out2.jpg"));
			 */

			/*
			 * SvgSpectrum mon_svg = new SvgSpectrum(800, 600);
			 * mon_svg.setFitPrecision(fitPrecision); //
			 * mon_svg.setCssClass(".classeligne", "stroke:red;");
			 * 
			 * Vector<Float> mz = new Vector<Float>(0); Vector<Float> intensity
			 * = new Vector<Float>(0);
			 * 
			 * mz.add((float) (71.04 + 1.00794)); intensity.add((float) 38045);
			 * mz.add((float) (81.04 + 1.00794)); intensity.add((float) 38045);
			 * mz.add((float) 100.0); intensity.add((float) 70000);
			 * mz.add((float) 110.0); intensity.add((float) 60000); //
			 * mon_svg.line(10, 20, 100, 50,"classeligne"); mz.add((float)
			 * 328.0); intensity.add((float) 26587);
			 * 
			 * mon_svg.setMzIntensity(mz, intensity);
			 */

			// mon_svg.setSpectrumRangeMz((float) 98.0, (float) 600.0);

			/*
			 * SvgIon iony1 = new SvgIon(); iony1.setIonY1();
			 * iony1.setMz((float) 110.01); iony1.setNumber(8);
			 * 
			 * SvgIon iona1 = new SvgIon(); iona1.setIonA1();
			 * iona1.setMz((float) 99.999);
			 * 
			 * SvgIon ionystar1 = new SvgIon(); ionystar1.setIonYstar1();
			 * ionystar1.setMz((float) 328.03); ionystar1.setNumber(8);
			 * 
			 * Vector<SvgIon> ion_table = new Vector<SvgIon>();
			 * ion_table.add(iony1); ion_table.add(iona1);
			 * ion_table.add(ionystar1);
			 * 
			 * mon_svg.setIonTable(ion_table);
			 */
			/*
			 * SvgPeptideSequence mon_peptide = new
			 * SvgPeptideSequence("ATGCKARTY");
			 * mon_peptide.getAA(1).addModification(10); //
			 * mon_peptide.get(1).addModification(10);
			 * mon_svg.setSequence(mon_peptide);
			 * 
			 * // mon_svg.setSequence("ATGCKARTY");
			 * mon_svg.setTitle("super beau spectreb");
			 * 
			 * // mon_svg.toOutputStream(System.out); File fichiersvg = new
			 * File("/home/langella/test.svg"); mon_svg.toFile(fichiersvg);
			 */
			/*
			 * Document doc = SvgGraphics.getDocument();
			 * 
			 * // get the root element (the svg element) Element svgRoot =
			 * SvgGraphics.getRootNode(doc, 400,450);
			 * 
			 * // create the rectangle SvgGraphics.svgRect(doc, svgRoot, 10, 20,
			 * 100, 50,"nomduneclasse"); SvgGraphics.svgLine(doc, svgRoot, 10,
			 * 20, 100, 50,"classeligne"); // /////////////// // Output the XML
			 * 
			 * // Write the DOM document to the file
			 * SvgGraphics.toSystemOut(doc);
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
