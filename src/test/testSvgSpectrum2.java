package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.DynamicSvgSpectrumDocument;
import fr.inra.moulon.svgutils.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgSpectrumDocument;

public class testSvgSpectrum2 {

	public static void main(String[] args) throws IOException {
		/*
		 * // Get a DOMImplementation. DOMImplementation domImpl =
		 * GenericDOMImplementation.getDOMImplementation();
		 * 
		 * // Create an instance of org.w3c.dom.Document. String svgNS =
		 * "http://www.w3.org/2000/svg"; Document document =
		 * domImpl.createDocument(svgNS, "svg", null);
		 * 
		 * // Create an instance of the SVG Generator. SVGGraphics2D
		 * svgGenerator = new SVGGraphics2D(document);
		 * 
		 * // Ask the test to render into the SVG Graphics2D implementation.
		 * TestSVGGen test = new TestSVGGen(); test.paint(svgGenerator);
		 */

		try {

			SvgSpectrumDocument mon_svg = new DynamicSvgSpectrumDocument(800,
					600);
			mon_svg.setFitPrecision((float) 0.5); //
			// mon_svg.setCssClass(".classeligne", "stroke:red;");
			Vector<Float> mz = new Vector<Float>(0);
			Vector<Float> intensity = new Vector<Float>(0);

			mz.add((float) (71.04 + 1.00794));
			intensity.add((float) 38045);
			mz.add((float) (81.04 + 1.00794));
			intensity.add((float) 38045);
			mz.add((float) 100.0);
			intensity.add((float) 70000);
			mz.add((float) 110.0);
			intensity.add((float) 60000); //
			// mon_svg.line(10, 20, 100, 50,"classeligne");
			mz.add((float) 328.0);
			intensity.add((float) 26587);
			mz.add((float) 328.01);
			intensity.add((float) 2658);

			mon_svg.setMzIntensity(mz, intensity);

			// mon_svg.setSpectrumRangeMz((float) 98.0, (float) 600.0);

			SvgIon iony1 = new SvgIon();
			iony1.setIonY1();
			iony1.setMz((float) 110.01);
			iony1.setNumber(8);

			SvgIon iona1 = new SvgIon();
			iona1.setIonA1();
			iona1.setMz((float) 99.999);

			SvgIon ionystar1 = new SvgIon();
			ionystar1.setIonYstar1();
			ionystar1.setMz((float) 328.03);
			ionystar1.setNumber(8);

			ArrayList<SvgIon> ion_table = new ArrayList<SvgIon>();
			ion_table.add(iony1);
			ion_table.add(iona1);
			ion_table.add(ionystar1);

			mon_svg.setIonTable(ion_table);

			/*
			 * SvgPeptideSequence mon_peptide = new
			 * SvgPeptideSequence("ATGCKARTY");
			 * mon_peptide.getAA(1).addModification(10); //
			 * mon_peptide.get(1).addModification(10);
			 * mon_svg.setSequence(mon_peptide);
			 */

			// mon_svg.setSequence("ATGCKARTY");
			mon_svg.setTitle("super beau spectreb");

			// mon_svg.setSpectrumRangeMz(320, 350);

			// mon_svg.toOutputStream(System.out);
			File fichiersvg = new File("test_spectrum2.svg");

			mon_svg.toFile(fichiersvg);

			// mon_svg.toString();

			/*
			 * Document doc = SvgGraphics.getDocument();
			 * 
			 * // get the root element (the svg element) Element svgRoot =
			 * SvgGraphics.getRootNode(doc, 400,450);
			 * 
			 * // create the rectangle SvgGraphics.svgRect(doc, svgRoot, 10, 20,
			 * 100, 50,"nomduneclasse"); SvgGraphics.svgLine(doc, svgRoot, 10,
			 * 20, 100, 50,"classeligne"); // /////////////// // Output the XML
			 * 
			 * // Write the DOM document to the file
			 * SvgGraphics.toSystemOut(doc);
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
