package test;

import java.io.File;
import java.io.IOException;

//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.xhtmlutils.HtmlDocument;
import fr.inra.moulon.xhtmlutils.Xhtml5Document;
import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.xhtmlprotcover.XhtmlSequence;

public class testXhtmlProtCover {

	public static void main(String[] args) throws IOException {

		try {

			//XhtmlDocument xhtmlDoc = new HtmlDocument();
			XhtmlDocument xhtmlDoc = new Xhtml5Document();
			//xhtmlDoc.setTryToProduceHtml(true);
			
			xhtmlDoc.setTitle("test prot cover");
			
			XhtmlSequence xhtmlSequence = new XhtmlSequence(xhtmlDoc.getBody(), "AEAZERTSBVCQSRTBZERTHVXVCQTAZTCRAERTVAZERCQSDQERCZA");
			
			xhtmlSequence.newPeptideCover(2,4, null);
			xhtmlSequence.newPeptideCover(12,14, null);
		
			File fichierXhtml = new File("prot_cover.html");
			xhtmlDoc.toFile(fichierXhtml);

			// mon_svg.toString();
			System.out.println(xhtmlDoc.toString());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
