package test;

import java.io.File;
import java.util.HashSet;

//import main.java.fr.inra.moulon.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.ProteomicSvgDocument;
import fr.inra.moulon.svgutils.elements.SvgCoordXY;
import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.svgprotcover.SvgProtCover;
import fr.inra.moulon.svgutils.svgprotcover.SvgProtein;

public class TestSvgProtCover {

	public static void main(String[] args) {
		/*
		 * // Get a DOMImplementation. DOMImplementation domImpl =
		 * GenericDOMImplementation.getDOMImplementation();
		 * 
		 * // Create an instance of org.w3c.dom.Document. String svgNS =
		 * "http://www.w3.org/2000/svg"; Document document =
		 * domImpl.createDocument(svgNS, "svg", null);
		 * 
		 * // Create an instance of the SVG Generator. SVGGraphics2D
		 * svgGenerator = new SVGGraphics2D(document);
		 * 
		 * // Ask the test to render into the SVG Graphics2D implementation.
		 * TestSVGGen test = new TestSVGGen(); test.paint(svgGenerator);
		 */
		ProteomicSvgDocument svgDoc = new ProteomicSvgDocument(200,150);

		try {
			SvgProtein prot = new SvgProtein("ARTYDECXVDSFHGTHXVNFTYKRHCVSDFGHFHGKEXCV");
			//SvgProtCover protCover = new SvgProtCover(svgDoc, prot, 16/9);
			
			SvgProtCover cover = svgDoc.newSvgProtCover(prot);
			SvgGroup peptide = cover.newPeptideCover(5,15, new String("coucou1"));
			peptide = cover.newPeptideCover(10,20,new String("coucou2"));
			File fichierXhtml = new File("test_prot_cover.svg");
			svgDoc.toFile(fichierXhtml);
			
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		SvgCoordXY coord = new SvgCoordXY(100, 30);
		
		try {
			HashSet<Object> obj = svgDoc.getDataSet(coord);
			
			for (Object s :obj) {
				System.out.println("coord "+coord +" contains "+s);				
			}
		} catch (Exception e) {
			System.out.println("no pattern found in ");

			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
