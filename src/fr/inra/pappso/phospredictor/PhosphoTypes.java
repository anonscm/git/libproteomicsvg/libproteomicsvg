package fr.inra.pappso.phospredictor;

import fr.inra.moulon.svgutils.svgspectrum.SvgAaMod;

public enum PhosphoTypes {
	phosphorylation("MOD:00696"), dehydratedResidue("MOD:00704");

	private SvgAaMod phosphorylationMod;

	private PhosphoTypes(String fileMimeType) {
		if (fileMimeType.equals("MOD:00696")) {
			phosphorylationMod = new SvgAaMod((float) 79.966331);
			phosphorylationMod.setPsiModAccession("MOD:00696");
		}
		if (fileMimeType.equals("MOD:00704")) {
			phosphorylationMod = new SvgAaMod((float) -18.010565);
			phosphorylationMod.setPsiModAccession("MOD:00704");
		}
	}

	public SvgAaMod getSvgAaMod() {
		return this.phosphorylationMod;
	}
}