/*******************************************************************************
 * Copyright (c) 2014 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr>
 ******************************************************************************/
package fr.inra.pappso.phospredictor;

public class PhosSpectrumPeak implements Comparable<PhosSpectrumPeak> {

	Double mz;
	Double intensity;

	public PhosSpectrumPeak(Double mz, Double intensity) {
		this.mz = mz;
		this.intensity = intensity;
	}

	public Double getMz() {
		return mz;
	}

	@Override
	public int compareTo(PhosSpectrumPeak arg0) {
		// peaks are ordered by intensity first
		if (arg0.intensity > this.intensity) {
			return (1);
		} else if (arg0.intensity == this.intensity) {
			if (this.mz == arg0.mz) {
				return (0);
			} else if (arg0.mz > this.mz) {
				return (1);
			}
			return (-1);
		} else {
			return (-1);

		}
	}

	public Double getIntensity() {
		return this.intensity;
	}

}
