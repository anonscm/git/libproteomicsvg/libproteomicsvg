/*******************************************************************************
 * Copyright (c) 2014 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of LibProteomicSvg.
 *
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr>
 ******************************************************************************/

package fr.inra.pappso.phospredictor;

public class PhosPredictorNotPhopshoPeptideException extends
		PhosPredictorException {

	/**
	 *
	 */
	private static final long serialVersionUID = -5293991073048013926L;
	private String mess;

	public PhosPredictorNotPhopshoPeptideException(String message) {
		super(message);
		this.mess = message;
	}

	public PhosPredictorNotPhopshoPeptideException(String message, Exception e) {
		super(message, e);
	}

	public String getMessage() {
		return (mess);
	}

	public String toString() {
		return (mess);
	}
}
