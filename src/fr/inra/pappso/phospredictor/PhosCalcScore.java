/*******************************************************************************
 * Copyright (c) 2014 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of LibProteomicSvg.
 *
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr>
 ******************************************************************************/
package fr.inra.pappso.phospredictor;

import java.math.BigInteger;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import fr.inra.moulon.svgutils.svgspectrum.FragmentationTypeBase;
import fr.inra.moulon.svgutils.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgPeptideSequence;

public class PhosCalcScore implements Comparable<PhosCalcScore> {
	private static Logger logger = Logger.getLogger(PhosCalcScore.class);
	private int numberPossibleBandYions = 0;
	private int numberAssignedBandYions = 0;

	private int mostIntensePeakPerCent = 4;
	private double proba = 0;
	private SvgPeptideSequence svgPeptideSequence = null;
	private float fitPrecision = (float) 0.4;

	public PhosCalcScore(SvgPeptideSequence svgPeptideSequence,
			float fitPrecision, PhosSpectrum spectrum,
			PhosSpectrum originalSpectrum,
			FragmentationTypeBase fragmentationType)
			throws PhosPredictorException {
		this.fitPrecision = fitPrecision;

		this.svgPeptideSequence = svgPeptideSequence;
		logger.debug("PhosCalcScore svgPeptideSequence "
				+ svgPeptideSequence.getStringWithModifs());
		// logger.debug("PhosCalcScore spectrum size" + spectrum.size());
		// logger.debug("PhosCalcScore min mz" + spectrum.getMinMz());
		// logger.debug("PhosCalcScore max mz" + spectrum.getMaxMz());
		/*
		 * for (PhosSpectrumPeak peak : spectrum) { logger.debug("" +
		 * peak.getMz() + " " + peak.getIntensity()); }
		 */
		ArrayList<SvgIon> iontableAll;
		try {
			iontableAll = svgPeptideSequence.getIonTable(fragmentationType);
		} catch (Exception e) {
			throw new PhosPredictorException("error generating ion table", e);
		}
		ArrayList<SvgIon> iontablebandy = selectBandYionsInSpectrumRange(
				iontableAll, spectrum);

		this.numberPossibleBandYions = iontablebandy.size();
		this.numberAssignedBandYions = spectrum.getNumberOfAssignedIons(
				iontablebandy, fitPrecision);

		double bigp = (double) mostIntensePeakPerCent / (double) 100;

		BigInteger nFac = fact(this.numberPossibleBandYions);
		BigInteger kFac = fact(this.numberAssignedBandYions);

		long nMk = this.numberPossibleBandYions - this.numberAssignedBandYions;
		// logger.debug("nMk" + nMk);
		BigInteger nMkFac = fact(nMk);

		double c = Math.pow(1 - bigp, nMk);
		// logger.debug("c" + c);
		double b = Math.pow(bigp, this.numberAssignedBandYions);
		// logger.debug("b" + b);
		BigInteger a = nFac.divide(kFac.multiply(nMkFac));
		// logger.debug("a" + a + " nFac" + nFac + " kFac" + kFac + " nMkFac"
		// + nMkFac);

		// logger.debug("" + bigp);
		this.proba = a.doubleValue() * b * c;
	}

	private ArrayList<SvgIon> selectBandYionsInSpectrumRange(
			ArrayList<SvgIon> iontableAll, PhosSpectrum spectrum) {
		ArrayList<SvgIon> result = new ArrayList<SvgIon>();
		for (SvgIon ion : iontableAll) {
			if (ion.getSvgIonSpecies().getType().equals("b")
					|| ion.getSvgIonSpecies().getType().equals("y")) {

				// if (ion.getZ() == 1) {
				if ((ion.getMz() > spectrum.getMinMz())
						&& (ion.getMz() < spectrum.getMaxMz())) {
					// logger.debug("ion selected : " + ion.getName());
					result.add(ion);
				}
				// }
			}
		}
		return result;
	}

	public double getProba() {
		return this.proba;
	}

	public double getProbaScore() {
		return (-10 * (Math.log10(this.proba)));
	}

	public float getFitPrecision() {
		return this.fitPrecision;
	}

	public int getNumberOfAssignedIons() {
		return numberAssignedBandYions;
	}

	public int getNumberOfPossibleIons() {
		return this.numberPossibleBandYions;
	}

	public SvgPeptideSequence getSvgPeptideSequence() {
		return this.svgPeptideSequence;
	}

	public static BigInteger fact(long input) {
		long x;
		BigInteger fact = new BigInteger("1");
		for (x = input; x > 1; x--) {
			fact = fact.multiply(new BigInteger("" + x));
		}

		return fact;
	}

	@Override
	public int compareTo(PhosCalcScore arg0) {
		// peaks are ordered by intensity first
		if (arg0.proba < this.proba) {
			return (1);
		} else if (arg0.proba == this.proba) {
			// logger.debug("arg0.proba == this.proba");
			return this.svgPeptideSequence.compareTo(arg0.svgPeptideSequence);
		} else {
			return (-1);
		}
	}

}
