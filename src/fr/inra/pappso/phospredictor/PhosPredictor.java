/*******************************************************************************
 * Copyright (c) 2014 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of LibProteomicSvg.
 *
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr>
 ******************************************************************************/

package fr.inra.pappso.phospredictor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import fr.inra.moulon.svgutils.svgspectrum.FragmentationTypeBase;
import fr.inra.moulon.svgutils.svgspectrum.SvgAaMod;
import fr.inra.moulon.svgutils.svgspectrum.SvgAminoAcid;
import fr.inra.moulon.svgutils.svgspectrum.SvgPeptideSequence;

public class PhosPredictor {

	private static final Logger logger = Logger.getLogger(PhosPredictor.class);

	private SvgPeptideSequence phosPeptideRef;

	private TreeSet<Integer> potentialSitePos = new TreeSet<Integer>();
	private TreeSet<Integer> phosSiteRefPos = new TreeSet<Integer>();

	private PhosphoTypes phosphoTypesSearch;

	private HashSet<TreeSet<Integer>> phosSiteCombiPos = new HashSet<TreeSet<Integer>>();

	private HashMap<TreeSet<Integer>, SvgPeptideSequence> combi2peptide = new HashMap<TreeSet<Integer>, SvgPeptideSequence>();

	// private HashMap<TreeSet<Integer>, Float> combi2phosCalc = new
	// HashMap<TreeSet<Integer>, Float>();

	private PhosSpectrum spectrum = null;

	public PhosPredictor(SvgPeptideSequence phosPeptideRefIn,
			PhosphoTypes phosphoTypesSearch, PhosSpectrum spectrum)
			throws PhosPredictorException {
		this.phosPeptideRef = phosPeptideRefIn;
		this.phosphoTypesSearch = phosphoTypesSearch;
		this.spectrum = spectrum;

		this.grabPositions();
		if (this.phosSiteRefPos.size() == 0) {
			String message = "this peptide is not a PhosphoPeptide";
			logger.error(message);
			throw new PhosPredictorNotPhopshoPeptideException(message);
		}
		this.generateOtherSolutions();
		for (TreeSet<Integer> combi : phosSiteCombiPos) {
			// logger.debug("new peptide " + this.collectionToString(combi));
			this.combi2peptide.put(combi, this.newSvgPeptideSequence(
					phosPeptideRef, phosSiteRefPos, combi));
		}

		// logger.debug("combi2peptide.size()" + combi2peptide.size());
		// for (TreeSet<Integer> combi : phosSiteCombiPos) {
		// logger.debug(this.combi2peptide.get(combi).getStringWithModifs());
		// }

	}

	public PhosphoTypes getPhosphoType() {
		return this.phosphoTypesSearch;
	}

	public SvgPeptideSequence getSvgPeptideSequenceRef() {
		return this.phosPeptideRef;
	}

	public TreeSet<PhosCalcScore> computePhosCalc(
			FragmentationTypeBase fragmentationType, float fitPrecision)
			throws PhosPredictorException {
		// FragmentationTypeCID.getInstance()
		TreeSet<PhosCalcScore> result = new TreeSet<PhosCalcScore>();
		if (this.spectrum == null) {
			String message = "spectrum is null";
			logger.error(message);
			throw new PhosPredictorException(message);
		}

		PhosSpectrum newSpectrum = spectrum.newSpectrumMostIntensePerCent(4);

		for (TreeSet<Integer> combi : phosSiteCombiPos) {
			result.add(new PhosCalcScore(this.combi2peptide.get(combi),
					fitPrecision, newSpectrum, spectrum, fragmentationType));
		}
		return result;
	}

	private SvgPeptideSequence newSvgPeptideSequence(
			SvgPeptideSequence phosPeptideRef, TreeSet<Integer> combiRef,
			TreeSet<Integer> combi) throws PhosPredictorException {
		if (combiRef.equals(combi)) {
			return phosPeptideRef;
		}
		SvgPeptideSequence newSeq;
		try {
			newSeq = new SvgPeptideSequence(
					phosPeptideRef.getStringOnlySequence());
		} catch (Exception e) {
			String message = "error creating SvgPeptideSequence: "
					+ e.getMessage();
			logger.error(message);
			throw new PhosPredictorException(message, e);
		}
		newSeq.setExp_z(phosPeptideRef.getExp_z());
		Integer pos = 1;
		SvgAaMod aaModPhospho = this.phosphoTypesSearch.getSvgAaMod();
		for (SvgAminoAcid aa : phosPeptideRef) {
			SvgAminoAcid newAa;
			try {
				newAa = newSeq.getAA(pos);
			} catch (Exception e) {
				String message = "error getting AA in SvgPeptideSequence: "
						+ e.getMessage();
				logger.error(message);
				throw new PhosPredictorException(message, e);
			}
			ArrayList<SvgAaMod> aaModList = aa.getSvgAaModList();
			for (SvgAaMod aaMod : aaModList) {
				if ((combiRef.contains(pos)) && (aaMod.equals(aaModPhospho))) {

				} else {
					newAa.addModification(aaMod);
				}
			}
			if (combi.contains(pos)) {
				newAa.addModification(aaModPhospho);
			}
			pos++;
		}
		return newSeq;
	}

	private void generateOtherSolutions() {
		// logger.debug("generateOtherSolutions begin");
		int nbSites = phosSiteRefPos.size();
		int nbFreePos = nbSites - potentialSitePos.size();
		if (nbFreePos == 0) {
			return;
		}
		logger.debug("generateOtherSolutions step2 nbSites" + nbSites
				+ " potentialSitePos.size()" + potentialSitePos.size());
		for (int i = 0; i < nbSites; i++) {
			for (Integer pos : potentialSitePos) {
				TreeSet<Integer> phosPos = new TreeSet<Integer>();
				phosPos.add(pos);
				phosSiteCombiPos.add(phosPos);
				this.addPhosPos(pos);
			}
		}
		/*
		 * for (TreeSet<Integer> combi : phosSiteCombiPos) {
		 * logger.debug(" all :" + PhosPredictor.collectionToString(combi)); }
		 */
		// logger.debug("generateOtherSolutions step3 phosSiteCombiPos"
		// + phosSiteCombiPos.size());
		HashSet<TreeSet<Integer>> finalPhosSiteCombiPos = new HashSet<TreeSet<Integer>>();
		for (TreeSet<Integer> combi : phosSiteCombiPos) {
			if (combi.size() < nbSites) {
				// logger.debug(" remove combi.size() " + combi.size()
				// + "< nbSites " + nbSites + " "
				// + PhosPredictor.collectionToString(combi));
			} else if (combi.size() > nbSites) {
				// logger.debug(" remove"
				// + PhosPredictor.collectionToString(combi));
			} else {
				finalPhosSiteCombiPos.add(combi);
			}
		}

		this.phosSiteCombiPos = finalPhosSiteCombiPos;

		/*
		 * for (TreeSet<Integer> combi : phosSiteCombiPos) {
		 * logger.debug(" solution :" +
		 * PhosPredictor.collectionToString(combi)); }
		 */

	}

	public static String collectionToString(Collection<?> obsPhosphoPos) {
		String toWrite = "";
		for (Object item : obsPhosphoPos) {
			toWrite += " " + item;
		}
		return toWrite.trim();
	}

	private void addPhosPos(Integer pos) {
		// logger.debug("addPhosPos begin");
		HashSet<TreeSet<Integer>> newPhosSiteCombiPos = new HashSet<TreeSet<Integer>>();
		for (TreeSet<Integer> combi : phosSiteCombiPos) {
			TreeSet<Integer> newCombi = new TreeSet<Integer>(combi);
			newCombi.add(pos);
			newPhosSiteCombiPos.add(newCombi);
			// this.addCombiPos(newCombi);
		}

		phosSiteCombiPos.addAll(newPhosSiteCombiPos);
		// this
		// logger.debug("addPhosPos end");
	}

	/*
	 * private void addCombiPos(TreeSet<Integer> newCombi) { boolean isThere =
	 * false; for (TreeSet<Integer> combi : phosSiteCombiPos) { if
	 * (combi.equals(newCombi)) {
	 *
	 * } }
	 *
	 * }
	 */
	private void grabPositions() {
		// logger.debug("grabPositions begin");
		int pos = 1;
		for (SvgAminoAcid aa : phosPeptideRef) {
			// logger.debug("pos " + pos);
			if (this.phosphoTypesSearch == PhosphoTypes.phosphorylation) {
				// logger.debug("pos" + pos + " " + aa.toString());
				if (aa.isAA('S') || aa.isAA('T') || aa.isAA('Y')) {
					// logger.debug("pos" + pos + " " + aa.toString());
					this.potentialSitePos.add(pos);
					if (aa.getSvgAaModList().contains(
							PhosphoTypes.phosphorylation.getSvgAaMod())) {
						this.phosSiteRefPos.add(pos);
					}
				}
			}
			if (this.phosphoTypesSearch == PhosphoTypes.dehydratedResidue) {
				// logger.debug("pos" + pos + " " + aa.toString());
				if (aa.isAA('S') || aa.isAA('T')) {
					this.potentialSitePos.add(pos);
					if (aa.getSvgAaModList().contains(
							PhosphoTypes.dehydratedResidue.getSvgAaMod())) {
						this.phosSiteRefPos.add(pos);
					}
				}
			}
			pos++;
		}

	}

	public int getNumberOfPhosphorylations() {
		return this.phosSiteRefPos.size();
	}

	// public boolean isPhospho() {
	/*
	 * if (this.aa.equals("S") || this.aa.equals("T")) { for (SvgAaMod mod :
	 * this.getSvgAaModList()) { if (mod.equals(PhosphoTypes.phosphorylation)) {
	 * return true; } // MOD:00704 dehydrated residue if (this.aa.equals("S")) {
	 * if (mod.equals(PhosphoTypes.dehydratedResidue)) { return true; } } } }
	 * return false;
	 */
	/*
	 * if ((this.modvalue > 79.9) & (this.modvalue < 80.1)) return true; boolean
	 * serthre = false; if (this.AA.equals("T")) serthre = true; else if
	 * (this.AA.equals("S")) serthre = true; if ((serthre) & (this.modvalue <
	 * -17.9) & (this.modvalue > -18.1)) return true; return false;
	 */
	// }

}
