/*******************************************************************************
 * Copyright (c) 2014 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of LibProteomicSvg.
 *
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr>
 ******************************************************************************/
package fr.inra.pappso.phospredictor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import fr.inra.moulon.svgutils.svgspectrum.SvgIon;

public class PhosSpectrum extends TreeSet<PhosSpectrumPeak> {
	/**
	 *
	 */
	private static final long serialVersionUID = -4272907598089695978L;
	private static Logger logger = Logger.getLogger(PhosSpectrum.class);
	private double minMz = 999999999;
	private double maxMz = 0;

	// TreeSet<PhosSpectrumPeak> arrSpectrumPeak = new
	// TreeSet<PhosSpectrumPeak>();

	public PhosSpectrum() {

	}

	@Override
	public boolean add(PhosSpectrumPeak peak) {
		if (minMz > peak.getMz()) {
			minMz = peak.getMz();
		}
		if (maxMz < peak.getMz()) {
			maxMz = peak.getMz();
		}
		return super.add(peak);
	}

	public PhosSpectrum(InputStream isDtaFile) throws PhosPredictorException {
		BufferedReader in = new BufferedReader(new InputStreamReader(isDtaFile));

		try {
			if (in.ready()) {
				// first line contains precursor mass
				String[] strPeak = in.readLine().trim().split(" ");

			}
			while (in.ready()) {
				String[] strPeak = in.readLine().trim().split(" ");

				if (strPeak.length == 2) {
					// for (int i = 0; i < strPeak.length; i++) {
					this.add(new PhosSpectrumPeak(new Double(strPeak[0]),
							new Double(strPeak[1])));
					// }
				}
			}

			in.close();
		} catch (IOException e) {
			String message = "ERROR reading dta file:\n" + e.getMessage();
			logger.error(message);
			throw new PhosPredictorException(message, e);
		}
	}

	public PhosSpectrum newSpectrumMostIntensePerCent(int max) {
		PhosSpectrum result = new PhosSpectrum();
		// logger.debug("min : " + this.minMz + " max:" + this.maxMz);

		HashMap<Integer, PhosSpectrum> spectrumParts = new HashMap<Integer, PhosSpectrum>();

		for (PhosSpectrumPeak spectrumPeak : this) {

			int mzClass = (int) (spectrumPeak.getMz() / 100);
			// int mzClass = (int) ((spectrumPeak.getMz() - this.minMz-100) /
			// 100);
			// logger.debug("class = " + mzClass + " mz=" +
			// spectrumPeak.getMz());
			if (spectrumParts.containsKey(mzClass)) {
			} else {
				spectrumParts.put(mzClass, new PhosSpectrum());
			}
			PhosSpectrum spectrumPart = spectrumParts.get(mzClass);
			spectrumPart.add(spectrumPeak);
		}

		for (Integer mzClass : spectrumParts.keySet()) {
			// logger.debug("i = 0");
			int i = 0;
			PhosSpectrum spectrumPart = spectrumParts.get(mzClass);
			for (PhosSpectrumPeak peak : spectrumPart) {
				if (i == max) {
					break;
				}
				// logger.debug("" + peak.getMz() + " " + peak.getIntensity());
				result.add(peak);
				i++;
				
			}
		}

		return result;
	}

	public int getNumberOfAssignedIons(ArrayList<SvgIon> iontablebandy,
			float fitPrecision) {

		// logger.debug("fitPrecision " + fitPrecision);
		float mzMinus = 0;
		float mzPlus = 0;
		int result = 0;
		for (SvgIon ion : iontablebandy) {
			mzMinus = ion.getMz() - fitPrecision;
			mzPlus = ion.getMz() + fitPrecision;

			for (PhosSpectrumPeak peak : this) {
				if ((mzMinus < peak.getMz()) && (peak.getMz() < mzPlus)) {
					// logger.debug("" + mzMinus + " " + peak.getMz() + " "
					// + mzPlus);

					result++;
					break;
				}
			}
		}
		return result;
	}

	public double getMinMz() {
		return this.minMz;
	}

	public double getMaxMz() {
		return this.maxMz;
	}
}
