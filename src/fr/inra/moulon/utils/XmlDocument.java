/*******************************************************************************
 * Copyright (c) 2011-06-28 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.w3c.dom.Document;

public abstract class XmlDocument {

	protected Document xmldoc;
	private boolean alreadyRendered = false;
	private int uidCount = 0;

	final public Document getDocument() {
		return this.xmldoc;
	}

	final public Document getRenderedDocument() {
		if (alreadyRendered == false) {
			render();
			writeCss();
			alreadyRendered = true;
		}
		return this.xmldoc;
	}

	final public void toOutputStream(OutputStream out) {
		getRenderedDocument();

		docToOutputStream(out);
	}

	abstract protected void render();

	abstract protected void writeCss();

	abstract protected void docToOutputStream(OutputStream out);

	final public String toString() {
		// Stream to write file

		ByteArrayOutputStream fout = new ByteArrayOutputStream();
		this.toOutputStream(fout);
		try {
			fout.flush();
		} catch (IOException e) {
			// logger
		}

		// StringBuilder sb = new StringBuilder();
		// sb.append(fout.toByteArray());

		return new String(fout.toByteArray());

	}

	final public void toFile(File filename) {
		// Stream to write file
		FileOutputStream fout;

		try {
			// Open an output stream
			fout = new FileOutputStream(filename.getAbsolutePath());

			// Print a line of text
			this.toOutputStream(fout);

			// Close our output stream
			fout.close();
		}
		// Catches any error conditions
		catch (IOException e) {
			System.err.println("Unable to write to file");
			System.exit(-1);
		}

	}

	final public String getUniqueId() {
		this.uidCount++;
		return ("uid" + uidCount);
	}

}