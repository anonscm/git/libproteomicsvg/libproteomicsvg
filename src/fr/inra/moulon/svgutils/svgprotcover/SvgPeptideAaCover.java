package fr.inra.moulon.svgutils.svgprotcover;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.svgutils.elements.SvgGroupBlock;

public class SvgPeptideAaCover extends SvgGroupBlock {

	public SvgPeptideAaCover(SvgDocument document) {
		super(document);
		this.setWidth(SvgAaCover.width);
		this.setHeight(SvgAaCover.height);

	}
}
