package fr.inra.moulon.svgutils.svgprotcover;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.svgutils.elements.SvgBlock;
import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgText;
import fr.inra.moulon.svgutils.svgspectrum.SvgAminoAcid;

public class SvgAaCover extends SvgGroup implements SvgBlock {

	static float width = 15;
	static float height = 20;
	SvgAminoAcid aa;

	public SvgAaCover(SvgDocument document, SvgAminoAcid aa) {
		super(document);
		this.aa = aa;

		SvgText text = this.newText(aa.toString());
		// SvgText text = new SvgText(document, aa.toString());

		text.setdx((float) (width * 0.2));
		text.setdy((float) (height - (height * 0.2)));
		// text.newTspan(aa.toString());
		// this.appendChild(text);

		// SvgRect rect = new SvgRect(document, width, height);
		// this.appendChild(rect);

	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}
}
