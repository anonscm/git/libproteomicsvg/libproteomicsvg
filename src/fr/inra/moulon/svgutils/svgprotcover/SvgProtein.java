package fr.inra.moulon.svgutils.svgprotcover;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

import fr.inra.moulon.svgutils.ProteomicSvgException;
import fr.inra.moulon.svgutils.svgspectrum.SvgAminoAcid;

public class SvgProtein implements Iterable<SvgAminoAcid> {
	private static Logger logger = Logger.getLogger(SvgProtein.class);
	protected ArrayList<SvgAminoAcid> aaList = new ArrayList<SvgAminoAcid>(0);

	public SvgProtein(String protSequence) throws ProteomicSvgException {

		if (protSequence == null) {
			String message = "protSequence is null";
			logger.error(message);
			throw new ProteomicSvgException(message);
		}
		if (protSequence.isEmpty()) {
			String message = "protSequence is empty";
			logger.error(message);
			throw new ProteomicSvgException(message);
		}
		try {
			for (Character aacar : protSequence.toCharArray()) {
				SvgAminoAcid aa;
				aa = new SvgAminoAcid(aacar);
				aaList.add(aa);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Iterator<SvgAminoAcid> iterator() {
		return this.aaList.iterator();
	}

}
