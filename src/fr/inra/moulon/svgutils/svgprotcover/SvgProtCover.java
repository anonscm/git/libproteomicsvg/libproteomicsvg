package fr.inra.moulon.svgutils.svgprotcover;

import java.util.Hashtable;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgGroupBlock;
import fr.inra.moulon.svgutils.elements.SvgPath;
import fr.inra.moulon.svgutils.elements.SvgRect;
import fr.inra.moulon.svgutils.svgspectrum.SvgAminoAcid;

public class SvgProtCover extends SvgGroupBlock {
	SvgProtein protein;
	float ratioXY;

	private int nbAaPerLine;
	private Object embedDataInCover;
	private SvgGroup peptidesGroup;
	private Hashtable<String, Integer> countCoverKey;

	public SvgProtCover(SvgDocument document, SvgProtein protein, float ratioXY) {
		super(document);
		countCoverKey = new Hashtable<String, Integer>();
		this.protein = protein;
		this.ratioXY = ratioXY;
		float ratioxy = SvgAaCover.width / SvgAaCover.height;

		int nbAa = protein.aaList.size() / 10;
		nbAa = (nbAa + 1) * 10;

		double Y;
		// System.out.println(nbAa);
		// System.out.println(SvgAaCover.width);
		// System.out.println(ratioXY);

		Y = java.lang.Math
				.sqrt(((double) nbAa * ratioxy * SvgAaCover.height * SvgAaCover.height)
						/ (ratioXY));

		// System.out.println(Y);
		int nbline = (int) (Y / SvgAaCover.height) + 1;
		Y = SvgAaCover.height * nbline;

		setHeight((float) Y);
		setWidth((float) (ratioXY * Y));

		int maxcol = (int) (this.getWidth() / SvgAaCover.width);
		this.nbAaPerLine = maxcol;
		// "text-anchor:middle;glyph-anchor:centerline;
		this.document.setCssClass(".protcover text", "text-anchor", "start");
		// this.document.setCssClass(".protcover text", "glyph-anchor",
		// "centerline");
		// this.document.setCssClass(".protcover text", "text-anchor", "start");
		// this.document.setCssClass(".protcover text", "text-align", "center");
		// this.document.setCssClass(".protcover text", "vertical-align",
		// "middle");
		// this.document.setCssClass(".protcover text", "alignment-baseline",
		// "baseline");
		// style="font-family:monospace;font-size:100px;">
		this.document.setCssClass(".protcover text", "font-family",
				"Andale Mono");
		// this.document.setCssClass(".protcover text", "font-family",
		// "monospace");
		this.document.setCssClass(".protcover text", "font-size", ""
				+ (SvgAaCover.height - (SvgAaCover.height * 0.2)) + "px");
		// fill:none;stroke:#000000;stroke-opacity:1;stroke-width:0.99997068;stroke-miterlimit:4;stroke-dasharray:none
		this.document.setCssClass(".protcover rect", "fill", "none");
		this.document.setCssClass(".protcover rect", "stroke", "#000000");
		this.document.setCssClass(".protcover rect", "stroke-width", "1px");
		// this.document.setCssClass(".cover path", "fill", "yellow");
		this.document.setCssClass(".cover path", "fill", "none");
		this.document.setCssClass(".cover path", "stroke-width", "1px");
		this.document.setCssClass(".cover path", "stroke", "red");
		this.document.setCssClass(".cover rect", "fill", "red");
		this.document.setCssClass(".cover rect", "fill-opacity", "0.5");
		this.document.setCssClass(".cover rect", "stroke", "none");
		this.document.setCssClass(".cover rect", "stroke-width", "0px");

		this.setCssClass("protcover");

		int i = 0;
		peptidesGroup = new SvgGroup(document);
		this.appendChild(peptidesGroup);
		SvgGroup sequence = new SvgGroup(document);
		this.appendChild(sequence);

		for (SvgAminoAcid aa : protein) {
			SvgAaCover aaCover = new SvgAaCover(this.getSvgDocument(), aa);
			// aaCover.setX(nbc*SvgAaCover.width);
			// aaCover.setY(nbl*SvgAaCover.height);

			aaCover.translate(getXcoordOfNaa(i), getYcoordOfNaa(i));

			sequence.appendChild(aaCover);

			i++;
		}
	}

	protected float getXcoordOfNaa(int naa) {
		float xcoord = 0;

		int nbline = naa / nbAaPerLine;

		xcoord = (naa - (nbline * nbAaPerLine)) * SvgAaCover.width;
		return (xcoord);
	}

	protected float getYcoordOfNaa(int naa) {
		float ycoord = 0;

		int nbline = naa / nbAaPerLine;

		ycoord = nbline * SvgAaCover.height;
		return (ycoord);
	}

	/**
	 *
	 * @param start
	 *            number of aa where to start (>=1)
	 * @param stop
	 * @return
	 */
	public SvgGroup newPeptideCover(int start, int stop, Object embedData) {
		String coverKey = "" + start + "-" + stop;
		Integer nb = 1;
		//countCoverKey.put(coverKey, nb);
		if (countCoverKey.containsKey(coverKey)) {
			nb = countCoverKey.get(coverKey) + 1;
		}
		countCoverKey.put(coverKey, nb);
		if (nb > 10) {
			return null;
		}
		SvgGroup pepGroup = new SvgGroup(document);
		peptidesGroup.appendChild(pepGroup);

		this.embedDataInCover = embedData;

		pepGroup.appendChild(this.newPeptideCoverStart(start));

		for (int i = (start + 1); i < stop; i++) {
			pepGroup.appendChild(this.newPeptideCoverIn(i));
		}
		pepGroup.appendChild(this.newPeptideCoverStop(stop));
		pepGroup.setCssClass("cover");

		return pepGroup;
	}

	private SvgPeptideAaCover newSvgPeptideAaCover(int nnAa) {
		SvgPeptideAaCover group = new SvgPeptideAaCover(document);
		float xcoord = this.getXcoordOfNaa(nnAa - 1);
		float ycoord = this.getYcoordOfNaa(nnAa - 1);

		group.translate(xcoord, ycoord);

		this.registerBlock(group);
		if (embedDataInCover != null) {
			group.setData(this.embedDataInCover);
		}

		return group;

	}

	private SvgPeptideAaCover newPeptideCoverStop(int nnAa) {
		SvgPeptideAaCover group = newSvgPeptideAaCover(nnAa);

		SvgRect rect = group.newRect(SvgAaCover.width - 1,
				SvgAaCover.height - 2);
		// SvgRect rect = new SvgRect(document, SvgAaCover.width-1,
		// SvgAaCover.height-2);
		rect.setX(0);
		rect.setY(1);
		// group.appendChild(rect);

		SvgPath path = group.newPath();
		// SvgPath path = new SvgPath(document);

		path.moveto(0, 1);
		path.lineto(SvgAaCover.width - 1, 1);
		path.lineto(SvgAaCover.width - 1, SvgAaCover.height - 1);
		path.lineto(0, SvgAaCover.height - 1);
		// group.appendChild(path);

		return group;
	}

	private SvgPeptideAaCover newPeptideCoverIn(int nnAa) {
		SvgPeptideAaCover group = newSvgPeptideAaCover(nnAa);

		SvgRect rect = group.newRect(SvgAaCover.width, SvgAaCover.height - 2);
		// SvgRect rect = new SvgRect(document, SvgAaCover.width,
		// SvgAaCover.height-2);
		rect.setX(0);
		rect.setY(1);
		// group.appendChild(rect);

		SvgPath path = group.newPath();
		// SvgPath path = new SvgPath(document);
		path.moveto(0, 1);
		path.lineto(SvgAaCover.width, 1);
		path.moveto(0, SvgAaCover.height - 1);
		path.lineto(SvgAaCover.width, SvgAaCover.height - 1);
		// group.appendChild(path);

		return group;
	}

	private SvgPeptideAaCover newPeptideCoverStart(int nnAa) {
		SvgPeptideAaCover group = newSvgPeptideAaCover(nnAa);

		SvgRect rect = group.newRect(SvgAaCover.width - 1,
				SvgAaCover.height - 2);
		// SvgRect rect = new SvgRect(document, SvgAaCover.width-1,
		// SvgAaCover.height-2);
		rect.setX(1);
		rect.setY(1);
		// group.appendChild(rect);

		SvgPath path = group.newPath();
		// SvgPath path = new SvgPath(document);

		path.moveto(SvgAaCover.width, 0 + 1);
		path.lineto(1, 1);
		path.lineto(1, SvgAaCover.height - 1);
		path.lineto(SvgAaCover.width, SvgAaCover.height - 1);

		// group.appendChild(path);
		return group;
	}

}
