package fr.inra.moulon.svgutils;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.*;

import fr.inra.moulon.svgutils.elements.SvgBlock;
import fr.inra.moulon.svgutils.elements.SvgCoordXY;
import fr.inra.moulon.svgutils.elements.SvgDefs;
import fr.inra.moulon.svgutils.elements.SvgGroupBlock;
import fr.inra.moulon.svgutils.elements.SvgScript;
import fr.inra.moulon.svgutils.elements.SvgStyle;
import fr.inra.moulon.svgutils.elements.SvgSvg;
import fr.inra.moulon.svgutils.elements.SvgSymbol;
import fr.inra.moulon.utils.XmlDocument;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//This is a small proof-of-concept graphics class that
// provides method calls for the creation of the following
// DOM tree nodes:
//  A simple element with no attributes.
//  A linear gradient element.
//  An ellipse
//  A circle

//Each method receives a reference to the overall document
// along with a reference to the parent for the new node.
//When the method returns, the new node has been appended
// to the parent node.

public class SvgDocument extends XmlDocument {

	public static String svgNS = "http://www.w3.org/2000/svg";
	public static String xlinkNS = "http://www.w3.org/1999/xlink";
	public static String xmlNS = "http://www.w3.org/2000/xmlns/";

	protected static Pattern viewBoxPattern = Pattern
			.compile("^(.*[^ ]) (.*[^ ]) (.*[^ ]) (.*[^ ])$");

	private SvgStyle style;
	private SvgScript script;

	HashMap<String, HashMap<String, String>> cssClasses = new HashMap<String, HashMap<String, String>>(
			0);
	private SvgSvg svgroot;
	private SvgDefs svgDefs;

	private ArrayList<SvgGroupBlock> arrBlocks = new ArrayList<SvgGroupBlock>(0);

	public SvgDocument() {

		createDocument();

	}

	public void addSymbol(SvgSymbol symbol) {
		svgDefs.appendChild(symbol);
	}

	public SvgDocument(float width, float height) {
		// System.out.println("SvgDocument(float width, float height) begin");
		createDocument();
		// System.out.println("SvgDocument(float width, float height) width "+width);

		svgroot.setWidth(width);
		svgroot.setHeight(height);

	}

	public void registerBlock(SvgGroupBlock block) {
		arrBlocks.add(block);
	}

	public HashSet<Object> getDataSet(SvgCoordXY coord) throws Exception {
		// transform coords to pass it to childs
		SvgCoordXY coordTrans = this.viewBoxCoordTrans(coord);

		// System.out.println(coordTrans.toString());
		HashSet<Object> objects = new HashSet<Object>();

		for (SvgGroupBlock block : arrBlocks) {
			HashSet<Object> objList = block.getDataSet(coordTrans);
			objects.addAll(objList);
		}
		return objects;
	}

	private SvgCoordXY viewBoxCoordTrans(SvgCoordXY coord) throws Exception {
		// height="600.0" viewBox="0 0 70.71068 212.13203" width="200.0"
		SvgCoordXY origView = this.getViewBoxOrigin();
		if (origView != null) {
			SvgCoordXY toView = this.getViewBoxOffset();

			float newX = ((coord.getX() / this.getWidth()) * (toView.getX() - origView
					.getX())) + origView.getX();
			float newY = ((coord.getY() / this.getHeight()) * (toView.getY() - origView
					.getY())) + origView.getY();
			// System.out.println("viewBoxCoordTrans  x "+newX+" y " + newY);
			SvgCoordXY newCoord = new SvgCoordXY(newX, newY);
			return newCoord;
		}
		return coord;
	}

	private SvgCoordXY getViewBoxOffset() throws Exception {
		// System.out.println("getViewBoxOffset  begin");
		String viewBox = this.svgroot.getViewBox(); // .getAttributeNS(null,
													// "viewBox");

		if (viewBox == null) {
			return null;
		}
		// System.out.println("getViewBoxOffset  viewBox "+viewBox);
		Matcher m = SvgDocument.viewBoxPattern.matcher(viewBox);
		if (m.find()) {
			// System.out.println("getViewBoxOffset  pattern found "+m.group(3));
			// System.out.println("getViewBoxOffset  pattern found "+m.group(4));
			SvgCoordXY orig = new SvgCoordXY(new Float(m.group(3)), new Float(
					m.group(4)));
			return orig;
		} else {
			// System.out.println("no pattern found in " + viewBox);
			throw new Exception("no pattern found in " + viewBox);
		}

	}

	private SvgCoordXY getViewBoxOrigin() throws Exception {
		// System.out.println("getViewBoxOrigin  begin");
		String viewBox = this.svgroot.getViewBox();
		if (viewBox == null) {
			return null;
		}
		// System.out.println("getViewBoxOrigin  viewBox "+viewBox);
		Matcher m = SvgDocument.viewBoxPattern.matcher(viewBox);
		if (m.find()) {
			// System.out.println("getViewBoxOrigin  pattern found "+m.group(1));
			// System.out.println("getViewBoxOrigin  pattern found "+m.group(2));
			SvgCoordXY orig = new SvgCoordXY(new Float(m.group(1)), new Float(
					m.group(2)));
			return orig;
		} else {
			// System.out.println("no pattern found in " + viewBox);
			throw new Exception("no pattern found in " + viewBox);
		}
	}

	// ----------------------------------------------------//

	// This is a utility method that is used to execute code
	// that is the same regardless of the graphic image
	// being produced.
	private void createDocument() {
		try {
			// We need a Document
			// String svgNS = "http://www.w3.org/2000/svg";
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			dbfac.setValidating(false);

			// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
			// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
			// DocumentType svgDocType = new DocumentType();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();

			xmldoc = docBuilder.newDocument();

			xmldoc.createAttributeNS(xlinkNS, "xlink");

			// System.out.println("SvgDocument(float width, float height) createDocument begin");
			svgroot = new SvgSvg(this);

			svgroot.setXmlnsXlink(); // AttributeNS(xmlNS, "xmlns:xlink",
										// xlinkNS);
			// svgroot.setAttributeNS(null, "contentScriptType",
			// "text/ecmascript");
			// svgroot.setAttributeNS(null, "zoomAndPan", "magnify");
			// svgroot.setAttributeNS(null, "contentStyleType", "text/css");
			// svgroot.setAttributeNS(null, "preserveAspectRatio",
			// "xMidYMid meet");
			// svgroot.setAttributeNS(null, "version", "1.1");

			// System.out.println("SvgDocument(float width, float height) createDocument begin 2");
			// document.createElementNS(svgNS, "svg");
			xmldoc.appendChild(svgroot.getElement());
			this.svgScript();
			this.svgStyle();

			svgDefs = svgroot.newDefs();
			// System.out.println("SvgDocument(float width, float height) createDocument ccc");

		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}

		if (svgroot == null) {
			System.out.println("svgroot is null (createDocument)");
			System.exit(0);

		}
	}// end getDocument

	public SvgDefs getSvgDefs() {
		// TODO Auto-generated method stub
		return svgDefs;
	}

	// ----------------------------------------------------//

	private static void docToOutputStream(Document document, OutputStream out) {
		try {
			// Get a TransformerFactory object.
			TransformerFactory xformFactory = TransformerFactory.newInstance();

			// Get an XSL Transformer object.
			Transformer transformer = xformFactory.newTransformer();

			// Sets the standalone property in the first line of
			// the output file.
			// transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer
					.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
			// OutputKeys.OMIT_XML_DECLARATION

			// <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
			// "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
			// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
					"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
					"-//W3C//DTD SVG 1.1//EN");
			// Get a DOMSource object that represents the
			// Document object

			DOMSource source = new DOMSource(document.getDocumentElement());

			// Get a StreamResult object that points to the
			// screen. Then transform the DOM sending XML to
			// the screen.
			StreamResult scrResult = new StreamResult(out);
			transformer.transform(source, scrResult);

		}// end try block

		catch (Exception e) {
			e.printStackTrace(System.err);
		}// end catch
	}// end transformTheDom

	public void setCssClass(String cssKey, String cssProperty, String cssValue) {
		HashMap<String, String> the_css;
		if (cssClasses.containsKey(cssKey)) {
			the_css = cssClasses.get(cssKey);
		} else {
			the_css = new HashMap<String, String>(0);
			cssClasses.put(cssKey, the_css);
		}
		the_css.put(cssProperty, cssValue);

	}

	public void setCssClass(String classname, String classdefinition) {

		String[] props = classdefinition.split(";");
		HashMap<String, String> the_css = new HashMap<String, String>(0);
		cssClasses.put(classname, the_css);

		for (String css : props) {
			String[] property = css.split(":", 2);
			if (property.length == 2) {
				the_css.put(property[0], property[1]);
			} else {
				// the_css.put(property[0], "");
			}
		}

		// this.cssClasses.put(classname, classdefinition);
	}

	public void writeCss() {
		// Add a CDATA section to the root element
		StringBuilder csssyntax = new StringBuilder("\n");

		for (String classKey : this.cssClasses.keySet()) {

			csssyntax.append("\n" + classKey + " {");

			HashMap<String, String> cssClass = cssClasses.get(classKey);
			for (String propertyKey : cssClass.keySet()) {
				csssyntax.append(propertyKey + ":" + cssClass.get(propertyKey)
						+ ";");
			}

			csssyntax.append("}");

			// la clé de l'élement courant est e . getKey ( ) ;

			// la valeur de l'élement courant est e . getValue ( )

		}
		csssyntax.append("\n");
		// System.out.print("cestca: "+csssyntax);

		// cdataStyle.deleteData(0, cdataStyle.getLength());
		this.style.setCssSyntax(csssyntax.toString());

	}

	private void svgScript() {
		this.script = svgroot.newScript();
	}

	protected SvgScript getScript() {
		return this.script;
	}

	protected void svgStyle() {

		style = svgroot.newStyle();

		// contentScriptType="text/ecmascript" contentStyleType="text/css"
		// svgroot.setAttributeNS(null, "contentScriptType", "text/ecmascript");

	}

	public Float getWidth() {
		return this.svgroot.getWidth();
	}

	public Float getHeight() {
		return this.svgroot.getHeight();
	}

	protected void fitViewBoxTo(SvgBlock block) {
		float width = block.getWidth();
		float height = block.getHeight();
		svgroot.setViewBox(0, 0, width, height);
	}

	public SvgSvg getSvgRoot() {
		if (this.svgroot == null) {
			System.out.print("svgroot is null");
		}
		return this.svgroot;
	}

	@Override
	protected void render() {

	}

	@Override
	protected void docToOutputStream(OutputStream out) {
		docToOutputStream(this.getDocument(), out);
	}

	public HashMap<String, HashMap<String, String>> getCssHashMap() {
		return this.cssClasses;
	}

}// end class SvgGraphics

