package fr.inra.moulon.svgutils.svggelimage;

import fr.inra.moulon.svgutils.elements.SvgSymbol;

public class SvgSpotSymbol extends SvgSymbol {

	protected SvgSpotSymbol(SvgGelImageDocument svgGelImage, String id) {
		super(svgGelImage, id);
		/*
		 * <symbol id="MySymbol" viewBox="0 0 20 20"> <desc>MySymbol - four
		 * rectangles in a grid</desc> <rect x="1" y="1" width="8" height="8"/>
		 * <rect x="11" y="1" width="8" height="8"/> <rect x="1" y="11"
		 * width="8" height="8"/> <rect x="11" y="11" width="8" height="8"/>
		 * </symbol>
		 */
		// this.setViewBox(0, 0, 50, 50);

		svgGelImage.addSymbol(this);
	}

	/*
	 * public void setCss(String css) throws Exception { if (css == null) { css
	 * = "fill:none;stroke-width:" + 2 +
	 * "px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"; }
	 * 
	 * this.svgGelImage.getSvgGraphic().setCssClass("#" + this.getId() + " *",
	 * css); }
	 */
	public void drawCross(Float width) throws Exception {
		if (width == null) {
			width = (float) 6;
		}
		// draw a cross in a 100/100 square (0->10), the center must be at 50/50
		float half = width / 2;
		// SvgGraphics.svgLine(this.svgGelImage.getSvgGraphic().getDocument(),
		// symbol, 50, 50 - half, 50, 50 + half, null);

		this.newLine(50, 50 - half, 50, 50 + half);

		// SvgGraphics.svgLine(this.svgGelImage.getSvgGraphic().getDocument(),
		// symbol, 50 - half, 50, 50 + half, 50, null);
		this.newLine(50 - half, 50, 50 + half, 50);

	}

	public void drawCircle(Float width) throws Exception {
		if (width == null) {
			width = (float) 10;
		}
		// draw a cross in a 100/100 square (0->10), the center must be at 50/50
		// <circle cx="100" cy="50" r="40"

		this.newCircle(50, 50, width);
	}


	public void setCss(String css) throws Exception {
		if (css == null) {
			css = "fill:none;stroke-width:"
					+ 2
					+ "px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1";
		}

		this.document.setCssClass("#" + this.getId() + " *",
				css);
	}

}
