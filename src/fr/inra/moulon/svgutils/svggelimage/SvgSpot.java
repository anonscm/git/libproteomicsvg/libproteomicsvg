/*******************************************************************************
 * Copyright (c) 2011-06-28 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/
package fr.inra.moulon.svgutils.svggelimage;

import java.net.URI;


import fr.inra.moulon.svgutils.elements.SvgAnchor;
import fr.inra.moulon.svgutils.elements.SvgElementDrawBase;
import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgSymbol;
import fr.inra.moulon.svgutils.elements.SvgTextInTspan;

public class SvgSpot {

	protected SvgSymbol symbol;

	protected float x, y;

	protected String label;
	protected String label_css_class;

	protected SvgGelImageDocument gel_image;

	protected boolean is_highlighted;

	private String spotIdentifier;

	private SvgGroup group;

	public SvgSpot(SvgGelImageDocument svgGelImageDocument, float x, float y,
			SvgSymbol currentSymbol) {
		this.gel_image = svgGelImageDocument;
		this.symbol = currentSymbol;
		this.x = x;
		this.y = y;
		this.label_css_class = "slabelf";
		is_highlighted = false;
		spotIdentifier = null;
		group = svgGelImageDocument.getCurrentSpotGroup();
	}

	public float getX() {
		return this.x;
	}

	public float getY() {
		return this.y;
	}

	public SvgElementDrawBase drawSvgSpot() throws Exception {
		/*
		 * <use x="45" y="10" width="10" height="10" xlink:href="#MySymbol" />
		 */
		SvgElementDrawBase container = group;
		if ((this.getId() != null) && (gel_image.getSpotBaseUrl() != null)) {
			String spotUrl = gel_image.getSpotBaseUrl().toString()
			+ this.getId();
			SvgAnchor a = group.newAnchor(new URI(spotUrl));
			a.setNewWindows();
			container = a;
		}		
		//gel_image.getSvgGraphic().goToGroup(group);
		container.newUse(this.symbol,this.x - 50, this.y - 50 );
		//Element spot = gel_image.getSvgGraphic().use(this.x - 50, this.y - 50,
		//		this.symbol.getElement());

		if (this.label != null) {
			// gel_image.getSvgGraphic().goToGroup("slabel");
			SvgTextInTspan Elabel = container.newTextInTspan(label);
			Elabel.setX(this.x);
			Elabel.setY(this.y - (this.gel_image.get_spot_label_size() / 2));
			//Elabel.setTextContent(label);
		}

		return container;

	}

	public void setId(String spotIdentifier) {
		this.spotIdentifier = spotIdentifier;
	}

	private String getId() {
		return spotIdentifier;
	}

	public boolean isHighLighted() {
		return (this.is_highlighted);
	}

	public void setHighLight(boolean to_highlight) {
		this.is_highlighted = to_highlight;
	}

	public void setSymbol(SvgSymbol symbol) {
		this.symbol = symbol;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return (this.label);
	}
}
