package fr.inra.moulon.svgutils.svggelimage;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Vector;

import javax.imageio.ImageIO;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.svgutils.elements.SvgElementDrawBase;
import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgImage;
import fr.inra.moulon.utils.Base64;

public class SvgGelImageDocument extends SvgDocument {
	private Float svg_width;
	private Float svg_height;

	private Float gel_image_width;
	private Float gel_image_height;

	private Float asked_x_center;
	private Float asked_y_center;

	private Float origin_x;
	private Float origin_y;

	private SvgGroup gel_image_graph;
	private SvgGroup whole_drawing;

	private Vector<SvgSpot> arr_spots;

	private Integer _spot_label_size;

	private URL spotBaseUrl;
	private SvgGroup currentSpotGroup;
	private SvgSpotSymbol currentSymbol;
	private float zoomRatio;
	private SvgGroup groupSpots;
	private SvgGroup groupSpotsLabel;
	private SvgImage gelImage;

	public SvgGelImageDocument() throws Exception {
		zoomRatio = 1;
		arr_spots = new Vector<SvgSpot>();
		spotBaseUrl = null;

		origin_x = new Float(0);
		origin_y = new Float(0);

		svg_width = null;
		svg_height = null;

		asked_x_center = null;
		asked_y_center = null;

		gel_image_width = null;
		gel_image_height = null;
		// super(width, height);
		// svg_graphic = new SvgGraphics(gel_image_width, gel_image_height);
		// svg_graphic.setCssClass("svg", "");
		this.setCssClass("svg", "");
		// svg_graphic
		// .setCssClass(
		// "*",
		// "fill:none;fill-rule:evenodd;stroke:blue;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1");
		// font-size:20px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;writing-mode:lr-tb;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial
		this.setCssClass(
				"*",
				"fill:none;fill-rule:evenodd;stroke:blue;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;");

		// svg_graphic
		// .setCssClass(
		// "text",
		// "font-size:30px;text-anchor:middle;font-style:normal;font-weight:normal;font-family:Bitstream Vera Sans;stroke-opacity:1;fill-opacity:1");
		this.setCssClass(
				"text",
				"font-size:30px;text-anchor:middle;font-style:normal;font-weight:normal;font-family:Bitstream Vera Sans;stroke-opacity:1;fill-opacity:1");

		// svg_graphic.setCssClass("highlight", "fill:red;stroke:red;");

		// whole_drawing = svg_graphic.group("whole_drawing", "");
		whole_drawing = this.getSvgRoot().newGroup();
		whole_drawing.setId("whole_drawing");

		// svg_graphic.goToGroup("whole_drawing");

		// gel_image_graph = svg_graphic.group("gel_image", "");
		gel_image_graph = whole_drawing.newGroup();
		gel_image_graph.setId("gel_image");

		// svg_graphic.goToGroup("gel_image");
		// svg_graphic.group("spots", "");
		groupSpots = whole_drawing.newGroup();
		groupSpots.setId("spots");
		// this.currentSpotGroup = "spots";
		this.currentSpotGroup = groupSpots;
		// svg_graphic.group("slabel", "");
		groupSpotsLabel = whole_drawing.newGroup();
		groupSpotsLabel.setId("slabel");
		/*
		 * svg_graphic .setCssClass( ".slabel *",
		 * "fill:none;fill-rule:evenodd;stroke:blue;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
		 * );
		 */
		// define symbol :
		// Element cross = svg_graphic.symbol("cross", 0, 0, 1000, 1000,
		// "xMinYMin");
		this.currentSymbol = new SvgSpotSymbol(this, "cross");
		// SvgSymbol symbolCross = this.getSvgDefs().newSymbol("cross");
		this.currentSymbol.drawCross(new Float(6.0));
		// symbolCross
		// this.currentSymbol.setCss(null);
		// SvgSpotSymbol cross = new SvgSpotSymbol(this, "redcross");
		// //svg_graphic.symbol("redcross");

		_spot_label_size = 12;

		this.getSvgRoot().setPreserveAspectRatio("xMinYMin meet");
		// .setAttributeNS(null, "preserveAspectRatio", "xMinYMin meet");

	}

	public float getZoomRatio() {
		return zoomRatio;
	}

	public SvgSpotSymbol newSvgSpotSymbol(String id) {
		return new SvgSpotSymbol(this, id);
	}

	public void newSpotGroup(String spotGroupName, String classname,
			SvgSpotSymbol symbol) throws Exception {
		// svg_graphic.goToGroup("spots");
		this.currentSpotGroup = groupSpots.newGroup();
		this.currentSpotGroup.setId(spotGroupName);
		this.currentSpotGroup.setCssClass(classname);

		// svg_graphic.group(spotGroupName, classname);
		// this.currentSpotGroup = spotGroupName;
		if (symbol != null) {
			this.currentSymbol = symbol;
		}
	}

	protected SvgGroup getCurrentSpotGroup() {
		return currentSpotGroup;
	}

	public void setSpotBaseUrl(URL urlBase) {
		this.spotBaseUrl = urlBase;
	}

	public URL getSpotBaseUrl() {
		return this.spotBaseUrl;
	}

	public void setZoomRatio(Float zoomRatio) throws Exception {
		if (zoomRatio == null) {
			zoomRatio = this.svg_width / this.gel_image_width;
		}
		this.zoomRatio = zoomRatio;
	}

	public void setWidth(Float width) throws Exception {
		if (this.gel_image_width == null) {
			throw new Exception(
					"unable to set SVG width before setting gel image");
		}
		this.svg_width = width;

		if (this.svg_width == null) {
			this.svg_height = null;
		} else {
			if (width < 100) {
				this.svg_width = new Float(100);
			}
			if (width > gel_image_width) {
				this.svg_width = gel_image_width;
			}

			if (this.svg_height == null) {
				this.setHeight(svg_width * (gel_image_height / gel_image_width));
			}
		}
	}

	public void setHeight(Float height) throws Exception {
		if (this.gel_image_height == null) {
			throw new Exception(
					"unable to set SVG height before setting gel image");
		}
		this.svg_height = height;

		if (svg_height == null) {
			this.svg_width = null;
		} else {
			if (height < 100) {
				this.svg_height = new Float(100);
			}
			if (height > gel_image_height) {
				this.svg_height = gel_image_height;
			}
			if (this.svg_width == null) {
				this.setWidth(svg_height * (gel_image_width / gel_image_height));
			}
		}
	}

	public Float getXcenter() {
		return asked_x_center;
	}

	public Float getYcenter() {
		return asked_y_center;
	}

	public void setXcenter(float x) {
		asked_x_center = new Float(x);
		if (asked_y_center == null) {
			asked_y_center = new Float(0);
		}
		// System.out.println(asked_x_center);
	}

	public void setYcenter(float y) {
		asked_y_center = new Float(y);
		if (asked_x_center == null) {
			asked_x_center = new Float(0);
		}
	}

	public Vector<SvgSpot> getSpotList() {
		return this.arr_spots;
	}

	public Float getWidth() {
		if (svg_width == null) {
			return (this.gel_image_width);
		}
		return this.svg_width;
	}

	public Float getHeight() {
		if (svg_height == null) {
			return (this.gel_image_height);
		}
		return this.svg_height;
	}

	public Float getOrigineX() {
		return (this.origin_x);
	}

	public Float getOrigineY() {
		return (this.origin_y);
	}

	public SvgSpot addSvgSpot(float x, float y) throws Exception {
		SvgSpot the_spot = new SvgSpot(this, x, y, this.currentSymbol);
		this.arr_spots.add(the_spot);

		return the_spot;
	}

	public SvgSpot addSvgSpot(float x, float y, SvgSpotSymbol symbol)
			throws Exception {
		SvgSpot the_spot = new SvgSpot(this, x, y, symbol);
		this.arr_spots.add(the_spot);

		return the_spot;
	}

	private void draw() throws Exception {

		if (this.gel_image_width == null) {
			throw new Exception(
					"cannot draw gel image if gel_image_width is NULL ");
		}
		if (this.gel_image_height == null) {
			throw new Exception(
					"cannot draw gel image if gel_image_height is NULL ");
		}
		// set spot label font size :
		// SvgGroup slabel = svg_graphic.goToGroup("slabel");

		/*
		 * svg_graphic .setCssClass( ".slabelf *", "font-size:" +
		 * this.get_spot_label_size() +
		 * "px;text-anchor:middle;font-style:normal;font-weight:normal;fill:blue;fill-opacity:1;stroke:none;font-family:Bitstream Vera Sans;"
		 * );
		 */
		float realWidth = (gel_image_width * this.zoomRatio);
		if (asked_x_center != null) {
			// System.out.println("coucou");
			if (svg_width != null) {

				if (this.svg_width < this.gel_image_width) {
					// 800 => 242 (0.05)
					// float tmp = 0;
					this.origin_x = asked_x_center - (realWidth / 2);
					// - ((this.gel_image_width * this.zoomRatio) / 2);
					// System.out.println("coucou2 " + origin_x);
				}
				// zoomRatio = this.svg_width / this.gel_image_width;
				if (this.origin_x > gel_image_width - realWidth) {
					this.origin_x = gel_image_width - realWidth;
				}
				if (this.origin_x <= 0) {
					this.origin_x = new Float(0);
				}
			}
		}
		if (asked_y_center != null) {
			if (svg_height != null) {
				float realHeight = realWidth * (svg_height / svg_width);
				if (this.svg_height < this.gel_image_height) {
					this.origin_y = asked_y_center - (realHeight / 2);
				}
				if (this.origin_y > gel_image_height - realHeight) {
					this.origin_y = gel_image_height - realHeight;
				}
				if (this.origin_y <= 0) {
					this.origin_y = new Float(0);
				}
				/*
				 * if (svg_height > svg_width) { this.origin_y = this.origin_y -
				 * ((svg_height) * this.zoomRatioH); }else{ this.origin_y =
				 * this.origin_y + ((svg_height /2) * this.zoomRatioH); }
				 */
			}
		}

		this.whole_drawing.translate(-this.origin_x, -this.origin_y);

		if (svg_height != null) {
			this.getSvgRoot().setWidth(svg_width);
			this.getSvgRoot().setHeight(svg_height);
			// viewbox="0 0 90 90"
			this.getSvgRoot().setViewBox(
					0,
					0,
					this.gel_image_width * this.zoomRatio,
					(this.gel_image_width * this.zoomRatio) * svg_height
							/ svg_width);
		}
		// System.out.println(svg_width + " " + svg_height);
		for (int i = 0; i < this.arr_spots.size(); i++) {
			this.arr_spots.get(i).drawSvgSpot();
		}
	}

	public void embedGelImage(File filename) throws Exception {
		FileInputStream isFile = new FileInputStream(filename);

		try {
			SvgImage image = gel_image_graph.newImage(isFile);
			this.setGelImageWidth(image.getWidth());
			this.setGelImageHeight(image.getHeight());

			this.getSvgRoot().setWidth(gel_image_width);
			this.getSvgRoot().setHeight(gel_image_height);
			// .embedJpgImage(0, 0, width, height, img, "");
		} catch (Exception e) {
			throw new Exception("error embedding gel image");
		}

	}

	public void setGelImageUrl(URI image_url, float width, float height)
			throws Exception {
		if (image_url == null) {
			throw new Exception(
					"setGelImageUrl(URL image_url, float width, float height) image_url is NULL");
		}
		this.setGelImageWidth(width);
		this.setGelImageHeight(height);

		this.getSvgRoot().setWidth(gel_image_width);
		this.getSvgRoot().setHeight(gel_image_height);

		// gel_image_graph.image(0, 0, gel_image_width, gel_image_height,
		// image_url,
		// null);

		gel_image_graph.newImage(image_url, gel_image_width, gel_image_height);

	}

	public Float getGelImageHeight() {
		return this.gel_image_height;
	}

	public Float getGelImageWidth() {
		return this.gel_image_width;
	}

	private void setGelImageHeight(float height) {
		this.gel_image_height = new Float(height);
	}

	private void setGelImageWidth(float width) {
		this.gel_image_width = new Float(width);
	}

	public void embedGelImage(InputStream is) throws Exception {
		// BufferedImage img = ImageIO.read(is);
		// Integer width = img.getWidth(null);
		// Integer height = img.getHeight(null);

		// svg_graphic.goToGroup("gel_image");
		// width="300px" height="200px"
		// gel_image_graph.embedJpgImage(0, 0, width, height, img, "");
		gelImage = gel_image_graph.newImage(is);
		this.gel_image_width = gelImage.getWidth();
		this.gel_image_height = gelImage.getHeight();

		this.getSvgRoot().setWidth(gel_image_width);
		this.getSvgRoot().setHeight(gel_image_height);
	}

	/**
	 * 
	 * @return the image that was embeded previously
	 * @throws Exception
	 */
	public BufferedImage getEmbededGelBufferedImage() throws Exception {
		BufferedImage image_buf = null;

		String offset = "data:image/jpeg;base64,";

		String image_base64 = gelImage.getHref().substring(offset.length());

		// System.out.println("href attribute "+ image_base64);

		// .substring(offset.length());
		// for (int i = 0; i < image_list.getLength(); i++) {
		image_buf = ImageIO.read(new ByteArrayInputStream(Base64
				.decode(image_base64)));
		// }
		return image_buf;
	}

	public Integer get_spot_label_size() {
		if (gel_image_height == null) {
			return this._spot_label_size;
		} else {
			Float result = ((_spot_label_size * gel_image_height) / 1000);
			return (result.intValue());
		}
	}

	public SvgElementDrawBase getSpotLabelGroup() {
		return this.groupSpotsLabel;
	}

	@Override
	protected void render() {
		try {
			this.draw();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.render();

	}

}
