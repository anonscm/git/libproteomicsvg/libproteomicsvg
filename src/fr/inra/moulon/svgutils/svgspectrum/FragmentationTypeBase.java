package fr.inra.moulon.svgutils.svgspectrum;

import java.util.ArrayList;

abstract public class FragmentationTypeBase {
	SvgIon tmpIon = new SvgIon();
	float neutralPhosphoLoss = 	(float) 97.9769;//MOD:00696

	abstract public void addNterIons(ArrayList<SvgIon> iontable, int num,
			ArrayList<SvgAminoAcid> sequence, int exp_z);

	abstract public void addCterIons(ArrayList<SvgIon> iontable, int num,
			ArrayList<SvgAminoAcid> sequence, int exp_z);
	// CID => y, b, perte eau amine, a
	// ETD/ECD => c, z, sans perte, y (optionnel)

}