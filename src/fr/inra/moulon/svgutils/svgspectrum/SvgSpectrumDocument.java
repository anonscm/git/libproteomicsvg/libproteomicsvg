package fr.inra.moulon.svgutils.svgspectrum;

/**
 *   SvgSpectrum free java library to draw annotated MS spectrum
 Copyright (C) 2009  Olivier Langella

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 * 
 * @author Olivier Langella <olivier.langella@moulon.inra.fr
 * 
 */

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.svgutils.elements.SvgClipPath;
import fr.inra.moulon.svgutils.elements.SvgCoordXY;
import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgRect;
import fr.inra.moulon.svgutils.elements.SvgText;
import fr.inra.moulon.svgutils.elements.SvgTextInTspan;

public class SvgSpectrumDocument extends SvgDocument {

	private SvgGroup spectrum_graph;
	private SvgGroup svgspectrumtitle;
	private SvgGroup peaksGroup;

	// private Vector<Float> arr_mz;
	// private Vector<Float> arr_int;
	private float[] arr_mz;
	private float[] arr_int;
	private ArrayList<SvgIon> arr_ions = null;

	/**
	 * @brief associate peak index in the spectrum with SvgIon
	 * 
	 */
	private Hashtable<Integer, SvgIon> hash_assigned_peaks;
	private Hashtable<SvgIonSpecies, SvgGroup> hash_ion2group;

	// maximum intensity in this spectrum
	private float spectrumMaxIntensity;

	private float spectrum_width;
	private float spectrum_height;

	// min range to display spectrum
	private float spectrum_mz_min;
	// max range to display spectrum
	private float spectrum_mz_max;
	// max intensity inside the mz range
	private float spectrumRelativeMaxIntensity;

	private float transformYtranslate;

	private float transformYratio;

	private Float transformXtranslate;

	private float transformXratio;

	private float fitPrecision;
	private float delta = 0;

	/** threshold ratio : above this ratio, no need to print ion name */
	private float printThreshold;

	private boolean alreadyRendered;

	private SvgPeptideSequence sequence;
	private String title;

	float marginLeft;
	float marginRight;
	float marginBottom;
	float marginTop;
	private FragmentationTypeBase fragmentation = null;
	private ArrayList<SvgPeak> svgPeakList;
	private SvgRect clipRect;
	private SvgGroup scrollPeaks;
	private SvgGroup clipPeaks;
	protected SvgRect spectrumBackground;
	private float paddingLeft;
	private float paddingRight;
	private String visibilityHidden;

	/**
	 * @brief main class to produce spectrum SVG dom document
	 * 
	 * @param width
	 *            in pixels
	 * @param height
	 *            in pixels
	 * @throws Exception
	 */

	public SvgSpectrumDocument(float width, float height) throws Exception {

		super(width, height);
		// System.out.println("SvgSpectrumDocument(float width, float height) begin");
		visibilityHidden = "visibility:hidden;opacity:0;fill-opacity:0;stroke-opacity:0;";
		this.arr_mz = new float[0];
		this.arr_int = new float[0];
		this.paddingLeft = (float) 2;
		this.paddingRight = (float) 2;

		alreadyRendered = false;
		this.fitPrecision = (float) 0.5;
		this.sequence = null;
		this.title = null;
		this.printThreshold = 0;
		this.spectrumMaxIntensity = 0;
		this.spectrumRelativeMaxIntensity = 0;

		hash_assigned_peaks = new Hashtable<Integer, SvgIon>();
		// private Hashtable<SvgIon, SvgGroup> hash_ion2group;
		hash_ion2group = new Hashtable<SvgIonSpecies, SvgGroup>();
		// super(width, height);
		// svg_graphic = new SvgGraphics(width, height);
		// svg_graphic.setCssClass("svg", "");
		this.setCssClass("svg", "");
		/*
		 * svg_graphic .setCssClass( "*",
		 * "fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
		 * );
		 */
		this.setCssClass(
				"*",
				"fill:none;fill-rule:evenodd;stroke:#000000;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;");
		this.setCssClass("*", "stroke-width", "1px");

		// font-size:20px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;writing-mode:lr-tb;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial
		/*
		 * svg_graphic .setCssClass( "text",
		 * "font-size:10px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;writing-mode:lr-tb;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
		 * );
		 */
		/*
		 * svg_graphic .setCssClass( "text",
		 * "font-size:10px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;writing-mode:lr-tb;text-anchor:middle;fill:#000000;stroke:none;stroke-width:0px;stroke-linecap:butt;stroke-linejoin:miter;font-family:Arial;-inkscape-font-specification:Arial"
		 * );
		 */
		this.setCssClass(
				"text",
				"font-size:10px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;writing-mode:lr-tb;text-anchor:middle;fill:#000000;stroke:none;stroke-linecap:butt;stroke-linejoin:miter;font-family:Arial;-inkscape-font-specification:Arial;");
		this.setCssClass("text", "stroke-width", "0px");
		// tspan
		// {font-size:10px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial}
		/*
		 * svg_graphic .setCssClass( "tspan",
		 * "font-size:10px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
		 * );
		 */
		this.setCssClass(
				"tspan",
				"font-size:10px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;fill:#000000;fill-opacity:1;stroke:none;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial;");
		this.setCssClass("tspan", "stroke-width", "0px");
		// svg_graphic.group("svgspectrumtitle", "");
		svgspectrumtitle = this.getSvgRoot().newGroup();
		svgspectrumtitle.setId("svgspectrumtitle");

		// svg_graphic.group("spectrum", "");
		SvgClipPath clip = this.getSvgDefs().newSvgClipPath("spectrumZoom");
		clipRect = clip.newRect(spectrum_width, this.getHeight());
		clipRect.setId("spectrumZoomRect");

		spectrum_graph = this.getSvgRoot().newGroup();
		spectrum_graph.setId("spectrum");
		spectrumBackground = spectrum_graph.newRect(this.spectrum_width,
				this.spectrum_height);

		SvgGroup spectrum_graph_clip = spectrum_graph.newGroup();
		spectrum_graph_clip.setId("spectrumClip");
		clipPeaks = spectrum_graph_clip.newGroup();
		clipPeaks.setClipPath(clip);

		scrollPeaks = clipPeaks.newGroup();
		scrollPeaks.setId("scrollPeaks");

		peaksGroup = scrollPeaks.newGroup();

		/*
		 * peaksGroup = spectrum_graph.newGroup(); this.scrollPeaks =
		 * peaksGroup; clipPeaks = peaksGroup.newGroup();
		 * clipPeaks.setClipPath(clip);
		 */

		peaksGroup.setId("peaks");
		peaksGroup.setCssClass("peak");

		// svg_graphic.setCssClass(".peak *","stroke:black;fill:black;fill-opacity:1;");
		this.setCssClass(".peak *", "stroke:black;fill:black;fill-opacity:1;");

		SvgIonSpecies svgIon = SvgIonSpecies.getA1Instance();
		// svg_graphic.group("a1", "");
		SvgGroup a1Group = scrollPeaks.newGroup();
		a1Group.setId("a1");
		hash_ion2group.put(svgIon, a1Group);
		/*
		 * svg_graphic.setCssClass("#a1 *", "stroke:" + svgIon.getCssColor() +
		 * ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		 */
		this.setCssClass("#a1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#a1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#a1 *", "fill-opacity", "1");

		// svg_graphic.setCssClass("#a1 text", visibilityHidden);
		this.setCssClass("#a1 text", visibilityHidden);
		// svg_graphic.group("c1", "");
		SvgGroup c1Group = scrollPeaks.newGroup();
		c1Group.setId("c1");
		hash_ion2group.put(SvgIonSpecies.getC1Instance(), c1Group);
		svgIon = SvgIonSpecies.getC1Instance();
		this.setCssClass("#c1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#c1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#c1 *", "fill-opacity", "1");

		// svg_graphic.group("c2", "");
		SvgGroup c2Group = scrollPeaks.newGroup();
		c2Group.setId("c2");
		hash_ion2group.put(SvgIonSpecies.getC2Instance(), c2Group);
		svgIon = SvgIonSpecies.getC2Instance();
		this.setCssClass("#c2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#c2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#c2 *", "fill-opacity", "1");

		// svg_graphic.group("z1", "");
		SvgGroup z1Group = scrollPeaks.newGroup();
		z1Group.setId("z1");
		hash_ion2group.put(SvgIonSpecies.getZ1Instance(), z1Group);
		svgIon = SvgIonSpecies.getZ1Instance();
		this.setCssClass("#z1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#z1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#z1 *", "fill-opacity", "1");

		// svg_graphic.group("z2", "");
		SvgGroup z2Group = scrollPeaks.newGroup();
		z2Group.setId("z2");
		hash_ion2group.put(SvgIonSpecies.getZ2Instance(), z2Group);
		svgIon = SvgIonSpecies.getZ2Instance();
		this.setCssClass("#z2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#z2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#z2 *", "fill-opacity", "1");

		// svg_graphic.group("bO2", "");
		SvgGroup bO2Group = scrollPeaks.newGroup();
		bO2Group.setId("bO2");
		svgIon = SvgIonSpecies.getBO2Instance();
		hash_ion2group.put(svgIon, bO2Group);
		// svg_graphic.setCssClass("#bO2 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#bO2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#bO2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#bO2 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#bO2 text", visibilityHidden);
		this.setCssClass("#bO2 text", visibilityHidden);

		svgIon = SvgIonSpecies.getBO1Instance();
		// svg_graphic.group("bO1", "");
		SvgGroup bO1Group = scrollPeaks.newGroup();
		bO1Group.setId("bO1");
		hash_ion2group.put(svgIon, bO1Group);
		// svg_graphic.setCssClass("#bO1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#bO1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#bO1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#bO1 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#bO1 text", visibilityHidden);
		this.setCssClass("#bO1 text", visibilityHidden);

		svgIon = SvgIonSpecies.getBstar2Instance();
		// svg_graphic.group("bstar2", "");
		SvgGroup bstar2Group = scrollPeaks.newGroup();
		bstar2Group.setId("bstar2");
		hash_ion2group.put(svgIon, bstar2Group);
		// svg_graphic.setCssClass("#bstar2 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#bstar2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#bstar2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#bstar2 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#bstar2 text", visibilityHidden);
		this.setCssClass("#bstar2 text", visibilityHidden);

		svgIon = SvgIonSpecies.getBstar1Instance();
		// svg_graphic.group("bstar1", "");
		SvgGroup bstar1Group = scrollPeaks.newGroup();
		bstar1Group.setId("bstar1");
		hash_ion2group.put(svgIon, bstar1Group);
		// svg_graphic.setCssClass("#bstar1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#bstar1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#bstar1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#bstar1 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#bstar1 text", visibilityHidden);
		this.setCssClass("#bstar1 text", visibilityHidden);

		svgIon = SvgIonSpecies.getYO2Instance();
		// svg_graphic.group("yO2", "");
		SvgGroup yO2Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, yO2Group);
		yO2Group.setId("yO2");
		// svg_graphic.setCssClass("#yO2 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#yO2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#yO2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#yO2 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#yO2 text", visibilityHidden);
		this.setCssClass("#yO2 text", visibilityHidden);

		svgIon = SvgIonSpecies.getYO1Instance();
		// svg_graphic.group("yO1", "");
		SvgGroup yO1Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, yO1Group);
		yO1Group.setId("yO1");
		// svg_graphic.setCssClass("#yO1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#yO1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#yO1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#yO1 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#yO1 text", visibilityHidden);
		this.setCssClass("#yO1 text", visibilityHidden);

		svgIon = SvgIonSpecies.getYstar1Instance();
		// svg_graphic.group("ystar1", "");
		SvgGroup ystar1Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, ystar1Group);
		ystar1Group.setId("ystar1");
		// svg_graphic.setCssClass("#ystar1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#ystar1 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#ystar1 *", "fill", svgIon.getCssColor());
		this.setCssClass("#ystar1 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#ystar1 text", visibilityHidden);
		this.setCssClass("#ystar1 text", visibilityHidden);

		svgIon = SvgIonSpecies.getYstar2Instance();
		// svg_graphic.group("ystar2", "");
		SvgGroup ystar2Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, ystar2Group);
		ystar2Group.setId("ystar2");
		// svg_graphic.setCssClass("#ystar2 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#ystar2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#ystar2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#ystar2 *", "fill-opacity", "1");
		// svg_graphic.setCssClass("#ystar2 text", visibilityHidden);
		this.setCssClass("#ystar2 text", visibilityHidden);

		svgIon = SvgIonSpecies.getB2Instance();
		// svg_graphic.group("b2", "");
		SvgGroup b2Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, b2Group);
		b2Group.setId("b2");
		// svg_graphic.setCssClass("#b2 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#b2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#b2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#b2 *", "fill-opacity", "1");
		this.setCssClass(".b2", "stroke-width", "0px");
		this.setCssClass(".b2", "stroke", svgIon.getCssColor());
		this.setCssClass(".b2", "fill", svgIon.getCssColor());
		this.setCssClass(".b2", "fill-opacity", "1");

		svgIon = SvgIonSpecies.getY2Instance();
		// svg_graphic.group("y2", "");
		SvgGroup y2Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, y2Group);
		y2Group.setId("y2");
		// svg_graphic.setCssClass("#y2 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("#y2 *", "stroke", svgIon.getCssColor());
		this.setCssClass("#y2 *", "fill", svgIon.getCssColor());
		this.setCssClass("#y2 *", "fill-opacity", "1");
		this.setCssClass(".y2", "stroke-width", "0px");
		this.setCssClass(".y2", "stroke", svgIon.getCssColor());
		this.setCssClass(".y2", "fill", svgIon.getCssColor());
		this.setCssClass(".y2", "fill-opacity", "1");

		svgIon = SvgIonSpecies.getB1Instance();
		// svg_graphic.group("b1", "b1");
		SvgGroup b1Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, b1Group);
		b1Group.setId("b1");
		b1Group.setCssClass("b1");
		// svg_graphic.setCssClass(".b1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass(".b1 *", "stroke", svgIon.getCssColor());
		this.setCssClass(".b1 *", "fill", svgIon.getCssColor());
		this.setCssClass(".b1 *", "fill-opacity", "1");
		// svg_graphic.setCssClass(".b1",
		// "stroke-width:0px;stroke:" + svgIon.getCssColor() + ";fill:"
		// + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass(".b1", "stroke-width", "0px");
		this.setCssClass(".b1", "stroke", svgIon.getCssColor());
		this.setCssClass(".b1", "fill", svgIon.getCssColor());
		this.setCssClass(".b1", "fill-opacity", "1");

		svgIon = SvgIonSpecies.getBP1Instance(1);
		// svg_graphic.group("b1", "b1");
		SvgGroup bP1Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, bP1Group);
		String className = svgIon.getCssClassname();
		bP1Group.setId(className);
		bP1Group.setCssClass(className);
		// svg_graphic.setCssClass(".b1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className + " *", "stroke", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill-opacity", "1");
		// svg_graphic.setCssClass(".b1",
		// "stroke-width:0px;stroke:" + svgIon.getCssColor() + ";fill:"
		// + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className, "stroke-width", "0px");
		this.setCssClass("." + className, "stroke", svgIon.getCssColor());
		this.setCssClass("." + className, "fill", svgIon.getCssColor());
		this.setCssClass("." + className, "fill-opacity", "1");
		for (int i = 2; i < 6; i++) {
			svgIon = SvgIonSpecies.getBP1Instance(i);
			hash_ion2group.put(svgIon, bP1Group);
		}

		svgIon = SvgIonSpecies.getBP2Instance(1);
		// svg_graphic.group("b1", "b1");
		SvgGroup bP2Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, bP2Group);
		className = svgIon.getCssClassname();
		bP2Group.setId(className);
		bP2Group.setCssClass(className);
		// svg_graphic.setCssClass(".b1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className + " *", "stroke", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill-opacity", "1");
		// svg_graphic.setCssClass(".b1",
		// "stroke-width:0px;stroke:" + svgIon.getCssColor() + ";fill:"
		// + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className, "stroke-width", "0px");
		this.setCssClass("." + className, "stroke", svgIon.getCssColor());
		this.setCssClass("." + className, "fill", svgIon.getCssColor());
		this.setCssClass("." + className, "fill-opacity", "1");
		for (int i = 2; i < 6; i++) {
			svgIon = SvgIonSpecies.getBP2Instance(i);
			hash_ion2group.put(svgIon, bP2Group);
		}

		// visibility: hidden
		svgIon = SvgIonSpecies.getY1Instance();
		// svg_graphic.group("y1", "y1");
		SvgGroup y1Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, y1Group);
		y1Group.setId("y1");
		y1Group.setCssClass("y1");
		// svg_graphic.setCssClass(".y1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass(".y1 *", "stroke", svgIon.getCssColor());
		this.setCssClass(".y1 *", "fill", svgIon.getCssColor());
		this.setCssClass(".y1 *", "fill-opacity", "1");
		// svg_graphic.setCssClass(".y1",
		// "stroke-width:0px;stroke:" + svgIon.getCssColor() + ";fill:"
		// + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass(".y1", "stroke-width", "0px");
		this.setCssClass(".y1", "stroke", svgIon.getCssColor());
		this.setCssClass(".y1", "fill", svgIon.getCssColor());
		this.setCssClass(".y1", "fill-opacity", "1");

		svgIon = SvgIonSpecies.getYP1Instance(1);
		// svg_graphic.group("y1", "y1");
		SvgGroup yP1Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, yP1Group);
		className = svgIon.getCssClassname();
		yP1Group.setId(className);
		yP1Group.setCssClass(className);
		// svg_graphic.setCssClass(".y1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className + " *", "stroke", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill-opacity", "1");
		// svg_graphic.setCssClass(".y1",
		// "stroke-width:0px;stroke:" + svgIon.getCssColor() + ";fill:"
		// + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className, "stroke-width", "0px");
		this.setCssClass("." + className, "stroke", svgIon.getCssColor());
		this.setCssClass("." + className, "fill", svgIon.getCssColor());
		this.setCssClass("." + className, "fill-opacity", "1");
		for (int i = 2; i < 6; i++) {
			svgIon = SvgIonSpecies.getYP1Instance(i);
			hash_ion2group.put(svgIon, yP1Group);
		}

		svgIon = SvgIonSpecies.getYP2Instance(1);
		// svg_graphic.group("y1", "y1");
		SvgGroup yP2Group = scrollPeaks.newGroup();
		hash_ion2group.put(svgIon, yP2Group);
		className = svgIon.getCssClassname();
		yP2Group.setId(className);
		yP2Group.setCssClass(className);
		// svg_graphic.setCssClass(".y1 *", "stroke:" + svgIon.getCssColor()
		// + ";fill:" + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className + " *", "stroke", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill", svgIon.getCssColor());
		this.setCssClass("." + className + " *", "fill-opacity", "1");
		// svg_graphic.setCssClass(".y1",
		// "stroke-width:0px;stroke:" + svgIon.getCssColor() + ";fill:"
		// + svgIon.getCssColor() + ";fill-opacity:1;");
		this.setCssClass("." + className, "stroke-width", "0px");
		this.setCssClass("." + className, "stroke", svgIon.getCssColor());
		this.setCssClass("." + className, "fill", svgIon.getCssColor());
		this.setCssClass("." + className, "fill-opacity", "1");
		for (int i = 2; i < 6; i++) {
			svgIon = SvgIonSpecies.getYP2Instance(i);
			hash_ion2group.put(svgIon, yP2Group);
		}

	}

	public float getDelta() {
		return delta;
	}

	public void setDelta(float delta) {
		this.delta = delta;
	}

	/**
	 * \brief sets the ratio beyond which the ion label is not printed
	 * 
	 * @param printThresholdRatio
	 *            (value between 0 and 1)
	 * @throws Exception
	 */
	public void setPrintThreshold(float newPrintThreshold) throws Exception {
		if ((newPrintThreshold < 0) || ((newPrintThreshold > 1))) {
			throw new Exception("unable to set printThreshold of "
					+ newPrintThreshold
					+ ": it must be a value between 0 and 1");
		}
		this.printThreshold = newPrintThreshold;
	}

	public void setIonTable(ArrayList<SvgIon> arrayList) throws Exception {
		if (arrayList == null) {
			throw new Exception("arr_ions is null");
		}
		this.arr_ions = arrayList;
	}

	public HashSet<SvgIon> getIonTable() {
		HashSet<SvgIon> arrReturn = new HashSet<SvgIon>();
		for (SvgIon ion : this.arr_ions) {
			arrReturn.add(ion);
		}
		return arrReturn;
	}

	private void assignPeaks() throws Exception {
		if (arr_ions == null) {
			throw new Exception("arr_ions is null");
		}
		hash_assigned_peaks = new Hashtable<Integer, SvgIon>();
		for (SvgIon ion : this.arr_ions) {
			this.findBestPeakForIon(ion);
		}
		Set<Entry<Integer, SvgIon>> lesEntrees = this.hash_assigned_peaks
				.entrySet();
		Iterator<Entry<Integer, SvgIon>> it = lesEntrees.iterator();
		while (it.hasNext()) {

			Entry<Integer, SvgIon> e = (Map.Entry<Integer, SvgIon>) it.next();
			if (this.sequence != null) {
				this.sequence.addAssignedIon(e.getValue());
			}
		}

	}

	/**
	 * @brief return the hash table that associates peak indexes to svgions
	 * 
	 * @return Hashtable<Integer, SvgIon>
	 */
	Hashtable<Integer, SvgIon> getAssignedPeaks() {
		return this.hash_assigned_peaks;
	}

	private void findBestPeakForIon(SvgIon ion) {
		float mzmin = (float) (ion.getMz() - this.fitPrecision + delta);
		float mzmax = (float) (ion.getMz() + this.fitPrecision + delta);

		// float mindeviation = 99999;
		// float currentdeviation;
		float bestpeak_intensity = 0;
		Integer bestpeak = null;

		int i = 0;

		for (float curMz : this.arr_mz) {
			if (mzmin <= curMz) {
				if (curMz <= mzmax) {

					// currentdeviation = (this.arr_mz.get(i) - ion.getMz());
					// currentdeviation *= currentdeviation;
					// if (currentdeviation < mindeviation) {
					if (this.arr_int[i] > bestpeak_intensity) {
						bestpeak = i;
						bestpeak_intensity = this.arr_int[i];
						// mindeviation = currentdeviation;
					}
				}
			}
			i++;
		}

		if (bestpeak != null) {
			if (this.hash_assigned_peaks.containsKey(bestpeak)) {
				if (ion.priority_over(this.hash_assigned_peaks.get(bestpeak))) {
					this.hash_assigned_peaks.get(bestpeak)
							.setAssignedPeakIntensityRatio(null);
					this.hash_assigned_peaks.put(bestpeak, ion);
					ion.setAssignedPeakIntensityRatio(this.arr_int[bestpeak]
							/ this.spectrumMaxIntensity);
				}
				return;
			}
			this.hash_assigned_peaks.put(bestpeak, ion);
			ion.setAssignedPeakIntensityRatio(this.arr_int[bestpeak]
					/ this.spectrumMaxIntensity);

		}
	}

	private void setAbsoluteSpectrumDescriptiveVars() {

		// **********************************
		// set margins
		this.updateMargin();

		spectrum_mz_min = this.arr_mz[0];
		for (float mz : this.arr_mz) {
			if (spectrum_mz_min > mz) {
				spectrum_mz_min = mz;
			}
		}
		spectrum_mz_max = spectrum_mz_min;

		// System.out.println("spectrum_mz_min " + spectrum_mz_min);
		for (float mz : this.arr_mz) {
			if (spectrum_mz_max < mz) {
				spectrum_mz_max = mz;
			}
		}
		// System.out.println("spectrum_mz_max " + spectrum_mz_max);

		this.autoScaleIntensity();
		setXratio();

		this.spectrumMaxIntensity = this.spectrumRelativeMaxIntensity;

		// System.out.println("spectrumMaxIntensity " + spectrumMaxIntensity);
		// spectrum_max_intensity = this
	}

	/**
	 * @brief set mz and intesity for this spectrum
	 * 
	 * @param arr_d_mz
	 * @param arr_d_int
	 */
	public void setMzIntensity(ArrayList<Double> varr_mz,
			ArrayList<Double> varr_int) {
		if (varr_mz == null) {
			return;
		}
		this.arr_mz = new float[varr_mz.size()];
		this.arr_int = new float[varr_int.size()];
		int i = 0;
		for (Double mz : varr_mz) {
			this.arr_mz[i] = mz.floatValue();
			this.arr_int[i] = varr_int.get(i).floatValue();
			// System.out.println(this.arr_int[i]);
			i++;
		}
		setAbsoluteSpectrumDescriptiveVars();
	}

	public void setMzIntensity(Vector<Float> varr_mz, Vector<Float> varr_int) {
		if (varr_mz == null) {
			return;
		}
		this.arr_mz = new float[varr_mz.size()];
		this.arr_int = new float[varr_int.size()];
		int i = 0;
		for (Float mz : varr_mz) {
			this.arr_mz[i] = mz;
			this.arr_int[i] = varr_int.get(i);
			// System.out.println(this.arr_int[i]);
			i++;
		}
		setAbsoluteSpectrumDescriptiveVars();
	}

	public void setMzIntensity(double[] in_arr_mz, double[] in_arr_int) {

		if (arr_mz == null) {
			return;
		}
		this.arr_mz = new float[in_arr_mz.length];
		this.arr_int = new float[in_arr_int.length];
		int i = 0;
		for (double mz : in_arr_mz) {
			this.arr_mz[i] = (float) mz;
			this.arr_int[i] = (float) in_arr_int[i];
			// System.out.println(this.arr_int[i]);
			i++;
		}
		setAbsoluteSpectrumDescriptiveVars();
	}

	public void setMzIntensity(float[] arr_mz2, float[] arr_int2) {
		this.arr_mz = arr_mz2;
		this.arr_int = arr_int2;
		setAbsoluteSpectrumDescriptiveVars();

	}

	private void drawPeaks() throws Exception {

		if (alreadyRendered) {
			return;
		}
		alreadyRendered = true;

		// **********************************
		// set margins
		this.updateMargin();

		// **********************************
		// draw peaks
		this.getSvgPeakList();
		for (SvgPeak svgPeak : this.svgPeakList) {
			// Element ionlabel =
			if ((svgPeak.getIntensity() / this.spectrumRelativeMaxIntensity) > printThreshold) {
				svgPeak.printIonName();
			}
		}

		// **********************************
		// write axis

		// svg_graphic.goToGroup("spectrum");
		// Y axis :
		this.spectrum_graph.newLine(-1, 0, -1, spectrum_height);
		// svg_graphic.line(-1, 0, -1, spectrum_height, "");
		// X axis :
		// svg_graphic.line(-1, spectrum_height, spectrum_width + 1,
		// spectrum_height, "");
		this.spectrum_graph.newLine(-1, spectrum_height, spectrum_width + 1,
				spectrum_height);

		clipRect.setWidth(spectrum_width);
		clipRect.setHeight(this.getHeight());
		spectrumBackground.setWidth(spectrum_width);
		spectrumBackground.setHeight(spectrum_height);
		spectrumBackground.setCssClass("white_rect");
		this.setCssClass(".white_rect",visibilityHidden);

		// **********************************
		// write title

		if (this.title != null) {
			// svg_graphic.goToGroup("svgspectrumtitle");
			// svg_graphic.text((spectrum_width / 2) + this.marginLeft, 30,
			// title,
			// "");
			SvgTextInTspan titleText = this.svgspectrumtitle
					.newTextInTspan(title);
			titleText.setX((spectrum_width / 2) + this.marginLeft);
			titleText.setY(30);

			// svg_graphic.setCssClass("#svgspectrumtitle tspan",
			// "font-size:22px;stroke:#000000;fill:#000000;");
			this.setCssClass("#svgspectrumtitle tspan", "font-size", "22px");
			this.setCssClass("#svgspectrumtitle tspan", "stroke", "#000000");
			this.setCssClass("#svgspectrumtitle tspan", "fill", "#000000");
			this.setCssClass("#svgspectrumtitle tspan", "stroke-width", "0px");
		}

		// **********************************
		// write sequence

		if (this.sequence != null) {
			// svg_graphic.goToGroup("spectrum");

			SvgGroup sequenceGroup = this.spectrum_graph.newGroup();
			sequenceGroup.setId("svgsequence");
			this.setCssClass("#svgsequence tspan", "font-size",
					"" + sequence.getFontSize() + "px");
			this.setCssClass("#svgsequence tspan", "stroke", "#000000");
			this.setCssClass("#svgsequence tspan", "fill", "#000000");
			this.setCssClass("#svgsequence tspan", "text-anchor", "middle");
			this.setCssClass("#svgsequence tspan", "stroke-width", "0px");

			this.sequence.drawSvgPeptideSequence(sequenceGroup,
					((spectrum_width / 2) - (sequence.svgWidth()) / 2), -30);
			// svg_graphic.text((spectrum_width / 2) - (((sequence.size()-1) *
			// 20)/2), 0, sequence.toString(), "",20);
		}

		// **********************************
		// write interval count on X axis
		// svg_graphic.goToGroup("spectrum");
		// svg_graphic.text(this.spectrum_width / 2, spectrum_height + 25,
		// "m/z",
		// "");
		this.setCssClass(".xaxislabel", "stroke", "#000000");
		this.setCssClass(".xaxislabel", "fill", "#000000");
		this.setCssClass(".xaxislabel", "stroke-width", "0px");
		SvgText xLabel = this.spectrum_graph.newText("m/z");
		xLabel.setX(this.spectrum_width / 2);
		xLabel.setY(spectrum_height + 25);
		xLabel.setCssClass("xaxislabel");

		int big_tic_int = 10000;

		float delta = this.spectrum_mz_max - this.spectrum_mz_min;
		while (big_tic_int > 10) {
			if ((delta / big_tic_int) > 2) {
				break;
			}
			big_tic_int = big_tic_int / 10;
		}
		if (big_tic_int >= 10) {
			int small_tic_int = (int) Math.floor(big_tic_int / 10);

			int begin_tic = (int) ((Math.floor(this.spectrum_mz_min
					/ big_tic_int) + 1) * big_tic_int);

			for (int i = begin_tic; i < this.spectrum_mz_max; i += big_tic_int) {
				// draw
				String label = "" + i;
				float pt_x = this.mzToX(i);
				// draw a line
				// svg_graphic.line(pt_x, spectrum_height, pt_x,
				// spectrum_height + 5, "");
				scrollPeaks.newLine(pt_x, spectrum_height, pt_x,
						spectrum_height + 5);
				// imageline($image, $pt_x, $y_end, $pt_x, $y_end +5, $grey);

				// svg_graphic.text(pt_x, spectrum_height + 15, label, "");
				SvgText mzLabel = scrollPeaks.newText(label);
				mzLabel.setCssClass("xaxislabel");
				mzLabel.setX(pt_x);
				mzLabel.setY(spectrum_height + 15);

				// imagestring($image, 4, $pt_x - (strlen($string) *
				// $this->_char_width) / 2, $y_end + ($this->_char_height *
				// 0.5), $string, $black);

			}

			begin_tic = (int) ((Math
					.floor(this.spectrum_mz_min / small_tic_int) + 1) * small_tic_int);

			for (int i = begin_tic; i < this.spectrum_mz_max; i += small_tic_int) {
				// draw
				float pt_x = this.mzToX(i);
				// $pt_x = $x_start + ($x_end - $x_start) * ($i - $min_x) /
				// ($max_x - $min_x);
				// svg_graphic.line(pt_x, spectrum_height, pt_x,
				// spectrum_height + 2, "");
				scrollPeaks.newLine(pt_x, spectrum_height, pt_x,
						spectrum_height + 2);
				// imageline($image, $pt_x, $y_end, $pt_x, $y_end +2, $grey);
			}
		}
		// end write interval count on X axis
		// *************************

		// **********************************
		// write interval count on Y axis
		// svg_graphic.goToGroup("spectrum");
		// exponential
		// svg_graphic.setCssClass("tspan.exponential","baseline-shift:super;");
		// svg_graphic.setCssClass("text.yaxislabel", "text-anchor:end;");
		this.setCssClass("text.yaxislabel tspan", "text-anchor", "end");
		this.setCssClass("text.yaxislabel tspan.msup", "baseline-shift",
				"super");

		// Element ylabel = svg_graphic.text(-30, spectrum_height / 2,
		// "intensity", "");
		SvgText ylabel = spectrum_graph.newText("intensity");
		ylabel.setX(-30);
		ylabel.setY(spectrum_height / 2);
		ylabel.rotate(270, -30, spectrum_height / 2);

		big_tic_int = 1000000;

		delta = this.spectrumRelativeMaxIntensity - 0;
		while (big_tic_int > 10) {
			if ((delta / big_tic_int) > 2) {
				break;
			}
			big_tic_int = big_tic_int / 10;
		}
		DecimalFormat formScientific = (DecimalFormat) NumberFormat
				.getIntegerInstance();
		formScientific.applyPattern("##0E0");
		DecimalFormat formClassic = (DecimalFormat) NumberFormat
				.getIntegerInstance();
		// formClassic.applyPattern("##0E0");
		if (big_tic_int >= 10) {
			int small_tic_int = (int) Math.floor(big_tic_int / 10);

			float begin_tic = (int) ((Math.floor(0 / big_tic_int) + 1) * big_tic_int);

			for (Float ipos = begin_tic; ipos < this.spectrumRelativeMaxIntensity; ipos += big_tic_int) {
				// draw
				// String label = "";
				float pt_y = this.intToY(ipos);
				// draw a line
				// svg_graphic.line(-1, pt_y, -6, pt_y, "");
				spectrum_graph.newLine(-1, pt_y, -6, pt_y);

				// svg_graphic.scientificNumber(-7, pt_y + 3, label,
				// "yaxislabel");
				SvgText intensityLabel = spectrum_graph.newText();
				if (ipos < 10001) {
					// label = formClassic.format(ipos);
					intensityLabel.printNumber(ipos, formClassic);
				} else {
					// label = formScientific.format(ipos);
					intensityLabel.printNumber(ipos, formScientific);
				}
				intensityLabel.setCssClass("yaxislabel");
				intensityLabel.setX(-7);
				intensityLabel.setY(pt_y + 3);

			}

			begin_tic = (int) ((Math.floor(0 / small_tic_int) + 1) * small_tic_int);

			for (Float ipos = begin_tic; ipos < this.spectrumRelativeMaxIntensity; ipos += small_tic_int) {
				// draw
				float pt_y = this.intToY(ipos);
				// svg_graphic.line(-1, pt_y, -3, pt_y, "");
				spectrum_graph.newLine(-1, pt_y, -3, pt_y);
			}
		}
		// end write interval count on Y axis
		// *************************

	}

	public ArrayList<SvgPeak> getSvgPeakList() throws Exception {
		if (this.svgPeakList == null) {
			updateMargin();
			this.svgPeakList = new ArrayList<SvgPeak>();
			if (arr_ions != null) {
				this.assignPeaks();
			}

			int i = 0;
			for (float curMz : this.arr_mz) {
				if ((curMz >= this.spectrum_mz_min)
						&& (curMz <= this.spectrum_mz_max)) {
					SvgPeak currentPeak = new SvgPeak(this, i);
					svgPeakList.add(currentPeak);
				}

				i++;
			}
		}
		return this.svgPeakList;
	}

	private void updateMargin() {
		marginLeft = (float) (60);
		marginRight = (float) (30);
		// float hmargin = (float) (height * 0.05);
		marginBottom = (float) (30);
		marginTop = (float) (30);
		if (this.sequence != null) {
			marginTop += 40;
		}
		if (this.title != null) {
			marginTop += 30;
		}

		spectrum_height = this.getHeight() - (marginBottom + marginTop);
		spectrum_width = this.getWidth() - (marginLeft + marginRight);

		float ratiox = 1;
		float ratioy = 1;
		// 1.0,0.0,0.001,1.0,60.0,70.0
		/*
		 * spectrum_graph.transformMatrix(ratiox, 0, 0, ratioy, marginLeft,
		 * marginTop);
		 */
		spectrum_graph.transformMatrix(ratiox, 0, (float) 0.001, ratioy,
				marginLeft, marginTop);
		clipRect.setY(-marginTop);
		clipRect.setX(0);
		autoScaleIntensity();

	}

	float intToY(float intensity) {
		intensity = intensity + this.transformYtranslate;
		intensity = intensity * this.transformYratio;
		return (spectrum_height - intensity);
	}

	float mzToX(float mz) {
		mz = mz + this.transformXtranslate;
		mz = mz * this.transformXratio;
		return mz;
	}

	private float getRelativeMaxIntensityValue() {
		float maxIntensity = 0;
		if (this.arr_int.length == 0) {
			maxIntensity = 0;
		} else {
			for (int i = 0; i < arr_int.length; i++) {
				if (this.arr_mz[i] >= this.spectrum_mz_min) {
					if (this.arr_mz[i] <= this.spectrum_mz_max) {
						if (arr_int[i] > maxIntensity) {
							maxIntensity = arr_int[i];
						}
					}
				}
			}
		}
		return maxIntensity;
	}

	private void autoScaleIntensity() {

		this.spectrumRelativeMaxIntensity = getRelativeMaxIntensityValue();

		this.transformYtranslate = 0;
		this.transformYratio = this.spectrum_height
				/ spectrumRelativeMaxIntensity;

	}

	public void setSpectrumRangeMz(float mzMin, float mzMax) {
		spectrum_mz_min = mzMin;
		spectrum_mz_max = mzMax;

		// this.updateMargin();
		this.autoScaleIntensity();
		this.setXratio();
	}

	private void setXratio() {
		this.transformXtranslate = -spectrum_mz_min + paddingLeft;
		this.transformXratio = this.spectrum_width
				/ (spectrum_mz_max - spectrum_mz_min + paddingLeft + paddingRight);
	}

	@Override
	protected void render() {
		try {
			this.drawPeaks();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.render();

	}

	public void setSequence(SvgPeptideSequence monPeptide) throws Exception {
		// guess the fragmentation model
		FragmentationTypeCID cidModel = FragmentationTypeCID.getInstance();
		FragmentationTypeETD etdModel = FragmentationTypeETD.getInstance();
		// System.out.println("coucou2");

		this.setSequence(etdModel, monPeptide);
		Enumeration<SvgIon> list = this.getAssignedPeaks().elements();

		int countZ = 0;
		int countC = 0;
		while (list.hasMoreElements()) {
			SvgIon svgIon = list.nextElement();
			if (svgIon.getSvgIonSpecies().getType().equals("c")) {
				countC++;
			} else if (svgIon.getSvgIonSpecies().getType().equals("z")) {
				countZ++;
			}

		}
		this.setSequence(cidModel, monPeptide);
		list = this.getAssignedPeaks().elements();

		int countB = 0;
		int countY = 0;
		while (list.hasMoreElements()) {
			SvgIon svgIon = list.nextElement();
			// System.out
			// .println("coucou3 " + svgIon.getSvgIonSpecies().getType());
			if (svgIon.getSvgIonSpecies().getType().equals("b")) {
				countB++;
			} else if (svgIon.getSvgIonSpecies().getType().equals("y")) {
				countY++;
			}

		}
		// System.out.println("coucou1 " + countC + " " + countB);
		if ((countC + countZ) > (countB + countY)) {
			// back to ETD;
			// System.out.println("coucou");
			this.setSequence(etdModel, monPeptide);
		}
	}

	public void setSequence(FragmentationTypeBase fragmentation,
			SvgPeptideSequence monPeptide) throws Exception {
		this.fragmentation = fragmentation;
		if (this.fragmentation == null) {
			throw new Exception(
					"Please set a fragmentation model for this spectrum");
		}
		this.sequence = monPeptide;
		this.setIonTable(monPeptide.getIonTable(fragmentation));
		this.assignPeaks();

	}

	public void setFitPrecision(float fitPrecision) {
		this.fitPrecision = fitPrecision;
	}

	public float getFitPrecision() {
		return fitPrecision;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	/**
	 * @return the hash_assigned_peaks
	 */
	public Hashtable<Integer, SvgIon> getHash_assigned_peaks() {
		return hash_assigned_peaks;
	}

	/*
	 * public Element getSvgRoot() { return this.svg_graphic.getSvgRoot(); }
	 */
	public SvgPeptideSequence getSvgPeptideSequence() {
		return this.sequence;
	}

	public Integer getAssignedPeak(SvgIon ion) {

		for (Integer peakNum : hash_assigned_peaks.keySet()) {
			if (hash_assigned_peaks.get(peakNum).equals(ion)) {
				return peakNum;
			}
		}
		return null;
	}

	public HashSet<SvgAaMod> getSvgAaModSet() {
		return this.sequence.getSvgAaModSet();
	}

	public void setCidFragmentation() {
		fragmentation = FragmentationTypeCID.getInstance();
	}

	public void setEtdFragmentation() {
		fragmentation = FragmentationTypeETD.getInstance();
	}

	public FragmentationTypeBase getFragmentationType() {
		return fragmentation;
	}

	public float getPeakMz(int peakIndex) {
		return this.arr_mz[peakIndex];
	}

	public float getPeakIntensity(int peakIndex) {
		return this.arr_int[peakIndex];
	}

	public boolean isPeakAssigned(int peakIndex) {
		return this.hash_assigned_peaks.containsKey(peakIndex);
	}

	public SvgIon getPeakAssignedSvgIon(int peakIndex) {
		return this.hash_assigned_peaks.get(peakIndex);
	}

	public SvgGroup getPeakSvgGroupByIonSpecies(SvgIonSpecies svgIonSpecies) {
		SvgGroup currentPeakGroup = this.hash_ion2group.get(svgIonSpecies);
		if (currentPeakGroup == null) {
			System.out.println("no svg group for ion species "
					+ svgIonSpecies.getName());
		}
		return currentPeakGroup;
	}

	public SvgGroup getPeaksSvgGroup() {
		return this.peaksGroup;
	}

	public SvgGroup getSpectrumSvgGroup() {
		return this.spectrum_graph;
	}

	public HashSet<SvgIonSpecies> getAssignedSvgIonSpecies() {
		HashSet<SvgIonSpecies> svgIonSpeciesList = new HashSet<SvgIonSpecies>();
		Hashtable<Integer, SvgIon> svgIonList = this.getAssignedPeaks();
		for (Integer indice : svgIonList.keySet()) {
			svgIonSpeciesList.add(svgIonList.get(indice).getSvgIonSpecies());
		}
		return svgIonSpeciesList;

	}

	public float getMinMz() {
		return this.spectrum_mz_min;
	}

	public float getMaxMz() {
		return this.spectrum_mz_max;
	}

	public SvgCoordXY getSpectrumCoordXY() {
		SvgCoordXY svgCoordXY = new SvgCoordXY(this.marginLeft, this.marginTop);
		return svgCoordXY;
	}

	public SvgCoordXY getSpectrumCoordXYend() {
		SvgCoordXY svgCoordXY = new SvgCoordXY(this.marginLeft
				+ this.spectrum_width, this.marginTop + this.spectrum_height);
		return svgCoordXY;
	}

	public void setPaddingLeft(float f) {
		this.paddingLeft = f;
		this.setXratio();
	}

	public void setPaddingRight(float f) {
		this.paddingRight = f;
		this.setXratio();
	}

}
