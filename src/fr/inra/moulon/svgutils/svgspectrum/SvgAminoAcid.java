package fr.inra.moulon.svgutils.svgspectrum;

import java.util.ArrayList;
import java.util.HashMap;

import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgRect;
import fr.inra.moulon.svgutils.elements.SvgTextInTspan;

public class SvgAminoAcid {

	private Character aa;
	private float mass;
	private ArrayList<SvgAaMod> aamods;
	private HashMap<String, SvgIon> assignedIon;

	public SvgAminoAcid(Character aa) throws Exception {
		super();
		assignedIon = new HashMap<String, SvgIon>();
		// System.out.print("SvgAminoAcid(Character aa) " + aa);
		aamods = new ArrayList<SvgAaMod>(0);
		this.aa = aa;

		switch (aa) {
		case 'W':
			mass = (float) 186.0793;
			break;
		case 'G':
			mass = (float) 57.0214;
			break;
		case 'A':
			mass = (float) 71.0371;
			break;
		case 'S':
			mass = (float) 87.0320;
			break;
		case 'P':
			mass = (float) 97.0527;
			break;
		case 'V':
			mass = (float) 99.0684;
			break;
		case 'T':
			mass = (float) 101.0476;
			break;
		case 'L':
			mass = (float) 113.0840;
			break;
		case 'I':
			mass = (float) 113.0840;
			break;
		case 'N':
			mass = (float) 114.0429;
			break;
		case 'D':
			mass = (float) 115.0269;
			break;
		case 'K':
			mass = (float) 128.0949;
			break;
		case 'Q':
			mass = (float) 128.0585;
			break;
		case 'E':
			mass = (float) 129.0425;
			break;
		case 'M':
			mass = (float) 131.0404;
			break;
		// $arr_aa_mass['m'] = 147.04; #METHIONINE OXIDEE (+16)
		case 'm':
			mass = (float) 131.0404;
			addModification((float) 15.994915);
			break;
		case 'H':
			mass = (float) 137.0589;
			break;
		case 'F':
			mass = (float) 147.0684;
			break;
		case 'R':
			mass = (float) 156.1011;
			break;
		/*
		 * case 'C': mass = (float) 161.0142; break;
		 */
		// $arr_aa_mass['C'] = 161.01; #CYSTEINE CARBAMIDOMETHYLE
		case 'c':
			mass = (float) 161.01;// CYSTEINE CARBAMIDOMETHYLE
			break;
		case 'C':
			mass = (float) 103.00919;
			// addModification((float) 57.021464);
			break;
		case 'Y':
			mass = (float) 163.0633;
			break;
		case 'X':
			mass = (float) 0;
			break;
		default:
			throw new Exception("SvgAminoAcid can not be built with '" + aa
					+ "'");
		}
		// System.out.print("SvgAminoAcid(Character aa) mass " + mass);

	}

	public float getMass() {
		float complete_mass = mass;
		for (SvgAaMod mod : this.aamods) {
			complete_mass += mod.getMass();
		}

		return complete_mass;
	}

	public String toString() {
		return aa.toString();
	}

	public void addModification(float mass) {
		SvgAaMod modification = new SvgAaMod(mass);
		this.aamods.add(modification);
	}

	public void addAssignedIon(SvgIon ion) {
		assignedIon.put(ion.getSvgIonSpecies().getCssClassname(), ion);
	}

	public void svgAssignedIon(SvgGroup svgGraphic, float xcoord, float ycoord,
			int fontSize) throws Exception {
		float bar_height = 0;
		// svgGraphic.text(xcoord + 10, ycoord, aa.toString(), "");
		SvgTextInTspan aaText = svgGraphic.newTextInTspan(aa.toString());
		aaText.setX(xcoord + 10);
		aaText.setY(ycoord);

		float cumul = 0;
		for (String ionKey : this.assignedIon.keySet()) {
			if (ionKey.startsWith("b1")) {
				cumul = this.assignedIon.get(ionKey)
						.getAssignedPeakIntensityRatio();
			}
		}
		bar_height += 10 * cumul;

		cumul = 0;
		for (String ionKey : this.assignedIon.keySet()) {
			if (ionKey.startsWith("b2")) {
				cumul = this.assignedIon.get(ionKey)
						.getAssignedPeakIntensityRatio();
			}
		}
		bar_height += 10 * cumul;

		if (this.assignedIon.containsKey("c1")) {
			bar_height += 10 * this.assignedIon.get("c1")
					.getAssignedPeakIntensityRatio();
		}
		if (this.assignedIon.containsKey("c2")) {
			bar_height += 10 * this.assignedIon.get("c2")
					.getAssignedPeakIntensityRatio();
		}
		if (bar_height != 0) {
			// SvgGraphics.svgRect(svgGraphic.getDocument(),
			// svgGraphic.getCurrentNode(), xcoord + 17, ycoord - 8, 3,
			// bar_height, "b1");
			if (bar_height > 15) {
				bar_height = 15;
			}
			SvgRect svgRect = svgGraphic.newRect(3, bar_height);
			svgRect.setX(xcoord + 17);
			svgRect.setY(ycoord - 8);
			svgRect.setCssClass("b1");

		}
		bar_height = 0;
		// if (this.assignedIon.containsKey("y1")) {
		cumul = 0;
		for (String ionKey : this.assignedIon.keySet()) {
			if (ionKey.startsWith("y1")) {
				cumul = this.assignedIon.get(ionKey)
						.getAssignedPeakIntensityRatio();
			}
		}
		bar_height += 10 * cumul;

		// }
		cumul = 0;
		for (String ionKey : this.assignedIon.keySet()) {
			if (ionKey.startsWith("y2")) {
				cumul = this.assignedIon.get(ionKey)
						.getAssignedPeakIntensityRatio();
			}
		}
		bar_height += 10 * cumul;

		if (this.assignedIon.containsKey("z1")) {
			bar_height += 10 * this.assignedIon.get("z1")
					.getAssignedPeakIntensityRatio();

		}
		if (this.assignedIon.containsKey("z2")) {
			bar_height += 10 * this.assignedIon.get("z2")
					.getAssignedPeakIntensityRatio();

		}
		if (bar_height != 0) {
			// SvgGraphics.svgRect(svgGraphic.getDocument(),
			// svgGraphic.getCurrentNode(), xcoord, ycoord - 8
			// - bar_height, 3, bar_height, "y1");
			if (bar_height > 15) {
				bar_height = 15;
			}
			SvgRect svgRect = svgGraphic.newRect(3, bar_height);
			svgRect.setX(xcoord);
			svgRect.setY(ycoord - 8 - bar_height);
			svgRect.setCssClass("y1");

		}
	}

	public ArrayList<SvgIon> getAssignedIons() {
		ArrayList<SvgIon> iontable = new ArrayList<SvgIon>();
		for (SvgIon ion : assignedIon.values()) {
			iontable.add(ion);
		}
		return (iontable);
	}

	public ArrayList<SvgAaMod> getSvgAaModList() {
		return this.aamods;
	}

	public void addModification(SvgAaMod themod) {
		this.aamods.add(themod);
	}

	public boolean isAA(Character string) {
		return this.aa.equals(string);
	}
}
