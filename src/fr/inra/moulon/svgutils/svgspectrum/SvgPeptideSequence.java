package fr.inra.moulon.svgutils.svgspectrum;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import fr.inra.moulon.svgutils.elements.SvgGroup;

public class SvgPeptideSequence implements Iterable<SvgAminoAcid>,
		Comparable<SvgPeptideSequence> {
	private static Logger logger = Logger.getLogger(SvgPeptideSequence.class);

	private ArrayList<SvgAminoAcid> sequence;
	private int exp_z;
	private int fontSize;

	// private SvgSpectrumDocument svgSpectrumDoc;
	// private FragmentationTypeBase fragmentation =
	// FragmentationTypeCID.getInstance();

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public SvgPeptideSequence(String str_sequence) throws Exception {
		super();
		// this.svgSpectrumDoc = svgSpectrumDoc;
		this.sequence = new ArrayList<SvgAminoAcid>();
		exp_z = 2;
		fontSize = 20;
		logger.debug("SvgPeptideSequence(String str_sequence) " + str_sequence);
		// System.out.print("SvgPeptideSequence(String str_sequence) coucou " +
		// str_sequence);
		for (Character aacar : str_sequence.toCharArray()) {
			// System.out.print("SvgPeptideSequence(String str_sequence)" +
			// aacar+ " " + str_sequence);
			SvgAminoAcid aa = new SvgAminoAcid(aacar);
			this.sequence.add(aa);
		}
	}

	public String toString() {
		String tmpstring = new String();
		for (SvgAminoAcid aa : this.sequence) {
			// System.out.print("SvgPeptideSequence(String str_sequence)" +
			// aacar+ " " + str_sequence);
			tmpstring += aa.toString();
		}
		return tmpstring;
	}

	public int getExp_z() {
		return exp_z;
	}

	public void setExp_z(int expZ) {
		exp_z = expZ;
	}

	public int size() {
		return sequence.size();
	}

	public SvgAminoAcid get(int i) throws Exception {
		if (i < 0) {
			throw new Exception("out of bound ERROR : i (" + i + ") < 0 ");
		}
		if (i >= this.sequence.size()) {
			throw new Exception("out of bound ERROR : i (" + i
					+ ") > sequence length (" + this.sequence.size() + ")");
		}
		return (this.sequence.get(i));
	}

	public SvgAminoAcid getAA(int i) throws Exception {
		if (i < 1) {
			throw new Exception("ERROR, position of aa < 1 (" + i
					+ "): aa position in peptide sequence starts at 1");
		}
		if (i > this.sequence.size()) {
			throw new Exception(
					"ERROR, position of aa > sequence length ("
							+ i
							+ ", "
							+ this.sequence.size()
							+ "), remember that aa position in peptide sequence starts at 1");
		}
		return (this.sequence.get(i - 1));
	}

	public float getSubCterMass(int length) throws Exception {
		if (length > sequence.size()) {
			throw new Exception("length too long in getSubCterMass :" + length
					+ " for maximum length : " + sequence.size());
		}
		float mass = 0;
		for (int i = (sequence.size() - 1); i >= (length - 1); i--) {
			mass += sequence.get(i).getMass();
		}
		return mass;
	}

	public float getSubNterMass(int length) throws Exception {
		if (length > sequence.size()) {
			throw new Exception("length too long in getSubCterMass :" + length
					+ " for maximum length : " + sequence.size());
		}
		float mass = 0;
		for (int i = 0; i < length; i++) {
			mass += sequence.get(i).getMass();
		}
		return mass;
	}

	public float getMass() {
		float mass = 0;
		for (SvgAminoAcid aa : sequence) {
			mass += aa.getMass();
		}
		return mass;
	}

	public ArrayList<SvgIon> getIonTable(FragmentationTypeBase fragmentation)
			throws Exception {
		if (fragmentation == null) {
			throw new Exception("fragmentation model is null");
		}
		ArrayList<SvgIon> iontable = new ArrayList<SvgIon>();
		/*
		 * 
		 * public void setIonC1() { setType("c"); setZ(1); this.priority = 13; }
		 * 
		 * public void setIonC2() { setType("c"); setZ(2); this.priority = 13; }
		 * 
		 * public void setIonZ1() { setType("z"); setZ(1); this.priority = 13; }
		 * 
		 * public void setIonZ2() { setType("z"); setZ(2); this.priority = 13; }
		 */
		// Nter ions
		int num = 1;

		for (int i = 0; i < sequence.size() - 1; i++, num++) {

			fragmentation.addNterIons(iontable, num, sequence, this.exp_z);

		}
		// Cter ions
		num = 1;
		for (int i = (sequence.size() - 1); i > 0; i--, num++) {
			fragmentation.addCterIons(iontable, num, sequence, this.exp_z);
		}

		return iontable;
	}

	public float svgWidth() {
		return (this.fontSize * (this.sequence.size()));
	}

	public void drawSvgPeptideSequence(SvgGroup svgSequenceGroup, float xcoord,
			float ycoord) throws Exception {
		// svg_graphic.group("svgsequence", "");
		// SvgGroup group_sequence = svg_graphic.goToGroup("svgsequence");

		// this.svgSpectrumDoc.setCssClass("#svgsequence tspan",
		// "font-size:" + this.getFontSize()
		// + "px;stroke:#000000;fill:#000000;text-anchor:middle;");

		// Element text = SvgGraphics.svgText(svg_graphic.getDocument(),
		// group_sequence.getElement(), xcoord, ycoord, "");
		// xcoord += this.fontSize;
		for (SvgAminoAcid aa : this.sequence) {
			// SvgGraphics.svgTspan(svg_graphic.getDocument(), text, xcoord,
			// ycoord, aa.toString(), "");
			aa.svgAssignedIon(svgSequenceGroup, xcoord, ycoord, this.fontSize);
			xcoord += this.fontSize;

		}

		// return group_sequence.getElement();

	}

	public void addAssignedIon(SvgIon ion) {
		if (ion.getSvgIonSpecies().isCside() == false) {
			this.sequence.get(ion.getNumber() - 1).addAssignedIon(ion);
		} else {
			this.sequence.get(sequence.size() - ion.getNumber())
					.addAssignedIon(ion);
		}
	}

	public ArrayList<SvgIon> getAssignedIons() {
		ArrayList<SvgIon> iontable = new ArrayList<SvgIon>();
		for (int i = 0; i < sequence.size(); i++) {
			for (SvgIon ion : sequence.get(i).getAssignedIons()) {
				iontable.add(ion);

			}
		}
		return (iontable);
	}

	public TreeSet<SvgIonSpecies> getNterSvgIonSpeciesList(
			FragmentationTypeBase fragmentation) throws Exception {
		TreeSet<SvgIonSpecies> speciesList = new TreeSet<SvgIonSpecies>();

		ArrayList<SvgIon> ionTable = getIonTable(fragmentation);
		for (SvgIon ion : ionTable) {
			SvgIonSpecies ionSpecies = ion.getSvgIonSpecies();
			if (ionSpecies.isCside() == false) {
				speciesList.add(ionSpecies);
			}
		}
		return speciesList;

	}

	public TreeSet<SvgIonSpecies> getCterSvgIonSpeciesList(
			FragmentationTypeBase fragmentation) throws Exception {
		TreeSet<SvgIonSpecies> speciesList = new TreeSet<SvgIonSpecies>();

		ArrayList<SvgIon> ionTable = getIonTable(fragmentation);
		for (SvgIon ion : ionTable) {
			SvgIonSpecies ionSpecies = ion.getSvgIonSpecies();
			if (ionSpecies.isCside() == true) {
				// System.out.println("coucou " + ionSpecies.getCssClassname());

				speciesList.add(ionSpecies);
			}
		}
		return speciesList;

	}

	public Iterator<SvgAminoAcid> iterator() {
		return this.sequence.iterator();
	}

	public HashSet<SvgAaMod> getSvgAaModSet() {

		HashSet<SvgAaMod> svgAaModList = new HashSet<SvgAaMod>(0);
		for (SvgAminoAcid aa : this.sequence) {
			for (SvgAaMod mod : aa.getSvgAaModList()) {
				svgAaModList.add(mod);
			}
		}
		return (svgAaModList);
	}

	public String getStringWithModifs() {
		String print = "";
		for (SvgAminoAcid aa : this.sequence) {
			print += aa.toString();
			for (SvgAaMod mod : aa.getSvgAaModList()) {
				print += mod.getMass();
			}
		}
		return print;
	}

	public String getStringHighlightModifs(SvgAaMod highlightMod) {
		String print = "";
		for (SvgAminoAcid aa : this.sequence) {
			print += aa.toString();
			if (aa.getSvgAaModList().contains(highlightMod)) {
				print += "*";
			}
		}
		return print;
	}

	public ArrayList<Integer> getModifPositions(SvgAaMod highlightMod) {
		ArrayList<Integer> posList = new ArrayList<Integer>();
		int pos = 1;
		for (SvgAminoAcid aa : this.sequence) {
			for (SvgAaMod mod : aa.getSvgAaModList()) {
				if (mod.equals(highlightMod)) {
					posList.add(pos);
				}
			}
			pos++;
		}
		return posList;
	}

	public String getStringOnlySequence() {
		String print = "";
		for (SvgAminoAcid aa : this.sequence) {
			print += aa.toString();
		}
		return print;
	}

	@Override
	public int compareTo(SvgPeptideSequence arg0) {
		int compar = this.getStringOnlySequence().compareTo(
				arg0.getStringOnlySequence());
		if (compar == 0) {
			return (this.getStringWithModifs().compareTo(arg0
					.getStringWithModifs()));
		} else {
			return (compar);
		}
	}
}
