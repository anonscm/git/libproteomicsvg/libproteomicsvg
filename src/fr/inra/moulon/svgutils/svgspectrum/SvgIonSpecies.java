package fr.inra.moulon.svgutils.svgspectrum;

import java.util.HashMap;

import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericTextContainer;

public class SvgIonSpecies implements Comparable<SvgIonSpecies> {
	private static HashMap<String, SvgIonSpecies> hashIonSpecies = new HashMap<String, SvgIonSpecies>(
			0);
	protected String type;
	protected Integer z;
	protected boolean isCside; // for a and b : false for y : true
	private int priority;
	protected Integer nbLoss = null;
	protected String mnemonicLoss = null;

	SvgIonSpecies() {
		type = null;
		z = null;
	}

	public String getCssClassname() {
		String classname = type + z;
		classname = classname.replace("*", "star");
		if (mnemonicLoss != null) {
			classname += "m" + this.mnemonicLoss;
			classname += "m" + this.mnemonicLoss;
		}
		return classname;
	}

	public String getTypeZ() {
		return type + z;
	}

	public String getType() {
		return type;
	}

	protected void setType(String type) {
		this.type = type;
	}

	public int getZ() {
		return z;
	}

	protected void setZ(int iz) {
		this.z = iz;
	}

	public String getName() {
		String name = type;
		if (z > 1) {
			for (int i = 0; i < z; i++) {
				name += '+';
			}
		}
		if (mnemonicLoss != null) {
			name += "(-" + this.nbLoss + this.mnemonicLoss + ")";
		}
		return name;
	}

	public static SvgIonSpecies getY1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "y1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("y");
			ionSpecies.isCside = true;
			ionSpecies.setZ(1);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getYP1Instance(int nbLoss) {
		SvgIonSpecies ionSpecies;
		String key = "y1-" + nbLoss + "P";

		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setMnemonicLoss("P");
			ionSpecies.setNbLoss(nbLoss);
			ionSpecies.setType("y");
			ionSpecies.isCside = true;
			ionSpecies.setZ(1);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getYP2Instance(int nbLoss) {
		SvgIonSpecies ionSpecies;
		String key = "y2-" + nbLoss + "P";

		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setMnemonicLoss("P");
			ionSpecies.setNbLoss(nbLoss);
			ionSpecies.setType("y");
			ionSpecies.isCside = true;
			ionSpecies.setZ(2);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	private void setMnemonicLoss(String loss) {
		this.mnemonicLoss = loss;
	}

	private void setNbLoss(int nbLoss2) {
		this.nbLoss = nbLoss2;
	}

	public static SvgIonSpecies getB1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "b1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("b");
			ionSpecies.isCside = false;
			ionSpecies.setZ(1);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;

	}

	public static SvgIonSpecies getBP1Instance(int nbLoss) {
		SvgIonSpecies ionSpecies;
		String key = "b1-" + nbLoss + "P";

		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setMnemonicLoss("P");
			ionSpecies.setNbLoss(nbLoss);
			ionSpecies.setType("b");
			ionSpecies.isCside = false;
			ionSpecies.setZ(1);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getBP2Instance(int nbLoss) {
		SvgIonSpecies ionSpecies;
		String key = "b2-" + nbLoss + "P";

		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setMnemonicLoss("P");
			ionSpecies.setNbLoss(nbLoss);
			ionSpecies.setType("b");
			ionSpecies.isCside = false;
			ionSpecies.setZ(2);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getY2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "y2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("y");
			ionSpecies.isCside = true;
			ionSpecies.setZ(2);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public boolean isCside() {
		return isCside;
	}

	public static SvgIonSpecies getB2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "b2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("b");
			ionSpecies.isCside = false;
			ionSpecies.setZ(2);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getYstar1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "y*1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("y*");
			ionSpecies.isCside = true;
			ionSpecies.setZ(1);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getYstar2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "y*2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("y*");
			ionSpecies.isCside = true;
			ionSpecies.setZ(2);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getYO1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "yO1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("yO");
			ionSpecies.isCside = true;
			ionSpecies.setZ(1);
			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getYO2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "yO2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("yO");
			ionSpecies.isCside = true;
			ionSpecies.setZ(2);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getBstar1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "b*1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("b*");
			ionSpecies.isCside = false;
			ionSpecies.setZ(1);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getBstar2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "b*2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("b*");
			ionSpecies.isCside = false;
			ionSpecies.setZ(2);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getBO1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "bO1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("bO");
			ionSpecies.isCside = false;
			ionSpecies.setZ(1);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getBO2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "bO2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("bO");
			ionSpecies.isCside = false;
			ionSpecies.setZ(2);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getA1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "a1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("a");
			ionSpecies.isCside = false;
			ionSpecies.setZ(1);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getC1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "c1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("c");
			ionSpecies.isCside = false;
			ionSpecies.setZ(1);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getC2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "c2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("c");
			ionSpecies.isCside = false;
			ionSpecies.setZ(2);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getZ1Instance() {
		SvgIonSpecies ionSpecies;
		String key = "z1";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("z");
			ionSpecies.isCside = true;
			ionSpecies.setZ(1);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	public static SvgIonSpecies getZ2Instance() {
		SvgIonSpecies ionSpecies;
		String key = "z2";
		if (hashIonSpecies.containsKey(key)) {
			ionSpecies = hashIonSpecies.get(key);
		} else {
			ionSpecies = new SvgIonSpecies();
			ionSpecies.setType("z");
			ionSpecies.isCside = true;
			ionSpecies.setZ(2);

			hashIonSpecies.put(key, ionSpecies);
		}

		return ionSpecies;
	}

	@Override
	public int hashCode() {
		String strCode = "aa" + type + "-" + z;

		if (this.nbLoss != null) {
			strCode = "aa" + type + "-" + z + "-" + nbLoss + "-" + mnemonicLoss;
		}
		return strCode.hashCode();
	}

	@Override
	public boolean equals(Object aThat) {
		// System.out.println("coucou");
		if (!(aThat instanceof SvgIonSpecies)) {
			// System.out.println(this.getCssClassname() + " buf "
			// + ((SvgIonSpecies) aThat).getCssClassname());
			return false;
		}
		if (this.hashCode() == aThat.hashCode()) {
			// System.out.println(this.getCssClassname() + " "
			// + ((SvgIonSpecies) aThat).getCssClassname());
			return true;
		}
		return false;
	}

	public String getCssColor() {
		String color = "black";
		if (type.equals("a")) {
			color = "green";
		} else if ((type.equals("bO") || (type.equals("bstar")))) {
			color = "#ff00ff";
		} else if ((type.equals("yO") || (type.equals("ystar")))) {
			color = "orange";
		} else if (type.equals("b")) {
			color = "blue";
		} else if (type.equals("bP")) {
			color = "blue";
		} else if (type.equals("c")) {
			color = "blue";
		} else if (type.equals("y")) {
			color = "red";
		} else if (type.equals("yP")) {
			color = "red";
		} else if (type.equals("z")) {
			color = "red";
		}
		return color;
	}

	public void printTo(XhtmlGenericTextContainer element) {
		element.appendText(type);
		if (this.z > 1) {
			String strExp = "";
			for (int i = 0; i < this.z; i++) {
				strExp += "+";
			}
			// exp.setTextContent(strExp);
			element.newSup(strExp);
		}
		if (this.nbLoss != null) {
			element.appendText("(-" + nbLoss + mnemonicLoss + ")");
		}

		// element.appendChild(node);
	}

	protected void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public int compareTo(SvgIonSpecies toCompare) {
		if (this.priority > toCompare.priority) {
			return (1);
		}
		if (this.priority < toCompare.priority) {
			// -1 less than
			return (-1);
		}
		if ((this.nbLoss != null) && (toCompare.nbLoss != null)) {
			if (this.nbLoss > toCompare.nbLoss) {
				return (1);
			}
			if (this.nbLoss < toCompare.nbLoss) {
				// -1 less than
				return (-1);
			}
			return 0; // equal
		}
		if ((this.nbLoss != null)) {
			return (-1);
		}
		return 0; // equal
	}

}
