/*******************************************************************************
 * Copyright (c) 2012 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libProteomicSvg.
 * 
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

package fr.inra.moulon.svgutils.svgspectrum;

import fr.inra.moulon.svgutils.elements.SvgGroup;
import fr.inra.moulon.svgutils.elements.SvgLine;
import fr.inra.moulon.svgutils.elements.SvgText;

public class SvgPeak {

	private float mz;
	private float intensity;
	private SvgIon assignedSvgIon;
	private SvgSpectrumDocument svgSpectrumDocument;
	private SvgGroup peakInGroup = null;
	private SvgGroup peakGroup = null;
	private SvgLine peakLine = null;

	public SvgPeak(SvgSpectrumDocument svgSpectrumDocument, int peakIndex)
			throws Exception {
		this.svgSpectrumDocument = svgSpectrumDocument;
		this.mz = svgSpectrumDocument.getPeakMz(peakIndex);
		this.intensity = svgSpectrumDocument.getPeakIntensity(peakIndex);

		if (svgSpectrumDocument.isPeakAssigned(peakIndex)) {
			this.setAssignedSvgIon(svgSpectrumDocument
					.getPeakAssignedSvgIon(peakIndex));
			this.setSvgGroup(svgSpectrumDocument
					.getPeakSvgGroupByIonSpecies(this.assignedSvgIon
							.getSvgIonSpecies()));
		}
		if (this.peakInGroup == null) {
			this.setSvgGroup(svgSpectrumDocument.getPeaksSvgGroup());
		}

		this.drawPeak();
	}

	private void setAssignedSvgIon(SvgIon svgIon) {
		this.assignedSvgIon = svgIon;

	}

	private void setSvgGroup(SvgGroup peakInGroup) throws Exception {
		if (peakInGroup == null) {
			throw new Exception("peakInGroup is null");
		}
		if (this.peakInGroup == null) {
			this.peakInGroup = peakInGroup;
			this.peakGroup = peakInGroup.newGroup();
		} else {
			throw new Exception("peak group already assigned");
		}
	}

	public void printIonName() throws Exception {
		if (peakGroup == null) {
			throw new Exception("svg group not assigned");
		}
		if (assignedSvgIon == null) {
			return;
		}
		SvgText peakLabel = peakGroup.newText(this.assignedSvgIon.getName());
		peakLabel.setX(svgSpectrumDocument.mzToX(this.mz));
		peakLabel.setY(svgSpectrumDocument.intToY(this.intensity) - 5);
		peakLabel.setCssClass(this.assignedSvgIon.getSvgIonSpecies()
				.getCssClassname());

	}

	public float getIntensity() {
		return this.intensity;
	}

	public void drawPeak() throws Exception {
		if (peakGroup == null) {
			throw new Exception("svg group not assigned");
		}
		if (peakLine == null) {
			this.peakLine = peakGroup.newLine(
					svgSpectrumDocument.mzToX(this.mz),
					svgSpectrumDocument.intToY(0),
					svgSpectrumDocument.mzToX(this.mz),
					svgSpectrumDocument.intToY(this.intensity));
		}
	}

	public void setOnMouseOver(String action) {
		peakGroup.setOnMouseOver(action);
	}

	public float getMz() {
		return this.mz;
	}

	public void setOnMouseOut(String action) {
		peakGroup.setOnMouseOut(action);
	}
}