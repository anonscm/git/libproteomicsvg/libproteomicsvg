package fr.inra.moulon.svgutils.svgspectrum;

import java.util.ArrayList;

import fr.inra.pappso.phospredictor.PhosphoTypes;

public class FragmentationTypeCID extends FragmentationTypeBase {
	// CID => y, b, perte eau amine, a

	private static FragmentationTypeCID instance = null;

	private FragmentationTypeCID() {
	}

	public static FragmentationTypeCID getInstance() {
		if (null == instance) { // Premier appel
			instance = new FragmentationTypeCID();
		}
		return instance;

	}

	@Override
	public void addNterIons(ArrayList<SvgIon> iontable, int num,
			ArrayList<SvgAminoAcid> sequence, int exp_z) {

		int nbPhopsho = 0;
		float mass = 0;
		for (int i = 0; i < num; i++) {
			mass += sequence.get(i).getMass();
			ArrayList<SvgAaMod> modList = sequence.get(sequence.size() - i - 1)
					.getSvgAaModList();
			for (SvgAaMod mod : modList) {

				if (PhosphoTypes.phosphorylation.equals(mod
						.getPsiModAccession())) {
					// DiffMono 79.966331
					nbPhopsho++;
				}
			}
		}

		// ion b, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonB1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(2);

		iontable.add(tmpIon);

		for (int i = 0; i < nbPhopsho; i++) {
			tmpIon = new SvgIon();
			tmpIon.setIonBP1((i + 1));
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass - ((i + 1) * this.neutralPhosphoLoss));
			tmpIon.setPriority(2);
			iontable.add(tmpIon);

		}

		// ion a, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonA1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(13);

		iontable.add(tmpIon);

		// ion b, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonB2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(4);

			iontable.add(tmpIon);
		}

		// ion b*, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonBstar1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(9);

		iontable.add(tmpIon);

		// ion b*, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonBstar2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(10);

			iontable.add(tmpIon);
		}

		// ion bO, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonBO1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(11);

		iontable.add(tmpIon);

		// ion bO, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonBO2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(12);

			iontable.add(tmpIon);
		}

	}

	@Override
	public void addCterIons(ArrayList<SvgIon> iontable, int num,
			ArrayList<SvgAminoAcid> sequence, int exp_z) {
		float mass = 0;
		int nbPhopsho = 0;
		for (int i = 0; i < num; i++) {
			mass += sequence.get(sequence.size() - i - 1).getMass();
			ArrayList<SvgAaMod> modList = sequence.get(sequence.size() - i - 1)
					.getSvgAaModList();
			for (SvgAaMod mod : modList) {
				if (PhosphoTypes.phosphorylation.equals(mod
						.getPsiModAccession())) {
					// DiffMono 79.966331
					nbPhopsho++;
				}
			}
		}
		// ion y, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonY1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(1);

		iontable.add(tmpIon);

		for (int i = 0; i < nbPhopsho; i++) {
			// System.out.println("coucou");
			tmpIon = new SvgIon();
			tmpIon.setIonYP1((i + 1));
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass - ((i + 1) * this.neutralPhosphoLoss));
			tmpIon.setPriority(1);

			iontable.add(tmpIon);

		}

		// ion y, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonY2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(3);

			iontable.add(tmpIon);

			for (int i = 0; i < nbPhopsho; i++) {
				tmpIon = new SvgIon();
				tmpIon.setIonYP2((i + 1));
				tmpIon.setNumber(num);
				tmpIon.setSequenceMass(mass
						- ((i + 1) * this.neutralPhosphoLoss));
				tmpIon.setPriority(3);

				iontable.add(tmpIon);

			}

		}

		// ion y*, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonYstar1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(5);

		iontable.add(tmpIon);

		// ion y*, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonYstar2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(6);

			iontable.add(tmpIon);
		}

		// ion yO, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonYO1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(7);

		iontable.add(tmpIon);

		// ion yO, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonYO2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(8);

			iontable.add(tmpIon);
		}

	}
}