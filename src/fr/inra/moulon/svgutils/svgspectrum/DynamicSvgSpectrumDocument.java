package fr.inra.moulon.svgutils.svgspectrum;

import java.util.ArrayList;

import fr.inra.moulon.svgutils.elements.SvgScript;
import fr.inra.moulon.svgutils.elements.SvgText;

/**
 * SvgSpectrum free java library to draw annotated MS spectrum Copyright (C)
 * 2009 Olivier Langella
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * 
 * @author Olivier Langella <olivier.langella@moulon.inra.fr
 * 
 */

public class DynamicSvgSpectrumDocument extends SvgSpectrumDocument {

	//private SvgScript javascriptElement;

	public DynamicSvgSpectrumDocument(float width, float height)
			throws Exception {
		super(width, height);

		SvgText labelMass = this.getPeaksSvgGroup().newText(" ");
		labelMass.setX(50);
		labelMass.setY(50);
		labelMass.setId("mzInfo");
		SvgScript javascriptElement = this.getScript();

		// console.log('x'+cursor.x+' y'+cursor.y);
		String code = "var mzSpectrum;var cursorOut={x:0, y:0};"
				+ "function initSvg() {mzSpectrum = {};"
				+ "		initSpectrum(mzSpectrum);"
				+ "		hideMass();"
				// + "		mzSpectrum.zoomMz(2);"
				+ "};"
				+ "function getEvtPosition(evt) {"
				+ "		var cursor = {x:0, y:0};"
				+ "		cursor.x = evt.clientX;"
				+ "		cursor.y = evt.clientY;"
			//	+ "		console.log('pos x:'+cursor.x+' pos y:'+cursor.y);"
				+ "		return cursor;"
				+ "};"
				// + this.writeGetPositionFunction()
				// + this.writeGetScrollPositionFunction()
				+ "function getEvtTargetPosition(evt) {"
				+ "		var cursor = {x:0, y:0};"
				+ "		cursor.x = evt.target.getBBox().x;"
				+ "		cursor.y = evt.target.getBBox().y;"
				+ "return cursor;"
				+ "};"
				+ "function showMass(evt,mz) {mzSpectrum.mzInfoNode.firstChild.nodeValue = 'mz = '+mz;mzSpectrum.mzInfoNode.setPosition(getEvtTargetPosition(evt)); }"
				+ "function hideMass() {mzSpectrum.mzInfoNode.setPosition(cursorOut);}";
		javascriptElement.appendText(code);
		this.getSvgRoot().setOnLoad("initSvg()");
	}

	private String writeInitializeSpectrumFunction() {
		String function = "function initSpectrum(spectrum) {\n"
				+ "spectrum.xOrigin = "
				+ this.getSpectrumCoordXY().getX()
				+ ";\n"
				+ "spectrum.width = "
				+ (this.getSpectrumCoordXYend().getX() - this
						.getSpectrumCoordXY().getX())
				+ ";\n"
				+ "spectrum.yOrigin = "
				+ this.getSpectrumCoordXY().getY()
				+ ";\n"
				+ "spectrum.mzStart = "
				+ this.getMinMz()
				+ ";\n"
				+ "spectrum.mzStop = "
				+ this.getMaxMz()
				+ ";\n"
				+ "spectrum.mzRatio = "
				+ this.mzToX(1)
				+ ";\n"
				+ "spectrum.isMouseDown = false;"
				+ "spectrum.isMouseMoveDown = false;"
				+ "spectrum.transformScale = '';\n"
				+ "spectrum.textTransformScale = '';\n"
				+ "spectrum.xScale = 1;\n"
				+ "spectrum.xScroll = 0;\n"
				+ "spectrum.spectrumScrollPeaksNode = document.getElementById(\"scrollPeaks\");\n"
				+ "spectrum.spectrumZoomNode = document.getElementById(\"spectrumZoom\");\n"
				+ "spectrum.spectrumZoomNodeRect = document.getElementById(\"spectrumZoomRect\");\n"
				+ "spectrum.spectrumNode = document.getElementById(\"spectrum\");\n"
				+ "spectrum.mzInfoNode = document.getElementById(\"mzInfo\");\n"
				+ "spectrum.mzInfoNode.spectrum = spectrum\n"
				+ "spectrum.getEvtPosition = function (evt) {"
				+ "		var cursor = {x:0, y:0};"
				+ "		cursor.x = evt.clientX-spectrum.xOrigin;"
				+ "		cursor.y = evt.clientY-spectrum.yOrigin;"
				//+ "		console.log('pos x:'+cursor.x+' pos y:'+cursor.y);"
				+ "		return cursor;"
				+ "};"
				+ "spectrum.mzInfoNode.setPosition = function (cursor) {\n"
				//+ "				console.log('c cursor.x '+cursor.x+ ' this.spectrum.textTransformScale '+this.spectrum.textTransformScale);"
				+ "		var posX = this.spectrum.xScale * cursor.x;"
				+ "		spectrum.mzInfoNode.setAttributeNS(null,'x', posX);"
				+ "		spectrum.mzInfoNode.setAttributeNS(null,'y', cursor.y);"
				+ "		spectrum.mzInfoNode.setAttributeNS(null,'transform', this.spectrum.textTransformScale);"
				+ "};\n"
				+ "spectrum.moveMzRangePixel = function (pixelNb) {\n"
				+ "		this.xScroll = this.xScroll+(pixelNb*this.xScale);"
				//+ "		console.log('moveto '+pixelNb+' '+this.xScroll);"
				// + "		this.spectrumZoomNodeRect.setAttributeNS(null,'x', x);"
				+ "		this.spectrumScrollPeaksNode.setAttributeNS(null,'transform', 'translate('+this.xScroll+', 0) '+this.transformScale);"
				+ "};\n"
				+ "spectrum.moveToPixel = function (pixelNb) {\n"
				// + "		this.xScroll = -(pixelNb*this.xScale);"
				+ "		if (pixelNb < 0) {this.xScroll =0;}"
				+ "		else {this.xScroll = -pixelNb;}"
				//+ "		console.log('moveto '+pixelNb+' this.xScroll='+this.xScroll);"
				// + "		this.spectrumZoomNodeRect.setAttributeNS(null,'x', x);"
				+ "		this.spectrumScrollPeaksNode.setAttributeNS(null,'transform', 'translate('+this.xScroll+', 0) '+this.transformScale);"
				+ "};\n"
				+ "spectrum.zoomMz = function (zoomRatio) {\n"
				+ "		var absScroll = this.xScroll/this.xScale;"
				+ "		this.xScale = this.xScale+zoomRatio;"
				+ "		if (this.xScale < 1) this.xScale =1;"
				+ "		this.xScroll = absScroll*this.xScale;"
				+ "		this.transformScale = 'scale('+this.xScale+',1)';"

				+ "		this.spectrumScrollPeaksNode.setAttributeNS(null,'transform', 'translate('+this.xScroll+', 0) '+this.transformScale);"
				+ "		this.downScaleText();"
				+ "};\n"

				+ "spectrum.downScaleText = function () {\n"
				+ "		var newX = (1/this.xScale);"
				+ "		this.textTransformScale = 'scale('+newX+',1)';"

				+ "		this.DomDownscaleText(this.spectrumScrollPeaksNode);\n"
				+ "};\n"
				+ "spectrum.getOriginalPosXof = function (element) {\n"
				+ "		var posX = element.originalPosX;\n"
				+ "		if (posX == undefined) {\n"
				+ "			element.originalPosX = element.getAttributeNS(null,'x');\n"
				+ "			return element.originalPosX;\n"
				+ "		}\n"
				+ "		else {return posX;}\n"
				+ "};\n"

				+ "spectrum.DomDownscaleText = function (node) {\n"
				+ "		if(node.children == undefined) return;\n"
				+ "		if(node.children.length > 0) {var j=0;\n"
				+ "     	for (j=0; j < node.children.length; j=j+1) {\n"
				+ "     		var childNode = node.children[j];\n"
				+ "     		if (childNode.tagName == 'text') {\n"
				+ "					var posX = this.xScale * this.getOriginalPosXof(childNode);"
				// +
				// "				console.log('coucou'+childNode.tagName+' '+this.xScale+' '+posX+' '+childNode.getAttributeNS(null,'x'));"
				+ "					childNode.setAttributeNS(null,'x', posX);"
				+ "					childNode.setAttributeNS(null,'transform', this.textTransformScale);"
				+ "     		}\n"
				+ "     		this.DomDownscaleText(childNode);\n"
				+ "     	}\n"
				+ "     }"
				+ "};\n"
				+ "spectrum.centerToEvt = function (evt) {\n"
				+ "		this.moveToPixel(this.getEvtPosition(evt).x+(this.width/2)-this.xScroll);"
				+ "};\n"
				+ "spectrum.onClick = function (evt) {\n"
				+ "		if (this.isMouseMoveDown) {"
				+ "			this.centerRangeAndZoomMz(this.cursorBeginMouseDown,this.getEvtPosition(evt));"
				+ "		}else {"
				// + "			this.zoomMz(-1);"
				+ "			this.centerToEvtAndZoomMz(evt,-1);"
				+ "		}"
				+ "		this.isMouseDown = false;"
				+ "		this.isMouseMoveDown = false;"
				+ "};\n"
				+ "spectrum.centerRangeAndZoomMz = function (cursorBegin, cursorEnd) {\n"
				+ "		var posXbegin = cursorBegin.x;"
				+ "		var posXend = cursorEnd.x;"
				+ "		if (cursorBegin.x > cursorEnd.x) {"
				+ "			posXbegin = cursorEnd.x;"
				+ "			posXend = cursorBegin.x;"
				+ "		}"
				+ "		var zoomRatioPlus = this.width/(posXend/this.xScale-posXbegin/this.xScale)-this.xScale;"
				// +
				// "		var zoomRatioPlus = (this.width/this.xScale)/(posXend/this.xScale-posXbegin/this.xScale)-this.xScale;"
				+ "		var mouseXratio = (posXbegin/this.xScale);"
				+ "		this.zoomMz(zoomRatioPlus);"
				+ "		var moveX = this.xScroll - mouseXratio*this.xScale;"
				+ "		this.moveToPixel(-moveX);"
				+ "};\n"
				+ "spectrum.onMouseDown = function (evt) {\n"
				+ "		this.cursorBeginMouseDown = this.getEvtPosition(evt);"
				+ "		this.isMouseDown = true;"
				+ "};\n"
				+ "spectrum.onMouseMove = function (evt) {\n"
				+ "		if (this.isMouseDown) {"
				+ "			this.isMouseMoveDown = true;"
				+ "		}"
				+ "};\n"

				+ "spectrum.centerToEvtAndZoomMz = function (evt, zoomRatio) {\n"
				+ "		var mouseXratio = (this.getEvtPosition(evt).x/this.xScale);"
				//+ "				console.log('this.xScroll '+this.xScroll);"
				+ "		this.zoomMz(zoomRatio);"
				//+ "				console.log('this.xScroll after '+this.xScroll);"
				+ "		var moveX = this.xScroll - mouseXratio*this.xScale+(this.width/2);"
				+ "		if (this.xScale == 1) moveX=0;"
				+ "		this.moveToPixel(-moveX);"

				+ "};\n" + "};\n";

		return function;
	}

	@Override
	protected void render() {
		super.render();

		spectrumBackground.setCssClass("white_rect");
		this.setCssClass(".white_rect",
				"stroke:white;fill:white;stroke-opacity:0;fill-opacity:0;");

		String code = this.writeInitializeSpectrumFunction();
		this.getScript().appendText(code);

		this.getSpectrumSvgGroup().setOnMouseDown(
				"mzSpectrum.onMouseDown(evt);");
		this.getSpectrumSvgGroup().setOnMouseMove(
				"mzSpectrum.onMouseMove(evt);");
		this.getSpectrumSvgGroup().setOnClick("mzSpectrum.onClick(evt);");
		// this.getSpectrumSvgGroup().setOnMouseMove(
		// "mzSpectrum.getEvtPosition(evt)");
		// this.getSpectrumSvgGroup().setOnClick("mzSpectrum.moveToPixel(mzSpectrum.getEvtPosition(evt).x+(mzSpectrum.width/2)-mzSpectrum.xScroll);");

		ArrayList<SvgPeak> peakList = null;
		try {
			peakList = this.getSvgPeakList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		for (SvgPeak peak : peakList) {
			// peak.printIonName();
			peak.setOnMouseOver("if (typeof showMass == 'function') {showMass(evt, '"
					+ peak.getMz() + "')};");
			// peak.setOnMouseOut("if (typeof hideMass == 'function') {hideMass()};");
		}

	}

	@SuppressWarnings("unused")
	private String writeGetPositionFunction() {
		String function = "function getPosition(element) {\n"
				+ "var left = 0;var top = 0;var el=element;\n"

				// + "console.log('e2 el.offsetLeft '+el.offsetLeft);"
				// + "console.log('e2 tag '+el.tagName);"
				+ "while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {"
				// + "console.log('e2 tag '+el.tagName);"
				+ "left += el.offsetLeft - el.scrollLeft;"
				+ "top += el.offsetTop - el.scrollTop;" + "el = el.parentNode;"
				+ "}" + "\nreturn {x:left, y:top};"

				+ "}";
		return function;
	}

	@SuppressWarnings("unused")
	private String writeGetScrollPositionFunction() {
		String function = "function getScrollPosition() {\n"
				+ "var cursorb = {x:0, y:0};"
				// + "if (typeof jQuery != 'undefined' ) {"
				// + "cursorb.x += $(window).scrollLeft();"
				// + "cursorb.y += $(window).scrollTop();} else {\n"
				+ "cursorb.x += document.documentElement.scrollLeft;"
				+ "cursorb.y += document.documentElement.scrollTop;"
				// +
				// "cursorb.x += (document.documentElement.scrollLeft ?document.documentElement.scrollLeft :document.body.scrollLeft);"
				// +
				// "cursorb.y += (document.documentElement.scrollTop ?document.documentElement.scrollTop : document.body.scrollTop);"
				// + "\n}"

				+ "return cursorb;" + "}";
		return function;
	}

}
