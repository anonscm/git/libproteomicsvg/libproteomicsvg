package fr.inra.moulon.svgutils.svgspectrum;

public class SvgIon implements Comparable<SvgIon> {

	SvgIonSpecies ionSpecies;
	protected Integer number;
	protected float mz;
	protected Float assignedPeakIntensityRatio = null;
	protected int priority = 100;

	public float getAssignedPeakIntensityRatio() {
		if (assignedPeakIntensityRatio == null) {
			return 0;
		}
		return assignedPeakIntensityRatio;
	}

	public void setAssignedPeakIntensityRatio(Float assignedPeakIntensityRatio) {
		this.assignedPeakIntensityRatio = assignedPeakIntensityRatio;
	}

	/*
	 * m(1H) = 1.00727646677 u = mass of proton; charge +1 m(1H+e-) = 1.00782504
	 * u = mass of proton + mass of electron
	 */
	static float mhplus = (float) 1.00727646677;

	/*
	 *
	 * Monoisotopic mass from : http://www.unimod.org/masses.html 1.007825035
	 */
	static float mhelectron = (float) 1.007825035;

	// static float mhplus = (float) 1.007825;
	static float moxygen = (float) 15.994915;
	static float mC = (float) 12.0;
	// H2O loss:
	static float mh2o = (mhplus * 2) + moxygen;
	// amine loss:
	static float mN = (float) (14.003074);
	static float mnh3 = (float) ((mhplus * 3) + mN);
	static float mco = (float) mC + moxygen;

	/*
	 * N = 14.003074 O = 15.994915 Regarde, si tu n'a pas une erreur 11:12:27
	 * Car, je retombe sur la même masse que le tableau 11:12:38 H = 1.007825
	 * 11:12:55 Le 1.00794 correspond à la masse moyenne tenant compte des
	 * isotopes
	 */

	public SvgIon() {
		number = null;
		mz = 0;
		assignedPeakIntensityRatio = null;
		ionSpecies = null;
	}

	public void setIonY1() {
		ionSpecies = SvgIonSpecies.getY1Instance();
		this.mz = SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonYP1(int nbloss) {
		ionSpecies = SvgIonSpecies.getYP1Instance(nbloss);
		// System.out.println(ionSpecies.getCssClassname());

		this.mz = SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonB1() {
		ionSpecies = SvgIonSpecies.getB1Instance();
		this.mz = SvgIon.mhplus;
	}

	public void setIonBP1(int nbloss) {
		ionSpecies = SvgIonSpecies.getBP1Instance(nbloss);
		this.mz = SvgIon.mhplus;
	}

	public void setIonY2() {
		ionSpecies = SvgIonSpecies.getY2Instance();
		this.mz = mhplus;
		this.mz += SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonYP2(int nbloss) {
		ionSpecies = SvgIonSpecies.getYP2Instance(nbloss);
		this.mz = mhplus;
		this.mz += SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonB2() {
		ionSpecies = SvgIonSpecies.getB2Instance();

		this.mz = mhplus;
		this.mz += SvgIon.mhplus;
	}

	public void setIonBP2(int nbloss) {
		ionSpecies = SvgIonSpecies.getBP2Instance(nbloss);

		this.mz = mhplus;
		this.mz += SvgIon.mhplus;
	}

	public void setIonYstar1() {
		ionSpecies = SvgIonSpecies.getYstar1Instance();
		this.mz = -mnh3;
		this.mz += SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonYstar2() {
		ionSpecies = SvgIonSpecies.getYstar2Instance();
		this.mz = -mnh3 + mhplus;
		this.mz += SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonYO1() {
		ionSpecies = SvgIonSpecies.getYO1Instance();
		this.mz = -mh2o;
		this.mz += SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonYO2() {
		ionSpecies = SvgIonSpecies.getYO2Instance();
		this.mz = -mh2o + mhplus;
		this.mz += SvgIon.mh2o + SvgIon.mhplus;
	}

	public void setIonBstar1() {
		ionSpecies = SvgIonSpecies.getBstar1Instance();
		this.mz = -mnh3;
		this.mz += SvgIon.mhplus;
	}

	public void setIonBstar2() {
		ionSpecies = SvgIonSpecies.getBstar2Instance();
		this.mz = -mnh3 + mhplus;
	}

	public void setIonBO1() {
		ionSpecies = SvgIonSpecies.getBO1Instance();
		this.mz = -mh2o;
		this.mz += SvgIon.mhplus;
	}

	public void setIonBO2() {
		ionSpecies = SvgIonSpecies.getBO2Instance();
		this.mz = -mh2o + mhplus;
		this.mz += SvgIon.mhplus;
	}

	public void setIonA1() {
		ionSpecies = SvgIonSpecies.getA1Instance();

		this.mz = -mco;
		this.mz += SvgIon.mhplus;
	}

	public void setIonC1() {
		ionSpecies = SvgIonSpecies.getC1Instance();
		this.mz = SvgIon.mnh3 + SvgIon.mhplus;
	}

	public void setIonC2() {
		ionSpecies = SvgIonSpecies.getC2Instance();
		this.mz = SvgIon.mnh3 + SvgIon.mhplus;
		this.mz += SvgIon.mhplus;
	}

	public void setIonZ1() {
		ionSpecies = SvgIonSpecies.getZ1Instance();
		this.mz = SvgIon.moxygen - mN + mhplus;
	}

	public void setIonZ2() {
		ionSpecies = SvgIonSpecies.getZ2Instance();
		this.mz = SvgIon.moxygen - mN + mhplus;
		this.mz += SvgIon.mhplus;
	}

	public int getNumber() {
		return number;

	}

	public void setNumber(int number) {
		this.number = number;
	}

	public float getMz() {
		return mz;
	}

	public void setMz(float mz) {
		this.mz = mz;
	}

	public String getName() {
		String name = this.ionSpecies.getName();
		if (number != null) {
			name += number;
		}
		return name;
	}

	public void setSequenceMass(float mass) {
		this.mz += mass;

		this.mz = this.mz / this.ionSpecies.getZ();
	}

	public SvgIonSpecies getSvgIonSpecies() {
		this.ionSpecies.setPriority(this.getPriority());
		return this.ionSpecies;
	}

	@Override
	public boolean equals(Object aThat) {
		// System.out.println("coucou");
		if (this == aThat)
			return true;
		if (!(aThat instanceof SvgIon))
			return false;
		SvgIon ion = (SvgIon) aThat;
		if (number.equals(ion.number)) {
			// System.out.println(ionSpecies.getCssClassname()
			// + ion.ionSpecies.getCssClassname());
			if (this.ionSpecies.equals(ion.ionSpecies)) {

				return true;
			}
		}
		return false;
	}

	/**
	 * tell if the current ion has priority over an other
	 *
	 */
	public boolean priority_over(SvgIon toCompare) {

		// y >> b >> y++ >> b++ >> a >> bO >> bO++
		if (this.priority > toCompare.getPriority()) {
			return (false);
		}
		return (true);
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int i) {
		this.priority = i;
	}

	public int compareTo(SvgIon toCompare) {
		if (this.priority > toCompare.getPriority()) {
			return (1);
		}
		if (this.priority < toCompare.getPriority()) {
			// -1 less than
			return (-1);
		}
		return 0; // equal
	}

	static public float getMhPlus() {
		return mhplus;

	}

	public int getZ() {
		return this.ionSpecies.getZ();
	}
}
