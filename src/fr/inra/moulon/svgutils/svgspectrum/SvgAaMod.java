package fr.inra.moulon.svgutils.svgspectrum;

import java.net.URI;
import java.net.URISyntaxException;

public class SvgAaMod {
	private float mass;
	private String psiModAccession = null;
	private String label = null;
	private String name = null;

	public SvgAaMod(float mass) {
		this.mass = mass;
		label = "" + mass;
	}

	public String getPsiModAccession() {
		return psiModAccession;
	}

	public void setPsiModAccession(String psiModAccession) {
		this.psiModAccession = psiModAccession;
		label = "" + mass + " (" + psiModAccession + ")";
	}

	public float getMass() {
		return mass;
	}

	@Override
	public String toString() {
		return label;
	}

	@Override
	public int hashCode() {
		if (psiModAccession != null) {
			return psiModAccession.hashCode();
		}
		String strCode = "" + mass;
		return strCode.hashCode();
	}

	@Override
	public boolean equals(Object aThat) {
		// System.out.println("coucou");
		if (this == aThat)
			return true;
		if (!(aThat instanceof SvgAaMod))
			return false;

		if (this.hashCode() == aThat.hashCode()) {
			SvgAaMod aaModToCompar = (SvgAaMod) aThat;
			if (psiModAccession != null
					|| aaModToCompar.getPsiModAccession() != null) {
				if (psiModAccession.equals(aaModToCompar.getPsiModAccession())) {
					return true;
				} else {
					return false;
				}
			} else {
				if (this.mass == aaModToCompar.mass) {
					return true;
				}
			}
		}
		return false;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public URI getURI() throws URISyntaxException {
		// http://www.ebi.ac.uk/ontology-lookup/?termId=
		if (psiModAccession != null) {
			String uri = "http://www.ebi.ac.uk/ontology-lookup/?termId="
					+ psiModAccession;
			return new URI(uri);
		}
		return null;
	}

}
