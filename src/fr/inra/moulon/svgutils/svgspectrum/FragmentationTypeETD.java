package fr.inra.moulon.svgutils.svgspectrum;

import java.util.ArrayList;

public class FragmentationTypeETD extends FragmentationTypeBase {
	// ETD/ECD => c, z, sans perte, y (optionnel)

	private static FragmentationTypeETD instance = null;

	private FragmentationTypeETD() {
	}

	public static FragmentationTypeETD getInstance() {
		if (null == instance) { // Premier appel
			instance = new FragmentationTypeETD();
		}
		return instance;

	}

	@Override
	public void addNterIons(ArrayList<SvgIon> iontable, int num,
			ArrayList<SvgAminoAcid> sequence, int exp_z) {

		float mass = 0;
		for (int i = 0; i < num; i++) {
			mass += sequence.get(i).getMass();
		}
		// ion c, z=1
		tmpIon = new SvgIon();
		tmpIon.setIonC1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(1);

		iontable.add(tmpIon);

		// ion c, z=2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonC2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(3);

			iontable.add(tmpIon);
		}

	}

	@Override
	public void addCterIons(ArrayList<SvgIon> iontable, int num,
			ArrayList<SvgAminoAcid> sequence, int exp_z) {
		float mass = 0;
		for (int i = 0; i < num; i++) {
			mass += sequence.get(sequence.size() - i - 1).getMass();
		}
		// ion y, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonY1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(5);

		iontable.add(tmpIon);

		// ion y, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonY2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(6);

			iontable.add(tmpIon);
		}

		// z
		tmpIon = new SvgIon();
		tmpIon.setIonZ1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(2);

		iontable.add(tmpIon);

		// ion bO, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonZ2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(4);

			iontable.add(tmpIon);
		}

		// ion y*, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonYstar1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(10);

		iontable.add(tmpIon);

		// ion y*, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonYstar2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(11);

			iontable.add(tmpIon);
		}

		// ion yO, z = 1
		tmpIon = new SvgIon();
		tmpIon.setIonYO1();
		tmpIon.setNumber(num);
		tmpIon.setSequenceMass(mass);
		tmpIon.setPriority(12);

		iontable.add(tmpIon);

		// ion yO, z = 2
		if (exp_z > 1) {
			tmpIon = new SvgIon();
			tmpIon.setIonYO2();
			tmpIon.setNumber(num);
			tmpIon.setSequenceMass(mass);
			tmpIon.setPriority(13);

			iontable.add(tmpIon);
		}

	}

}