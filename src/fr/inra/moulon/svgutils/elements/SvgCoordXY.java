package fr.inra.moulon.svgutils.elements;

public class SvgCoordXY {
	float x;
	float y;

	public SvgCoordXY(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public SvgCoordXY(SvgCoordXY coord) {
		this.x = coord.x;
		this.y = coord.y;
	}

	public Float getX() {
		return x;
	}

	public Float getY() {
		return y;
	}

	public String toString() {
		return "x=" + x + " y=" + y;
	}

	public SvgCoordXY minus(SvgCoordXY coordTranslate) {
		SvgCoordXY result = new SvgCoordXY((x - coordTranslate.x), (y - coordTranslate.y));
		return result;
	}

}
