package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgRect extends SvgElementPositionBase implements SvgBlock {

	protected SvgRect(SvgDocument document, float width, float height) {
		super(document, "rect");
		element.setAttributeNS(null, "width", "" + width);
		element.setAttributeNS(null, "height", "" + height);
	}

	public float getWidth() {
		return new Float(element.getAttributeNS(null, "width"));
	}

	public float getHeight() {
		return new Float(element.getAttributeNS(null, "height"));
	}

	public void setWidth(float width) {
		element.setAttributeNS(null, "width", "" + width);
	}

	public void setHeight(float height) {
		element.setAttributeNS(null, "height", "" + height);
	}

}
