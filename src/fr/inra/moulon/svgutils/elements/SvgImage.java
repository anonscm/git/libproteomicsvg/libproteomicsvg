package fr.inra.moulon.svgutils.elements;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import javax.imageio.ImageIO;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.utils.Base64;

public class SvgImage extends SvgElementPositionBase implements SvgBlock {

	protected SvgImage(SvgDocument document, URI uri, float width, float height) {
		super(document, "image");
		element.setAttributeNS(null, "width", "" + width);		
		element.setAttributeNS(null, "height", "" + height);
		this.setHref(uri);
	}

	protected SvgImage(SvgDocument document, InputStream is) throws IOException {
		super(document, "image");
		BufferedImage img = ImageIO.read(is);
		Integer width = img.getWidth(null);
		Integer height = img.getHeight(null);

		//this.gel_image_width = width.floatValue();
		//this.gel_image_height = height.floatValue();
		element.setAttributeNS(null, "width", "" + width);		
		element.setAttributeNS(null, "height", "" + height);

		//svg_graphic.goToGroup("gel_image");
		// width="300px" height="200px"
		//gel_image_graph.embedJpgImage(0, 0, width, height, img, "");
		
		element.setAttributeNS(SvgDocument.xlinkNS, "xlink:href", encodeBufferedImage(img));
	}

	//		image.setAttributeNS(xlinkNS, "xlink:href", href);
//
	/*	public Element embedJpgImage(float xcoord, float ycoord, float width,
				float height, BufferedImage img, String classname) throws Exception {
			StringBuffer encoded_image_buffer = new StringBuffer(
					"data:image/jpg;base64,");
			// encoded_image_buffer.append("\n");

			try {

				ByteArrayOutputStream out = new ByteArrayOutputStream();
				ImageIO.write(img, "jpg", out);
				// byte[] bytes = out.toByteArray();

				encoded_image_buffer.append(Base64.encodeBytes(out.toByteArray()));
				// .encodeFromFile(img.image_jpg_filename.toString()));

			} catch (IOException e) {
				System.out.println(e.getMessage());
			}

			if (encoded_image_buffer.length() > 0) {

			} else {
				throw new Exception("encoded_image_buffer empty");
			}

			return svgImage(this.svgdoc, currentNode, xcoord, ycoord, width,
					height, encoded_image_buffer.toString(), classname);
		}
*/
	
/*
 * 	public void embedGelImage(InputStream is) throws Exception {
		BufferedImage img = ImageIO.read(is);
		Integer width = img.getWidth(null);
		Integer height = img.getHeight(null);

		this.gel_image_width = width.floatValue();
		this.gel_image_height = height.floatValue();

		this.getSvgRoot().setWidth(width);
		this.getSvgRoot().setHeight(height);

		//svg_graphic.goToGroup("gel_image");
		// width="300px" height="200px"
		gel_image_graph.embedJpgImage(0, 0, width, height, img, "");

	}
 */

	private String encodeBufferedImage(BufferedImage img) {

		StringBuffer encoded_image_buffer = new StringBuffer(
				"data:image/jpeg;base64,");
		// encoded_image_buffer.append("\n");

		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(img, "jpg", out);
			// byte[] bytes = out.toByteArray();

			encoded_image_buffer.append(Base64.encodeBytes(out.toByteArray()));
			// .encodeFromFile(img.image_jpg_filename.toString()));

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		return encoded_image_buffer.toString();
	}

	public void setHref(URI uri) {
		if (uri != null) {
			element.setAttributeNS(SvgDocument.xlinkNS, "xlink:href", uri.toString());
		}
	}

	public float getWidth() {
		return new Float(element.getAttributeNS(null, "width"));		
	}

	public float getHeight() {
		return new Float(element.getAttributeNS(null, "height"));		
	}

	public String getHref() {
		return element.getAttributeNS(SvgDocument.xlinkNS, "xlink:href");		
	}

}
