package fr.inra.moulon.svgutils.elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgElementDrawBase extends SvgElementBase {

	protected SvgElementDrawBase(SvgDocument document, String elementName) {
		super(document, elementName);
	}

	public SvgImage newImage(URI image_url, Float gel_image_width,
			Float gel_image_height) {
		SvgImage svgGroup = new SvgImage(document, image_url, gel_image_width,
				gel_image_height);

		this.appendChild(svgGroup);

		return svgGroup;
	}

	public SvgImage newImage(InputStream img) throws IOException {
		SvgImage svgGroup = new SvgImage(document, img);

		this.appendChild(svgGroup);

		return svgGroup;
	}

	public SvgPath newPath() {
		SvgPath svgGroup = new SvgPath(document);

		this.appendChild(svgGroup);

		return svgGroup;
	}

	public SvgRect newRect(float width, float height) {
		SvgRect svgGroup = new SvgRect(document, width, height);

		this.appendChild(svgGroup);

		return svgGroup;
	}

	public SvgGroup newGroup() {

		SvgGroup svgGroup = new SvgGroup(document);

		this.appendChild(svgGroup);

		return svgGroup;

	}

	public SvgAnchor newAnchor(URI url) {
		SvgAnchor svgGroup = new SvgAnchor(document, url);

		this.appendChild(svgGroup);

		return svgGroup;
	}

	public SvgUse newUse(SvgSymbol symbol, float x, float y) {
		SvgUse svgGroup = new SvgUse(document, symbol, x, y);

		this.appendChild(svgGroup);

		return svgGroup;
	}

	public SvgCircle newCircle(float cx, float cy, float ray) {

		SvgCircle svgGroup = new SvgCircle(document, cx, cy, ray);

		this.appendChild(svgGroup);

		return svgGroup;

	}

	public SvgLine newLine(float xorig, float yorig, float xdest, float ydest) {

		SvgLine svgGroup = new SvgLine(document, xorig, yorig, xdest, ydest);

		this.appendChild(svgGroup);

		return svgGroup;

	}

	public SvgText newText() {

		SvgText svgText = new SvgText(document);

		this.appendChild(svgText);

		return svgText;

	}
	

	public SvgText newText(String string) {

		SvgText svgText = new SvgText(document, string);

		this.appendChild(svgText);

		return svgText;
	}


	public SvgTextInTspan newTextInTspan(String content) {
		SvgTextInTspan svgText = new SvgTextInTspan(document, content);

		this.appendChild(svgText);

		return svgText;
	}

}// end class SvgGraphics

