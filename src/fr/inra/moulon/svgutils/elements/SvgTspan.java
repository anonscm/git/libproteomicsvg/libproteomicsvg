package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgTspan extends SvgElementPositionBase {

	protected SvgTspan(SvgDocument document, String content) {
		super(document, "tspan");
		this.setTextContent(content);
	}
}
