package fr.inra.moulon.svgutils.elements;

import org.w3c.dom.CDATASection;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgScript extends SvgElementBase {

	private CDATASection cdata;

	public SvgScript(SvgDocument document, String content) {
		super(document, "script");
		this.element.setAttributeNS(null, "type", "text/ecmascript");

		cdata = document.getDocument().createCDATASection(content);
		this.element.appendChild(cdata);
	}

	public void appendText(String code) {
		cdata.appendData(code);
	}
}
