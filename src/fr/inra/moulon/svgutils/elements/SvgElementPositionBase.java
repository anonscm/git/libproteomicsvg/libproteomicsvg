package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgElementPositionBase extends SvgElementBase {

	protected SvgElementPositionBase(SvgDocument document, String elementName) {
		super(document, elementName);
	}

	public void setX(float x) {
		element.setAttributeNS(null, "x", "" + x);
	}

	public void setY(float y) {
		element.setAttributeNS(null, "y", "" + y);
	}

	public Float getY() {
		String value = element.getAttributeNS(null, "y");
		if (value == null) {
			return null;
		}
		return new Float(value);
	}

	public Float getX() {
		String value = element.getAttributeNS(null, "x");
		if (value == null) {
			return null;
		}
		return new Float(value);
	}
}
