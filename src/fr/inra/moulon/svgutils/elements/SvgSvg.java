package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgSvg extends SvgElementDrawBase {

	public SvgSvg(SvgDocument document) {
		super(document, "svg");
	}

	public void setWidth(float width) {
		element.setAttributeNS(null, "width", "" + width);
	}

	public void setHeight(float height) {
		element.setAttributeNS(null, "height", "" + height);
	}

	public void setPreserveAspectRatio(String string) {
		element.setAttributeNS(null, "preserveAspectRatio", "" + string);
	}

	public SvgDefs newDefs() {
		SvgDefs svgDefs = new SvgDefs(document);

		this.appendChild(svgDefs);

		return svgDefs;
	}

	public SvgStyle newStyle() {
		SvgStyle svgDefs = new SvgStyle(document);

		this.appendChild(svgDefs);
		this.setAttributeNS(null, "contentStyleType", "text/css");

		return svgDefs;
	}

	public SvgScript newScript() {
		SvgScript svgDefs = new SvgScript(document, "");

		this.appendChild(svgDefs);

		return svgDefs;
	}

	public void setViewBox(float x, float y, float width, float height) {
		element.setAttributeNS(null, "viewBox", "" + x + " " + y + " " + width
				+ " " + height);
	}

	public String getViewBox() {
		return element.getAttributeNS(null, "viewBox");
	}

	public void setXmlnsXlink() {
		element.setAttributeNS(SvgDocument.xmlNS, "xmlns:xlink",
				SvgDocument.xlinkNS);
	}

	public Float getWidth() {
		String width = this.getAttributeNS(null, "width");
		if (width == null) {
			return null;
		}
		return new Float(width);
	}

	public Float getHeight() {
		String width = this.getAttributeNS(null, "height");
		if (width == null) {
			return null;
		}
		return new Float(width);
	}

	public void setOnLoad(String onload) {
		element.setAttributeNS(null, "onload", onload);
	}

}
