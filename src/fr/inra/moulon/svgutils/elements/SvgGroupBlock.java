package fr.inra.moulon.svgutils.elements;

import java.util.ArrayList;
import java.util.HashSet;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgGroupBlock extends SvgGroup implements SvgBlock {
	float width;
	float height;

	private ArrayList<SvgGroupBlock> arrBlocks = new ArrayList<SvgGroupBlock>(0);
	private Object embeddedData;

	public SvgGroupBlock(SvgDocument document) {
		super(document);
	}

	public void registerBlock(SvgGroupBlock block) {
		arrBlocks.add(block);
	}

	public float getWidth() {
		//System.out.println(width);
		return width;
	}

	public float getHeight() {
		return height;
	}

	protected void setHeight(float y) {
		this.height = y;
	}

	protected void setWidth(float y) {
		this.width = y;
	}

	public HashSet<Object> getDataSet(SvgCoordXY coord) throws Exception {
		// transform coords to pass it to childs
		SvgCoordXY coordTrans = this.getCoordTranslation(coord);

		// System.out.println(coordTrans.toString());
		HashSet<Object> objects = new HashSet<Object>();

		if (this.contains(coordTrans)) {
			//System.out.println("getDataSet coord "+coordTrans);
			if (getData( )!=null) {
				objects.add(getData());
			}
			
			for (SvgGroupBlock block : arrBlocks) {
				HashSet<Object> objList = block.getDataSet(coordTrans);
				objects.addAll(objList);
			}
		}
		return objects;
	}
	public void setData(Object obj) {
		this.embeddedData = obj;
	}

	public Object getData() {
		return this.embeddedData;
	}

	private boolean contains(SvgCoordXY coord) {
		//System.out.println("contains width "+this.width);
		//System.out.println("contains height "+this.height);
		//System.out.println("contains coord "+coord);
		if (coord.getX() < 0) {
		//	System.out.println("no");
			return false;
		}
		if (coord.getY() < 0) {
			//System.out.println("no");
			return false;
		}
		if (coord.getX() > this.width) {
			//System.out.println("no");
			return false;
		}
		if (coord.getY() > this.height) {
			//System.out.println("no");
			return false;
		}
		//System.out.println("yes");
		return true;
	}

	private SvgCoordXY getCoordTranslation(SvgCoordXY coord) throws Exception {
		//System.out.println("getCoordTranslation coord "+coord);
		SvgCoordXY coordTranslate = this.getCoordTransformTranslate();
		SvgCoordXY coordResult = new SvgCoordXY(coord);
		if (coordTranslate != null) {
			coordResult = coord.minus(coordTranslate);
		}
		return coordResult;
	}

}
