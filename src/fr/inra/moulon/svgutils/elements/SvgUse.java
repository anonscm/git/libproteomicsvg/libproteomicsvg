/*******************************************************************************
 * Copyright (c) 2011-06-28 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/
package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgUse extends SvgElementPositionBase {
	protected SvgUse(SvgDocument document, SvgSymbol symbol, float x, float y) {
		super(document, "use");
		/*
		 * public Element use(float x, float y, Element symbol) { /* <use x="45"
		 * y="10" width="10" height="10" xlink:href="#MySymbol" />
		 */
		/*
		 * Element use = this.svgdoc.createElementNS(svgNS, "use");
		 * this.currentNode.appendChild(use); use.setAttributeNS(null, "x", "" +
		 * x); use.setAttributeNS(null, "y", "" + y);
		 * use.setAttributeNS(xlinkNS, "xlink:href", "#" +
		 * symbol.getAttributeNS(null, "id")); return use; }
		 */

		// <circle cx="100" cy="50" r="40"
		this.setX(x);
		this.setY(y);
		this.setAttributeNS(SvgDocument.xlinkNS, "xlink:href",
				"#" + symbol.getAttributeNS(null, "id"));
	}

}
