package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgTextInTspan extends SvgText {

	protected SvgTextInTspan(SvgDocument document, String content) {
		super(document);
		this.newTspan(content);
	}

}
