package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgDefs extends SvgElementBase {

	protected SvgDefs(SvgDocument document) {
		super(document, "defs");
	}

	public SvgSymbol newSymbol(String id) {
		SvgSymbol symbol = new SvgSymbol(this.document, id);
		this.appendChild(symbol);	
		return symbol;
	}
	public SvgClipPath newSvgClipPath(String id) {
		SvgClipPath symbol = new SvgClipPath(this.document, id);
		this.appendChild(symbol);	
		return symbol;
	}

}
