package fr.inra.moulon.svgutils.elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgGroup extends SvgElementDrawBase {

	// protected static Pattern viewBoxPattern =
	// Pattern.compile("^(.*[^ ]) (.*[^ ]) (.*[^ ]) (.*[^ ])$");
	protected static Pattern coordTranslatePattern = Pattern
			.compile("translate\\((.*[^,]),(.*[^)])\\)");

	public SvgGroup(SvgDocument document) {
		super(document, "g");
	}

	public void translate(float x, float y) {
		element.setAttributeNS(null, "transform", "translate(" + x + "," + y
				+ ")");
	}

	protected SvgCoordXY getCoordTransformTranslate() throws Exception {
		String transform = this.element.getAttributeNS(null, "transform");
		if (transform == null) {
			return null;
		}
		if (transform.isEmpty()) {
			return null;
		}
		// System.out.println("getCoordTransformTranslate  transform "+transform);
		Matcher m = SvgGroup.coordTranslatePattern.matcher(transform);
		if (m.find()) {
			// System.out.println("getViewBoxOffset  pattern found "+m.group(3));
			// System.out.println("getViewBoxOffset  pattern found "+m.group(4));
			SvgCoordXY orig = new SvgCoordXY(new Float(m.group(1)), new Float(
					m.group(2)));
			return orig;
		} else {
			// System.out.println("no pattern found in " + viewBox);
			throw new Exception("no pattern found in " + transform);
		}
	}

	public SvgGroup newGroup() {

		SvgGroup svgGroup = new SvgGroup(document);

		this.appendChild(svgGroup);

		return svgGroup;

	}

	public void transformMatrix(float ratiox, float rotatea, float rotateb,
			float ratioy, float posx, float posy) {
		element.setAttributeNS(null, "transform", "matrix(" + ratiox + ","
				+ rotatea + "," + rotateb + "," + ratioy + "," + posx + ","
				+ posy + ")");
	}

	public void setOnMouseOver(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onmouseover", action);
		}
	}

	public void setOnClick(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onclick", action);
		}
	}

	public void setOnMouseOut(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onmouseout", action);
		}
	}

	// clip-path="url(#clipPath5334)">
	public void setClipPath(SvgClipPath clippath) {
		if (clippath != null) {
			element.setAttributeNS(null, "clip-path",
					"url(#" + clippath.getId() + ")");
		}
	}

	//http://www.w3.org/TR/SVG/interact.html#SVGEvents
	public void setOnMouseMove(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onmousemove", action);
		}
	}

	public void setOnMouseDown(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onmousedown", action);
		}
	}
	public void setOnMouseUp(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onmouseup", action);
		}
	}

}
