package fr.inra.moulon.svgutils.elements;

import java.net.URI;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgAnchor extends SvgElementDrawBase {

	String href;
	protected SvgAnchor(SvgDocument document, URI uri) {
		super(document, "a");
		setHref(uri);
	}

	public void setHref(URI uri) {
		if (uri != null) {
			element.setAttributeNS(SvgDocument.xlinkNS, "xlink:href", uri.toString());
			href = uri.toString();
		}
	}

	public void setNewWindows() {
		// onclick="window.open(this.href); return false;"
		element.setAttributeNS(null, "onclick",
				"window.open("+this.getHref()+"); return false;");
	}

	private String getHref() {
		//String value = element.getAttributeNS(SvgDocument.xlinkNS, "xlink:href");
		return href;
	}

}
