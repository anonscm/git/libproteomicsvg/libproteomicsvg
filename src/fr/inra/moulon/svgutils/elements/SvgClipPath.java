package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgClipPath extends SvgElementDrawBase {

	public SvgClipPath(SvgDocument document, String id) {
		super(document, "clipPath");
		// clipPathUnits="userSpaceOnUse"
		element.setAttributeNS(null, "clipPathUnits", "userSpaceOnUse");
		this.setId(id);
	}

}
