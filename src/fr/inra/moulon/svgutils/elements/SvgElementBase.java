package fr.inra.moulon.svgutils.elements;

import org.w3c.dom.*;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgElementBase {

	protected SvgDocument document;
	protected Element element;

	public SvgElementBase(SvgDocument document, String elementName) {
		this.document = document;
		element = document.getDocument().createElementNS(SvgDocument.svgNS,
				elementName);
	}

	public SvgDocument getSvgDocument() {
		return document;
	}

	public void setId(String id) {
		element.setAttributeNS(null, "id", id);
	}
	
	public String getId() {
		String value = element.getAttributeNS(null, "id");
		if (value == null) {
			return null;
		}
		return value;
	}

	public void setCssClass(String className) {
		element.setAttributeNS(null, "class", className);
	}

	public Element getElement() {
		return this.element;
	}

	public SvgElementBase appendChild(SvgElementBase newChild)
			throws DOMException {
		element.appendChild(newChild.getElement());
		return this;
	}

	protected Node appendChild(Node newChild) throws DOMException {
		return element.appendChild(newChild);
	}

	protected String getNamespaceURI() {
		return element.getNamespaceURI();
	}

	protected Document getOwnerDocument() {
		return element.getOwnerDocument();
	}

	protected String getTextContent() throws DOMException {
		return element.getTextContent();
	}

	protected void setPrefix(String prefix) throws DOMException {
		element.setPrefix(prefix);
	}

	protected void setTextContent(String textContent) throws DOMException {
		if (textContent != null) {
			element.setTextContent(textContent);
		}
	}

	protected String getAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		return element.getAttributeNS(namespaceURI, localName);
	}

	protected String getTagName() {
		return element.getTagName();
	}

	protected boolean hasAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		return element.hasAttributeNS(namespaceURI, localName);
	}

	protected void setAttributeNS(String namespaceURI, String qualifiedName,
			String value) throws DOMException {
		element.setAttributeNS(namespaceURI, qualifiedName, value);
	}

}// end class SvgGraphics

