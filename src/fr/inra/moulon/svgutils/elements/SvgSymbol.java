package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgSymbol extends SvgElementDrawBase {

	protected SvgSymbol(SvgDocument document, String id) {
		super(document, "symbol");
		this.setId(id);
	}

}
