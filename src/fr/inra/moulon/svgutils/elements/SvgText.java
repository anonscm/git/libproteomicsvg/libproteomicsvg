package fr.inra.moulon.svgutils.elements;

import java.text.DecimalFormat;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgText extends SvgElementPositionBase {
	protected SvgText(SvgDocument document) {
		super(document, "text");
	}

	protected SvgText(SvgDocument document, String string) {
		super(document, "text");
		this.setTextContent(string);
	}

	public SvgTspan newTspan(String content) {
		SvgTspan tspan = new SvgTspan(document, content);

		this.appendChild(tspan);

		return tspan;

	}

	public void setdx(float dx) {
		element.setAttributeNS(null, "dx", "" + dx);
	}

	public void setdy(float dy) {
		element.setAttributeNS(null, "dy", "" + dy);
	}

	public void rotate(float degree, float centerx, float centery) {
		element.setAttributeNS(null, "transform", "rotate(" + degree + " "
				+ centerx + "," + centery + ")");
	}

	public void printNumber(Float ipos, DecimalFormat formClassic) {
		if (ipos == null) {
			return;
		}
		if (formClassic == null) {
			return;
		}
		String number = formClassic.format(ipos);
		String[] part = number.split("E", 2);
		if (part.length == 1) {
			newTspan(number);
		}
		else {
			newTspan(part[0]);
			SvgTspan exp = newTspan("e"+part[1]);
			exp.setCssClass("msup");
		}
	}

}
