package fr.inra.moulon.svgutils.elements;

public interface SvgBlock {
	
	public float getWidth();
	public float getHeight();

}
