package fr.inra.moulon.svgutils.elements;

import fr.inra.moulon.svgutils.SvgDocument;

/*
 *
 public static Element svgLine(Document document, Element parent,
 float xBeginCoor, float yBeginCoor, float width, float height,
 String classname) {

 // create the rectangle
 Element line = document.createElementNS(svgNS, "path");
 parent.appendChild(line);
 line.setAttributeNS(null, "d", "M " + xBeginCoor + "," + yBeginCoor
 + " L " + width + "," + height);
 if (classname != null) {
 if (!classname.equals("")) {
 line.setAttributeNS(null, "class", classname);
 }
 }

 return line;
 }

 */
public class SvgLine extends SvgPath {

	protected SvgLine(SvgDocument document, float xorig, float yorig,
			float xdest, float ydest) {
		super(document);
		this.moveto(xorig, yorig);
		this.lineto(xdest, ydest);
	}

}
