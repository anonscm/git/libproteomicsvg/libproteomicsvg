/*******************************************************************************
 * Copyright (c) 2011-06-28 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/
package fr.inra.moulon.svgutils.elements;

import org.w3c.dom.CDATASection;

import fr.inra.moulon.svgutils.SvgDocument;

public class SvgStyle extends SvgElementBase {

	private CDATASection cdataStyle;

	protected SvgStyle(SvgDocument document) {
		super(document, "style");
		
		cdataStyle = this.document.getDocument().createCDATASection(" ");
		this.appendChild(cdataStyle);
		this.setAttributeNS(null, "type", "text/css");
	}

	public void setCssSyntax(String csssyntax) {
		cdataStyle.setData(csssyntax);
		
	}

}
