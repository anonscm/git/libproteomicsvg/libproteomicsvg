/*******************************************************************************
 * Copyright (c) 2010 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.xhtmlutils;

import java.io.IOException;
import java.io.OutputStream;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;

public class HtmlDocument extends XhtmlDocument {

	public HtmlDocument() {
		super();

	}

	@Override
	public Document createDocument() {
		Document document = null;
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			dbfac.setValidating(false);

			// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
			// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
			// DocumentType svgDocType = new DocumentType();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();

			document = docBuilder.newDocument();

			// document.createAttributeNS(xlinkNS, "xlink");

			Element root = document
					.createElementNS(this.getNameSpace(), "html");
			document.appendChild(root);

		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}// end catch
		return document;
	}// end getDocument

	@Override
	public String getNameSpace() {
		return null;
	}

	@Override
	protected Transformer modifyTransformer(Transformer transformer) {
		// Sets the standalone property in the first line of
		// the output file.
		// transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
		String version = System.getProperty("java.version");
		transformer.setOutputProperty(OutputKeys.METHOD, "html");
		//
		// <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
		/*
		 * Element meta = xmldoc.createElementNS(this.getNameSpace(), "META");
		 * meta.setAttribute("http-equiv", "Content-Type");
		 * meta.setAttribute("content", "text/html; charset=UTF-8");
		 * 
		 * head.insertBefore(meta, title);
		 */

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
		// OutputKeys.OMIT_XML_DECLARATION

		// <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		// "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
		// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

		if (version.compareTo("1.6.0_22") >= 0) {
		} else {
			// buggy doctype in html for java version < 1.6.0_22
			this.setWriteDocType(false);
		}
		if (this.getWriteDocType()) {
			// if (this.getContainsSvg()) {
			/*
			 * <!DOCTYPE html PUBLIC
			 * "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN"
			 * "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd" >
			 */
			/*
			 * transformer .setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
			 * "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN");
			 * transformer .setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
			 * "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd");
			 */
			// } else {
			/*
			 * <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
			 * "http://www.w3.org/TR/html4/loose.dtd">
			 */
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
					"-//W3C//DTD HTML 4.01 Transitional//EN");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
					"http://www.w3.org/TR/html4/loose.dtd");

			// }
		}
		return transformer;
	}

	@Override
	protected void docToOutputStream(OutputStream out) {
		String version = System.getProperty("java.version");
		if (version.compareTo("1.6.0_22") >= 0) {
		} else {
			if (this.getWriteDocType()) {
				String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n";
				try {
					out.write(docType.getBytes());
				} catch (IOException e) {
					//ignoring problem writing doctype
				}
				// buggy doctype in html for java version < 1.6.0_22
				this.setWriteDocType(false);
			}
		}
		docToOutputStream(this, out);
	}

}// end class SvgGraphics

