package fr.inra.moulon.xhtmlutils.elements;

import org.w3c.dom.CDATASection;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlScript extends XhtmlElementBase {

	private CDATASection cdata;

	public XhtmlScript(XhtmlDocument document, String content) {
		super(document, "script");
		
		
		cdata = document.getDocument().createCDATASection(content);
		this.element.appendChild(cdata);
	}
	
	public void appendText(String code) {
		cdata.appendData(code);
	}
}
