package fr.inra.moulon.xhtmlutils.elements;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlAnchor extends XhtmlGenericTextContainer {

	public XhtmlAnchor(XhtmlDocument document, URI uri, String content) {
		super(document, "a", content);
		setHref(uri);
	}

	public void setHref(URI uri) {
		if (uri != null) {
			element.setAttributeNS(null, "href", uri.toString());
		}
	}

	public void setNewWindows() {
		// onclick="window.open(this.href); return false;"
		element.setAttributeNS(null, "onclick",
				"window.open(this.href); return false;");
	}

	public void setOnClick(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onclick", action);
		}
	}

}
