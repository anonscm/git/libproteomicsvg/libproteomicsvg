package fr.inra.moulon.xhtmlutils.elements;

import java.util.UUID;

import org.w3c.dom.*;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlElementBase {

	protected XhtmlDocument document;
	protected Element element;

	public XhtmlElementBase(XhtmlDocument document, String elementName) {
		this.document = document;
		element = document.getDocument().createElementNS(
				document.getNameSpace(), elementName);
	}

	public XhtmlDocument getXhtmlDocument() {
		return document;
	}

	protected void removeAll() {
		while (this.element.hasChildNodes()) {
			this.element.removeChild(this.element.getFirstChild());
		}
	}

	public XhtmlDocument xhtmlSelect() {
		return document;
	}

	public String getId() {
		String id = element.getAttributeNS(null, "id");
		if (id.isEmpty()) {
			id = null;
		}
		return id;
	}

	final public void setId(String id) {
		element.setAttributeNS(null, "id", id);
	}

	final public void setUniqueId() {
		element.setAttributeNS(null, "id", "x"
				+ UUID.randomUUID().toString().replace("-", "")
						.substring(0, 10));
	}

	public XhtmlElementBase setCssClass(String className) throws Exception {
		try {
			element.setAttributeNS(null, "class", className);
		} catch (Exception e) {
			throw new Exception("ERROR in setCssClass : " + e.getMessage());
		}
		return this;
	}

	protected Element getElement() {
		return this.element;
	}

	public XhtmlElementBase appendChild(XhtmlElementBase newChild)
			throws DOMException {
		element.appendChild(newChild.getElement());
		return this;
	}

	protected Node appendChild(Node newChild) throws DOMException {
		return element.appendChild(newChild);
	}

	protected String getNamespaceURI() {
		return element.getNamespaceURI();
	}

	protected Document getOwnerDocument() {
		return element.getOwnerDocument();
	}

	protected String getTextContent() throws DOMException {
		return element.getTextContent();
	}

	protected void setPrefix(String prefix) throws DOMException {
		element.setPrefix(prefix);
	}

	protected void setTextContent(String textContent) throws DOMException {
		if (textContent != null) {
			element.setTextContent(textContent);
		}
	}

	protected String getAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		return element.getAttributeNS(namespaceURI, localName);
	}

	protected String getTagName() {
		return element.getTagName();
	}

	protected boolean hasAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		return element.hasAttributeNS(namespaceURI, localName);
	}

	protected void setAttributeNS(String namespaceURI, String qualifiedName,
			String value) throws DOMException {
		element.setAttributeNS(namespaceURI, qualifiedName, value);
	}

}// end class SvgGraphics

