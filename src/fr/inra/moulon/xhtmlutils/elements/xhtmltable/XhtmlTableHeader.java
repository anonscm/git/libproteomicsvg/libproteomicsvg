package fr.inra.moulon.xhtmlutils.elements.xhtmltable;

import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;

public class XhtmlTableHeader extends XhtmlGenericContainer {

	protected XhtmlTableHeader(XhtmlTable table, String content) {
		super(table.getXhtmlDocument(), "th", content);
	}

	public void setColSpan(int nbCols) {
		element.setAttributeNS(null, "colspan", "" + nbCols);
	}

}
