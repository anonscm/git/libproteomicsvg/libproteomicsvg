package fr.inra.moulon.xhtmlutils.elements.xhtmltable;

import org.w3c.dom.Element;

import fr.inra.moulon.xhtmlutils.elements.XhtmlElementBase;

public class XhtmlTableRow extends XhtmlElementBase {

	private XhtmlTable table;

	protected XhtmlTableRow(XhtmlTable table) {
		super(table.getXhtmlDocument(), "tr");
		this.table = table;
	}

	public XhtmlTableCell newCell(Object content) {
		if (content == null) {
			return newCell("");			
		}
		return newCell(content.toString());
	}
	public XhtmlTableCell newCell(String content) {
		XhtmlTableCell td = new XhtmlTableCell(table, content);
		this.appendChild(td);
		return td;
	}
	
	public XhtmlTableHeader newHeaderCell(String content) {
		XhtmlTableHeader th = new XhtmlTableHeader(table, content);
		this.appendChild(th);
		return th;
	}

	protected Element getElement() {
		return this.element;
	}

}
