package fr.inra.moulon.xhtmlutils.elements.xhtmltable;

import fr.inra.moulon.xhtmlutils.Html5Document;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;

public class XhtmlTableCell extends XhtmlGenericContainer {

	protected XhtmlTableCell(XhtmlTable table, String content) {
		super(table.getXhtmlDocument(), "td", content);

		if (this.document.getClass().equals(Html5Document.class)) {
			if ((content == null) || (content.isEmpty())) {
				this.element.setTextContent(" ");
			}
		}
	}
}
