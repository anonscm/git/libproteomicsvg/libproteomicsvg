package fr.inra.moulon.xhtmlutils.elements.xhtmltable;

import java.util.ArrayList;

import org.w3c.dom.Element;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlElementBase;

public class XhtmlTable extends XhtmlElementBase {

	private Element caption;
	private Element thead = null;
	private Element tbody = null;

	// private XhtmlTableRow currentTableRow = null;

	public XhtmlTable(XhtmlDocument document) {
		super(document, "table");
		caption = document.getDocument().createElementNS(document.getNameSpace(),
				"caption");
		caption.setTextContent("");
		this.element.appendChild(caption);
		thead = null;
		tbody = document.getDocument().createElementNS(document.getNameSpace(),
				"tbody");
		tbody.setTextContent("");
		this.element.appendChild(tbody);
		// document.getCurrentNode().appendChild(this.getElement());
	}

	protected void newTableHead() {
		thead = document.getDocument().createElementNS(document.getNameSpace(),
				"thead");
		this.element.insertBefore(thead, tbody);
	}

	public void setCaption(String captionText) {
		// caption.setNodeValue(captionText);
		caption.setTextContent(captionText);
	}

	public void addHeaderLine(ArrayList<String> titleList) {

		XhtmlTableRow tr = newHeaderLine();
		for (String title : titleList) {
			tr.newHeaderCell(title);
		}
	}

	public XhtmlTableRow newHeaderLine() {
		XhtmlTableRow tableHeaderLine = new XhtmlTableRow(this);
		if (thead == null) {
			newTableHead();
		}
		thead.appendChild(tableHeaderLine.getElement());
		return tableHeaderLine;
	}

	public XhtmlTableRow newTableRow() {
		XhtmlTableRow currentTableRow = new XhtmlTableRow(this);
		tbody.appendChild(currentTableRow.getElement());
		return currentTableRow;
	}

	public XhtmlDocument getXhtmlDocument() {
		return this.document;
	}

}
