package fr.inra.moulon.xhtmlutils.elements;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlForm;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;
import fr.inra.moulon.xhtmlutils.interfaces.XhtmlContainer;

public class XhtmlGenericContainer extends XhtmlGenericTextContainer implements
		XhtmlContainer {

	protected XhtmlGenericContainer(XhtmlDocument document, String elementName,
			Object content) {
		super(document, elementName, content);
	}

	protected XhtmlGenericContainer(XhtmlDocument document, String elementName) {
		super(document, elementName, null);
	}

	public XhtmlForm newForm(String method, String action) {

		XhtmlForm xhtmlForm = new XhtmlForm(document, method, action);

		this.appendChild(xhtmlForm);

		return xhtmlForm;

	}

	@Override
	public XhtmlP newP(String content) {

		XhtmlP xhtmlDiv = new XhtmlP(document, content);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public XhtmlDiv newDiv(String content) {

		XhtmlDiv xhtmlDiv = new XhtmlDiv(document, content);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public XhtmlHeader newHeader1(String content) {

		XhtmlHeader xhtmlHeader = new XhtmlHeader(this.document, 1, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;

	}

	@Override
	public XhtmlHeader newHeader2(String content) {

		XhtmlHeader xhtmlHeader = new XhtmlHeader(document, 2, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;

	}

	@Override
	public XhtmlHeader newHeader3(String content) {

		XhtmlHeader xhtmlHeader = new XhtmlHeader(document, 3, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;

	}

	@Override
	public XhtmlScript newScript(String content) {
		XhtmlScript xhtmlHeader = new XhtmlScript(document, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;
	}

	@Override
	public XhtmlTable newTable() {

		XhtmlTable xhtmlTable = new XhtmlTable(document);

		this.appendChild(xhtmlTable);

		return xhtmlTable;

	}

	@Override
	public XhtmlUl newUl() {

		XhtmlUl xhtmlDiv = new XhtmlUl(this.document);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public XhtmlOl newOl() {

		XhtmlOl xhtmlDiv = new XhtmlOl(this.document);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public XhtmlVideo newVideo(URI src, String alt) {

		XhtmlVideo xhtmlDiv = new XhtmlVideo(this.document, src, alt);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

}
