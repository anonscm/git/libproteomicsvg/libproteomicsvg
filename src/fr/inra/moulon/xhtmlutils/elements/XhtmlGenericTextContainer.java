package fr.inra.moulon.xhtmlutils.elements;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.xhtmlutils.Html5Document;
import fr.inra.moulon.xhtmlutils.HtmlDocument;
import fr.inra.moulon.xhtmlutils.Xhtml5Document;
import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.interfaces.XhtmlTextContainer;

public class XhtmlGenericTextContainer extends XhtmlElementBase implements
		XhtmlTextContainer {

	protected XhtmlGenericTextContainer(XhtmlDocument document,
			String elementName, Object content) {
		super(document, elementName);
		if (content != null) {
			this.setTextContent(content.toString());
		}
	}

	public XhtmlGenericTextContainer addHtmlContent(String htmlContent)
			throws DOMException {

		if (htmlContent != null) {
			String errorMessage = null;
			try {
				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader("<content>"
						+ htmlContent + "</content>"));

				Document doc = db.parse(is);
				NodeList nodeListToCopy = doc.getElementsByTagName("content")
						.item(0).getChildNodes();

				for (int i = 0; i < nodeListToCopy.getLength(); i++) {
					this.element.appendChild(this.document.getDocument()
							.adoptNode(nodeListToCopy.item(i).cloneNode(true)));
				}
			} catch (ParserConfigurationException parsE) {
				errorMessage = " ParserConfigurationException "
						+ parsE.getMessage();
			} catch (SAXException e) {
				errorMessage = " SAXException " + e.getMessage();
			} catch (IOException e) {
				errorMessage = " IOException " + e.getMessage();
			} finally {
				if (errorMessage != null) {
					htmlContent = "<div class='error'>" + errorMessage
							+ "</div>" + htmlContent;
					String[] fragList = htmlContent.split("<br\\/>|\\n");
					for (int i = 0; i < (fragList.length - 1); i++) {
						this.appendText(fragList[i]);
						this.newBr();
					}
					this.appendText(fragList[fragList.length - 1]);
				}
			}
		}
		return this;
	}

	@Override
	public void setTextContent(String textContent) throws DOMException {
		if (textContent != null) {
			element.setTextContent(textContent);
		}
	}

	@Override
	public void appendText(String content) {
		Text node = this.document.getDocument().createTextNode(content);
		appendChild(node);
	}

	@Override
	public XhtmlSpan newSpan() {

		XhtmlSpan xhtmlA = new XhtmlSpan(document, null);

		this.appendChild(xhtmlA);

		return xhtmlA;
	}

	@Override
	public XhtmlBr newBr() {

		XhtmlBr xhtmlBr = new XhtmlBr(document);

		this.appendChild(xhtmlBr);

		return xhtmlBr;

	}

	@Override
	public XhtmlSup newSup(String content) {

		XhtmlSup xhtmlA = new XhtmlSup(document, content);

		this.appendChild(xhtmlA);

		return xhtmlA;

	}

	@Override
	public XhtmlAnchor newAnchor(URI uri, String content) {

		XhtmlAnchor xhtmlA = new XhtmlAnchor(document, uri, content);

		this.appendChild(xhtmlA);

		return xhtmlA;

	}

	@Override
	public XhtmlImage newImage(URI src, String alt) {

		XhtmlImage xhtmlDiv = new XhtmlImage(this.document, src, alt);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public void includeSvgDocument(SvgDocument mon_svg) throws DOMException,
			Exception {
		// Document svgDoc = spectrum.getDocument();
		// Node svg = svgDoc.cloneNode(true);
		/*
		 * <svg:svg xmlns:svg="http://www.w3.org/2000/svg" width="10cm"
		 * height="8cm" viewBox="0 0 500 400" version="1.1">
		 */
		document.setContainsSvg(true);

		Node svgInXhtml = document.getRenderedDocument().importNode(
				mon_svg.getRenderedDocument().getDocumentElement(), true);

		if (this.document.getClass().equals(Html5Document.class)
				|| this.document.getClass().equals(Xhtml5Document.class)
				|| this.document.getClass().equals(HtmlDocument.class)) {
			this.includeInXhtml5SvgNode(svgInXhtml);
		} else {
			this.includeSvgNode(svgInXhtml);
		}
		// xhtmlDoc.getCurrentNode().appendChild(svgInXhtml);
		appendChild(svgInXhtml);

	}

	private void includeInXhtml5SvgNode(Node svgNode) {
		if (svgNode.getNodeName().equals("#cdata-section"))
			return;
		if (svgNode.getNodeName().equals("#text"))
			return;
		try {
			svgNode.setPrefix(null);
			NodeList nodeList = svgNode.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				includeInXhtml5SvgNode(nodeList.item(i));
			}
		} catch (org.w3c.dom.DOMException e) {
			System.out.println("ERROR on element named :"
					+ svgNode.getNodeName() + " error :" + e.getMessage());
		}
	}

	private void includeSvgNode(Node svgNode) {
		if (svgNode.getNodeName().equals("#cdata-section"))
			return;
		if (svgNode.getNodeName().equals("#text"))
			return;
		try {
			svgNode.setPrefix("svg");
			NodeList nodeList = svgNode.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				includeSvgNode(nodeList.item(i));
			}
		} catch (org.w3c.dom.DOMException e) {
			System.out.println("ERROR on element named :"
					+ svgNode.getNodeName() + " error :" + e.getMessage());
		}
	}

}
