package fr.inra.moulon.xhtmlutils.elements;


import fr.inra.moulon.xhtmlutils.XhtmlDocument;


//http://www.december.com/html/x1tran/element/div.html
public class XhtmlSpan extends XhtmlGenericTextContainer {

	public XhtmlSpan(XhtmlDocument document, String content) {
		super(document, "span", content);
	}

}
