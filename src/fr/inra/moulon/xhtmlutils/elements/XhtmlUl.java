package fr.inra.moulon.xhtmlutils.elements;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlUl extends XhtmlElementBase {

	public XhtmlUl(XhtmlDocument document) {
		super(document, "ul");
	}
	
	public XhtmlLi newLi(String content) {

		XhtmlLi xhtmlDiv = new XhtmlLi(this.document, content);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

}
