package fr.inra.moulon.xhtmlutils.elements;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlBr extends XhtmlGenericTextContainer {

	public XhtmlBr(XhtmlDocument document) {
		super(document, "br", null);
	}

}
