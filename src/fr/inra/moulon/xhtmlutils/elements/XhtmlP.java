package fr.inra.moulon.xhtmlutils.elements;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlP extends XhtmlGenericContainer {

	protected XhtmlP(XhtmlDocument document, String content) {
		super(document, "p", content);
	}

	protected XhtmlP(XhtmlDocument document) {
		super(document, "p");
	}

}
