package fr.inra.moulon.xhtmlutils.elements;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlOl extends XhtmlElementBase {

	public XhtmlOl(XhtmlDocument document) {
		super(document, "ol");
	}
	
	public XhtmlLi newLi(String content) {

		XhtmlLi xhtmlDiv = new XhtmlLi(this.document, content);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

}
