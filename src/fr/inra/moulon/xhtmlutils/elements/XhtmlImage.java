package fr.inra.moulon.xhtmlutils.elements;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlImage extends XhtmlElementBase {

	/**
	 * 
	 * @param document
	 * @param href the URI to the image
	 * @param alt alternative text description
	 */
	public XhtmlImage(XhtmlDocument document, URI href, String alt) {
		//<img src="/image/w3c.gif" alt="Le logo du W3C" />
		super(document, "img");
		setSrc(href);
		setAlt(alt);
	}
	
	public void setSrc(URI uri) {
		element.setAttributeNS(null, "src", uri.toString());
	}
	public void setAlt(String alt) {
		element.setAttributeNS(null, "alt", alt);
	}

}
