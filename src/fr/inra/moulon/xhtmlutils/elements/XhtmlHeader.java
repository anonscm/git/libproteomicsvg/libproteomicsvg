package fr.inra.moulon.xhtmlutils.elements;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlHeader extends XhtmlGenericTextContainer {

	public XhtmlHeader(XhtmlDocument document, int headerNum, String content) {
		super(document, "h" + headerNum, content);
	}
}
