package fr.inra.moulon.xhtmlutils.elements;


import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlLi extends XhtmlGenericContainer {

	public XhtmlLi(XhtmlDocument document, String content) {
		super(document, "li", content);
	}

}
