package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import org.w3c.dom.Element;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlFieldset extends XhtmlGenericFormContainer {

	private Element legend;

	protected XhtmlFieldset(XhtmlDocument document, String legendStr) {
		super(document, "fieldset", null);
		// <label for="">
		legend = document.getDocument().createElementNS(
				document.getNameSpace(), "legend");
		legend.setTextContent(legendStr);
		this.element.appendChild(legend);

	}

}
