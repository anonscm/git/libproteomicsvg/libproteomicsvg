package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputButton extends XhtmlGenericFormInput {

	protected XhtmlInputButton(XhtmlDocument document, String title) {
		super(document, "input");
		// //<input type="submit" />

		element.setAttributeNS(null, "type", "button");
		if (title != null) {
			element.setAttributeNS(null, "value", title);
		}
	}

}
