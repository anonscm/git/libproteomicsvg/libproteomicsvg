package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputCheckbox extends XhtmlGenericFormInput {

	protected XhtmlInputCheckbox(XhtmlDocument document, String name,
			String value) {
		super(document, "input");
		// //<input type="text" name="nom" value="" size="5" />
		element.setAttributeNS(null, "type", "checkbox");
		element.setAttributeNS(null, "name", name);
		element.setAttributeNS(null, "value", value);
	}

	public void setChecked(boolean ischecked) {
		if (ischecked) {
			element.setAttributeNS(null, "checked", "checked");
		} else {
			if (element.getAttributes().getNamedItemNS(null, "checked") != null) {
				element.getAttributes().removeNamedItemNS(null, "checked");
			}
		}
	}

}
