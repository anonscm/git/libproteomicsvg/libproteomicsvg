package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlElementBase;

public class XhtmlGenericFormInput extends XhtmlElementBase {

	protected XhtmlGenericFormInput(XhtmlDocument document, String elementName) {
		super(document, elementName);
	}

	public void assigneLabel(XhtmlLabel label) throws Exception {
		if (this.getId() == null) {
			throw new Exception("an id is required to use 'assigneLabel'");
		}
		label.setFor(this);
	}

	public String getName() {
		return this.getAttributeNS(null, "name");
	}

	public void setOnClick(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onclick", action);
		}
	}

	public void setOnChange(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onchange", action);
		}
	}

}
