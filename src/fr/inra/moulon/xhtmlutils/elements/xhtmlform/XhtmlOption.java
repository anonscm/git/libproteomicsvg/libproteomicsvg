package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.elements.XhtmlElementBase;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlOption extends XhtmlElementBase {

	private XhtmlSelect select;

	protected XhtmlOption(XhtmlSelect xhtmlSelect, String value, String content) {
		super(xhtmlSelect.getXhtmlDocument(), "option");
		element.setAttributeNS(null, "value", value);
		this.setTextContent(content);
		this.select = xhtmlSelect;
		this.select.add(this);
	}

	public void setSelected(boolean ischecked) {
		if (ischecked) {
			select.unselectAll();
			element.setAttributeNS(null, "selected", "selected");
		} else {
			if (element.getAttributes().getNamedItemNS(null, "selected") != null) {
				element.getAttributes().removeNamedItemNS(null, "selected");
			}
		}
	}

}
