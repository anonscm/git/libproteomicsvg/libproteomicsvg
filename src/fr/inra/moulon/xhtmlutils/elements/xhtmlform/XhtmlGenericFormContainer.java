package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericTextContainer;
import fr.inra.moulon.xhtmlutils.elements.XhtmlHeader;
import fr.inra.moulon.xhtmlutils.elements.XhtmlOl;
import fr.inra.moulon.xhtmlutils.elements.XhtmlUl;
import fr.inra.moulon.xhtmlutils.elements.XhtmlVideo;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;
import fr.inra.moulon.xhtmlutils.interfaces.XhtmlFormContainer;
import fr.inra.moulon.xhtmlutils.interfaces.XhtmlFormInput;

public class XhtmlGenericFormContainer extends XhtmlGenericTextContainer
		implements XhtmlFormContainer, XhtmlFormInput {

	protected XhtmlGenericFormContainer(XhtmlDocument document,
			String elementName, Object content) {
		super(document, elementName, content);
	}

	protected XhtmlGenericFormContainer(XhtmlDocument document,
			String elementName) {
		super(document, elementName, null);
	}

	@Override
	public XhtmlInputText newInputText(String name, String value, Integer size) {
		XhtmlInputText xhtmlInputText = new XhtmlInputText(document, name,
				value, size);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputTextarea newInputTextarea(String name, String value,
			Integer cols, Integer rows) {
		XhtmlInputTextarea xhtmlInputText = new XhtmlInputTextarea(document,
				name, value, cols, rows);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputRadio newInputRadio(String name, String value) {
		XhtmlInputRadio xhtmlInputText = new XhtmlInputRadio(document, name,
				value);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputCheckbox newInputCheckbox(String name, String value) {
		XhtmlInputCheckbox xhtmlInputText = new XhtmlInputCheckbox(document,
				name, value);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputHidden newInputHidden(String name, String value) {
		XhtmlInputHidden xhtmlInputText = new XhtmlInputHidden(document, name,
				value);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputFile newInputFile(String name) {
		XhtmlInputFile xhtmlDiv = new XhtmlInputFile(this.document, name);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;
	}

	@Override
	public XhtmlInputSubmit newInputSubmit(String value) {
		XhtmlInputSubmit xhtmlInputText = new XhtmlInputSubmit(document, value);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputButton newInputButton(String title) {
		XhtmlInputButton xhtmlInputText = new XhtmlInputButton(document, title);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputReset newInputReset() {
		XhtmlInputReset xhtmlInputText = new XhtmlInputReset(document);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlLabel newLabel(String content) {
		XhtmlLabel xhtmlInputText = new XhtmlLabel(document, content);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlFieldset newFieldset(String legend) {
		XhtmlFieldset xhtmlInputText = new XhtmlFieldset(document, legend);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlSelect newSelect(String name) {
		XhtmlSelect xhtmlInputText = new XhtmlSelect(document, name);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	@Override
	public XhtmlFormP newP(String content) {

		XhtmlFormP xhtmlDiv = new XhtmlFormP(document, content);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public XhtmlFormDiv newDiv(String content) {

		XhtmlFormDiv xhtmlDiv = new XhtmlFormDiv(document, content);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;

	}

	@Override
	public XhtmlHeader newHeader1(String content) {

		XhtmlHeader xhtmlHeader = new XhtmlHeader(this.document, 1, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;
	}

	@Override
	public XhtmlHeader newHeader2(String content) {

		XhtmlHeader xhtmlHeader = new XhtmlHeader(document, 2, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;
	}

	@Override
	public XhtmlHeader newHeader3(String content) {

		XhtmlHeader xhtmlHeader = new XhtmlHeader(document, 3, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;
	}

	@Override
	public XhtmlTable newTable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XhtmlUl newUl() {

		XhtmlUl xhtmlDiv = new XhtmlUl(this.document);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;
	}

	@Override
	public XhtmlOl newOl() {

		XhtmlOl xhtmlDiv = new XhtmlOl(this.document);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;
	}

	@Override
	public XhtmlVideo newVideo(URI src, String alt) {
		XhtmlVideo xhtmlDiv = new XhtmlVideo(this.document, src, alt);

		this.appendChild(xhtmlDiv);

		return xhtmlDiv;
	}

}
