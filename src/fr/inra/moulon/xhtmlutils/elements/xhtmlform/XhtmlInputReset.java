package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputReset extends XhtmlGenericFormInput {

	protected XhtmlInputReset(XhtmlDocument document) {
		super(document, "input");
		// //<input type="reset" />

		element.setAttributeNS(null, "type", "reset");
	}

}
