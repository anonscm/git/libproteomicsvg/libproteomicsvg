package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlScript;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlForm extends XhtmlGenericFormContainer {

	public XhtmlForm(XhtmlDocument document, String method, String action) {
		super(document, "form");
		element.setAttributeNS(null, "method", method);
		element.setAttributeNS(null, "action", action);
	}

	public XhtmlScript newScript(String content) {
		XhtmlScript xhtmlHeader = new XhtmlScript(document, content);

		this.appendChild(xhtmlHeader);

		return xhtmlHeader;
	}

}
