package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputSubmit extends XhtmlGenericFormInput {

	protected XhtmlInputSubmit(XhtmlDocument document, String value) {
		super(document, "input");
		// //<input type="submit" />

		element.setAttributeNS(null, "type", "submit");
		if (value != null) {
			element.setAttributeNS(null, "value", value);
		}
	}

}
