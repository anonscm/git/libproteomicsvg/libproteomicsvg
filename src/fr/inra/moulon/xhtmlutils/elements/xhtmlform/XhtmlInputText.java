package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputText extends XhtmlGenericFormInput {

	protected XhtmlInputText(XhtmlDocument document, String name, String value,
			Integer size) {
		super(document, "input");
		if (size == null) {
			size = 10;
		}
		// //<input type="text" name="nom" value="" size="5" />
		element.setAttributeNS(null, "type", "text");
		element.setAttributeNS(null, "name", name);
		element.setAttributeNS(null, "value", value);
		element.setAttributeNS(null, "size", "" + size);
	}

}
