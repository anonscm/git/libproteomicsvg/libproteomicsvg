package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputFile extends XhtmlGenericFormInput {

	protected XhtmlInputFile(XhtmlDocument document, String name) {
		super(document, "input");
		// //<input type="text" name="nom" value="" size="5" />
		element.setAttributeNS(null, "type", "file");
		element.setAttributeNS(null, "name", name);
	}

}
