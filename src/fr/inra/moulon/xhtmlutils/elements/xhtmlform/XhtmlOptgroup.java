package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.elements.XhtmlElementBase;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlOptgroup extends XhtmlElementBase {

	private XhtmlSelect select;

	protected XhtmlOptgroup(XhtmlSelect xhtmlSelect, String label) {
		super(xhtmlSelect.getXhtmlDocument(), "optgroup");
		element.setAttributeNS(null, "label", label);
		this.select = xhtmlSelect;
	}

	public XhtmlOption newOption(String value, String content) {
		XhtmlOption xhtmlInputText = new XhtmlOption(select, value, content);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

}
