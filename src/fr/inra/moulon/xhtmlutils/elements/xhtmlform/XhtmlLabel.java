package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericTextContainer;
import fr.inra.moulon.xhtmlutils.interfaces.XhtmlFormInput;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlLabel extends XhtmlGenericTextContainer implements
		XhtmlFormInput {

	private XhtmlGenericFormInput linkedInput = null;

	protected XhtmlLabel(XhtmlDocument document, String content) {
		super(document, "label", content);
		// <label for="">
	}

	protected void setFor(XhtmlGenericFormInput name) throws Exception {
		if (linkedInput == null) {
			element.setAttributeNS(null, "for", name.getId());
			this.linkedInput = name;
		} else {
			throw new Exception("this label is already assigned");
		}
	}

	@Override
	public XhtmlInputText newInputText(String name, String value, Integer size) {
		XhtmlInputText xhtmlInputText = new XhtmlInputText(document, name,
				value, size);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputTextarea newInputTextarea(String name, String value,
			Integer cols, Integer rows) {
		XhtmlInputTextarea xhtmlInputText = new XhtmlInputTextarea(document,
				name, value, cols, rows);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputRadio newInputRadio(String name, String value) {
		XhtmlInputRadio xhtmlInputText = new XhtmlInputRadio(document, name,
				value);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputCheckbox newInputCheckbox(String name, String value) {
		XhtmlInputCheckbox xhtmlInputText = new XhtmlInputCheckbox(document,
				name, value);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputHidden newInputHidden(String name, String value) {
		XhtmlInputHidden xhtmlInputText = new XhtmlInputHidden(document, name,
				value);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputSubmit newInputSubmit(String value) {
		XhtmlInputSubmit xhtmlInputText = new XhtmlInputSubmit(document, value);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputReset newInputReset() {
		XhtmlInputReset xhtmlInputText = new XhtmlInputReset(document);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputFile newInputFile(String name) {
		XhtmlInputFile xhtmlInputText = new XhtmlInputFile(document, name);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlSelect newSelect(String name) {
		XhtmlSelect xhtmlInputText = new XhtmlSelect(document, name);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

	@Override
	public XhtmlInputButton newInputButton(String title) {
		XhtmlInputButton xhtmlInputText = new XhtmlInputButton(document, title);
		this.appendChild(xhtmlInputText);
		this.linkedInput = xhtmlInputText;
		return xhtmlInputText;
	}

}
