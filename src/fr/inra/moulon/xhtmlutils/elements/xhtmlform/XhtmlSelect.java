package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import java.util.ArrayList;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlSelect extends XhtmlGenericFormInput {

	private ArrayList<XhtmlOption> optionList = new ArrayList<XhtmlOption>();

	protected XhtmlSelect(XhtmlDocument document, String name) {
		super(document, "select");
		element.setAttributeNS(null, "name", name);
	}

	public XhtmlOptgroup newOptgroup(String label) {
		XhtmlOptgroup xhtmlInputText = new XhtmlOptgroup(this, label);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	public XhtmlOption newOption(String value, String content) {
		XhtmlOption xhtmlInputText = new XhtmlOption(this, value, content);
		this.appendChild(xhtmlInputText);
		return xhtmlInputText;
	}

	public XhtmlDocument getXhtmlDocument() {
		return document;
	}

	public void add(XhtmlOption xhtmlOption) {
		this.optionList.add(xhtmlOption);
	}

	public void unselectAll() {
		for (XhtmlOption option : this.optionList) {
			option.setSelected(false);
		}

	}
}
