package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputTextarea extends XhtmlGenericFormInput {

	protected XhtmlInputTextarea(XhtmlDocument document, String name,
			String value, Integer cols, Integer rows) {
		super(document, "textarea");
		if (cols == null) {
			cols = 50;
		}
		if (rows == null) {
			rows = 5;
		}
		// //<input type="text" name="nom" value="" size="5" />
		// element.setAttributeNS(null, "type", "textarea");
		element.setAttributeNS(null, "name", name);
		element.setAttributeNS(null, "cols", "" + cols);
		element.setAttributeNS(null, "rows", "" + rows);
		this.setTextContent(value);
	}

}
