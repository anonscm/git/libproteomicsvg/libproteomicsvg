package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlFormDiv extends XhtmlGenericFormContainer {

	protected XhtmlFormDiv(XhtmlDocument document, String content) {
		super(document, "div", content);
	}

	protected XhtmlFormDiv(XhtmlDocument document) {
		super(document, "div");
	}

}
