package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlInputHidden extends XhtmlGenericFormInput {

	protected XhtmlInputHidden(XhtmlDocument document, String name, String value) {
		super(document, "input");
		// //<input type="submit" />

		element.setAttributeNS(null, "type", "hidden");
		element.setAttributeNS(null, "name", name);
		element.setAttributeNS(null, "value", value);
	}

}
