package fr.inra.moulon.xhtmlutils.elements.xhtmlform;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlFormP extends XhtmlGenericFormContainer {

	protected XhtmlFormP(XhtmlDocument document, String content) {
		super(document, "p", content);
	}

	protected XhtmlFormP(XhtmlDocument document) {
		super(document, "p");
	}

}
