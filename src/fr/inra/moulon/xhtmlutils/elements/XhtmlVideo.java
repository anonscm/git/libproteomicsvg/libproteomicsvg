package fr.inra.moulon.xhtmlutils.elements;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;

public class XhtmlVideo extends XhtmlElementBase {

	/**
	 * 
	 * @param document
	 * @param href
	 *            the URI to the image
	 * @param alt
	 *            alternative text description
	 */
	public XhtmlVideo(XhtmlDocument document, URI src, String alt) {
		// <img src="/image/w3c.gif" alt="Le logo du W3C" />
		super(document, "video");
		if (src != null) {
			element.setAttributeNS(null, "src", src.toString());
		}
		setAlt(alt);
	}

	public void setAlt(String alt) {
		if (alt != null) {
			element.setTextContent(alt);
		}
	}

	public void setWidth(float width) {
		element.setAttributeNS(null, "width", "" + width);
	}

	public void setHeight(float width) {
		element.setAttributeNS(null, "height", "" + width);
	}

	public float getWidth() {
		return new Float(element.getAttributeNS(null, "width"));
	}

	public float getHeight() {
		return new Float(element.getAttributeNS(null, "height"));
	}

}
