package fr.inra.moulon.xhtmlutils.elements;

import fr.inra.moulon.xhtmlutils.Html5Document;
import fr.inra.moulon.xhtmlutils.XhtmlDocument;

//http://www.december.com/html/x1tran/element/div.html
public class XhtmlSup extends XhtmlGenericTextContainer {

	public XhtmlSup(XhtmlDocument document, String content) {
		super(document, "sup", content);
		if (this.document.getClass().equals(Html5Document.class)) {
			if ((content == null) || (content.isEmpty())) {
				this.element.setTextContent(" ");
			}
		}
	}

}
