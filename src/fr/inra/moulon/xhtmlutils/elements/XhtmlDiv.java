package fr.inra.moulon.xhtmlutils.elements;


import fr.inra.moulon.xhtmlutils.XhtmlDocument;


//http://www.december.com/html/x1tran/element/div.html
public class XhtmlDiv extends XhtmlGenericContainer {

	protected XhtmlDiv(XhtmlDocument document, String content) {
		super(document, "div", content);
	}

	protected XhtmlDiv(XhtmlDocument document) {
		super(document, "div");
	}

}
