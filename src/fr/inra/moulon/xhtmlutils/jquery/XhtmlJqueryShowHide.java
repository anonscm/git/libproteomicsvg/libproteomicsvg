package fr.inra.moulon.xhtmlutils.jquery;

import fr.inra.moulon.xhtmlutils.elements.XhtmlDiv;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;
import fr.inra.moulon.xhtmlutils.elements.XhtmlScript;
import fr.inra.moulon.xhtmlutils.elements.XhtmlSpan;

/**
 * include jquery plugin "expand"
 * http://www.adipalaz.com/experiments/jquery/expand.html
 * 
 * @author langella
 * 
 */
public class XhtmlJqueryShowHide extends XhtmlGenericContainer {

	private XhtmlSpan title;
	private XhtmlDiv container;

	public XhtmlJqueryShowHide(XhtmlGenericContainer div, String titleStr,
			String urlJqueryLoad) {
		super(div.getXhtmlDocument(), "div", null);
		title = this.newSpan();
		title.setTextContent(titleStr);
		title.newAnchor(null, "show");

		// this.setCssClass("expand");

		container = this.newDiv(null);

		this.setUniqueId();

		div.appendChild(this);

		String id = this.getId();

		// container.setCssClass("collapse");

		// $("#outer  div.demo:eq(1) h4.expand:eq(0)").toggler({speed: "fast"});

		// this.newScript("$(\"#" + id
		// + " span\").toggler({method: \"toggle\", speed: 0});");
		XhtmlScript script = this.newScript("$('#" + id
				+ " div').toggle(false);" + "$('#" + id
				+ " span a').click(function() {");

		if (urlJqueryLoad != null) {
			script.appendText("$('#" + id + " div').load('" + urlJqueryLoad
					+ "');");
		}
		script.appendText("if ($('#" + id + " span a').html() == 'show') {"
				+ " $('#" + id + " span a').html('hide');" + "} else { $('#"
				+ id + " span a').html('show');};" + "$('#" + id
				+ " div').toggle();" + "});");

		/*
		 * this.newScript("" + "" + "" + "jQuery(document).ready(function() {"
		 * +"var content = $(\"#" + id + " div\");"
		 * 
		 * + "content.hide();"
		 * 
		 * + "button.click(function(e) {"+ "e.preventDefault();"+ "});" +
		 * "});");
		 */
		/*
		
		 * 
		 */

	}

	public XhtmlDiv getContainer() {
		return container;
	}
}
