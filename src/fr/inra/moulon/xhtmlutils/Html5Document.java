/*******************************************************************************
 * Copyright (c) 2010 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.xhtmlutils;

import java.io.IOException;
import java.io.OutputStream;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory; //This is a small proof-of-concept graphics class that
// provides method calls for the creation of the following
// DOM tree nodes:
//  A simple element with no attributes.
//  A linear gradient element.
//  An ellipse
//  A circle
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;

public class Html5Document extends XhtmlDocument {

	public Html5Document() {
		super();
		this.setWriteDocType(true);
	}

	// ----------------------------------------------------//

	// This is a utility method that is used to execute code
	// that is the same regardless of the graphic image
	// being produced.
	public Document createDocument() {
		Document document = null;
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			dbfac.setValidating(false);

			// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
			// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
			// DocumentType svgDocType = new DocumentType();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();

			document = docBuilder.newDocument();

			// document.createAttributeNS(xlinkNS, "xlink");

			Element root = document
					.createElementNS(this.getNameSpace(), "html");
			document.appendChild(root);

		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}// end catch
		return document;
	}// end getDocument

	@Override
	protected Transformer modifyTransformer(Transformer transformer) {
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
		if (this.getWriteDocType()) {
			/*
			 * <!DOCTYPE html > <html lang="en">
			 */
			//transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
			//		"about:legacy-compat");
			// transformer .setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
			// "XSLT-compat");

		}
		return transformer;
	}

	@Override
	protected void docToOutputStream(OutputStream out) {
		if (this.getWriteDocType()) {
			String docType = "<!DOCTYPE html >\n";
			try {
				out.write(docType.getBytes());
			} catch (IOException e) {
				// ignoring problem writing doctype
			}
			// buggy doctype in html for java version < 1.6.0_22
			this.setWriteDocType(false);
		}
		docToOutputStream(this, out);
	}

	@Override
	public void setWriteDocType(boolean writeIt) {
		super.setWriteDocType(true);
	}

	@Override
	protected void render() {

	}

	public String getNameSpace() {
		return null;
	}

}// end class SvgGraphics

