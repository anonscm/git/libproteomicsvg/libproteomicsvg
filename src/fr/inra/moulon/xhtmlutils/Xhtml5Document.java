/*******************************************************************************
 * Copyright (c) 2010 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.xhtmlutils;


import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
//This is a small proof-of-concept graphics class that
// provides method calls for the creation of the following
// DOM tree nodes:
//  A simple element with no attributes.
//  A linear gradient element.
//  An ellipse
//  A circle
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;

public class Xhtml5Document extends XhtmlDocument {

	public Xhtml5Document() {
		super();
		this.setWriteDocType(false);
	}

	// ----------------------------------------------------//

	// This is a utility method that is used to execute code
	// that is the same regardless of the graphic image
	// being produced.
	public Document createDocument() {
		Document document = null;
		try {
			/*
			 * DocumentBuilderFactory factory = DocumentBuilderFactory
			 * .newInstance();
			 * 
			 * DocumentBuilder builder = factory.newDocumentBuilder(); document
			 * = builder.newDocument(); document.setXmlStandalone(false);
			 */

			/*
			 * <?xml version="1.0" encoding="UTF-8" standalone="no"?> <svg
			 * xmlns:xlink="http://www.w3.org/1999/xlink"
			 * xmlns="http://www.w3.org/2000/svg"
			 * contentScriptType="text/ecmascript" width="800.0"
			 * zoomAndPan="magnify" contentStyleType="text/css" height="600.0"
			 * preserveAspectRatio="xMidYMid meet" version="1.0">
			 */
			// We need a Document
			// String svgNS = "http://www.w3.org/2000/svg";
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			dbfac.setValidating(false);

			// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
			// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
			// DocumentType svgDocType = new DocumentType();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();

			document = docBuilder.newDocument();

			// document.createAttributeNS(xlinkNS, "xlink");

			Element root = document
					.createElementNS(this.getNameSpace(), "html");
			document.appendChild(root);

			// root.createAttributeNS(xlinkNS, "xlink");
			// root.setAttributeNS(xmlNS, "xmlns", svgNS);
			// root.setPrefix("xlink");
			// root.setAttributeNS(xmlNS, "xmlns:xlink", xlinkNS);
			// root.setAttributeNS(null, "contentScriptType",
			// "text/ecmascript");
			// root.setAttributeNS(null, "zoomAndPan", "magnify");
			// root.setAttributeNS(null, "contentStyleType", "text/css");
			// root.setAttributeNS(null, "preserveAspectRatio",
			// "xMidYMid meet");
			// root.setAttributeNS(xmlNS, "xmlns:xhtml", xhtmlNS);
			// root.setAttributeNS("", "version", "1.1");

			// <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

			// DOMImplementation impl =
			// SVGDOMImplementation.getDOMImplementation();
			// document = impl.createDocument(svgNS, "svg", null);

		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}// end catch
		return document;
	}// end getDocument

	@Override
	protected Transformer modifyTransformer(Transformer transformer) {
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.VERSION, "1.0");

		return transformer;
	}

	@Override
	public void setWriteDocType(boolean writeIt) {
		super.setWriteDocType(true);
	}

	@Override
	protected void render() {

	}

	public String getNameSpace() {
		return xhtmlNS;
	}

}// end class SvgGraphics

