package fr.inra.moulon.xhtmlutils.interfaces;

import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputButton;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputCheckbox;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputFile;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputHidden;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputRadio;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputReset;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputSubmit;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputText;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlInputTextarea;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlSelect;

public interface XhtmlFormInput {

	public XhtmlInputText newInputText(String name, String value, Integer size);

	public XhtmlInputTextarea newInputTextarea(String name, String value,
			Integer cols, Integer rows);

	public XhtmlInputRadio newInputRadio(String name, String value);

	public XhtmlInputCheckbox newInputCheckbox(String name, String value);

	public XhtmlInputHidden newInputHidden(String name, String value);

	public XhtmlInputFile newInputFile(String name);

	public XhtmlInputSubmit newInputSubmit(String value);

	public XhtmlInputButton newInputButton(String title);

	public XhtmlInputReset newInputReset();

	public XhtmlSelect newSelect(String name);

}
