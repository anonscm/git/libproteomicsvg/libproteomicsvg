package fr.inra.moulon.xhtmlutils.interfaces;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.elements.XhtmlDiv;
import fr.inra.moulon.xhtmlutils.elements.XhtmlHeader;
import fr.inra.moulon.xhtmlutils.elements.XhtmlOl;
import fr.inra.moulon.xhtmlutils.elements.XhtmlP;
import fr.inra.moulon.xhtmlutils.elements.XhtmlScript;
import fr.inra.moulon.xhtmlutils.elements.XhtmlUl;
import fr.inra.moulon.xhtmlutils.elements.XhtmlVideo;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;

public interface XhtmlContainer extends XhtmlTextContainer {

	public XhtmlP newP(String content);

	public XhtmlDiv newDiv(String content);

	public XhtmlHeader newHeader1(String content);

	public XhtmlHeader newHeader2(String content);

	public XhtmlHeader newHeader3(String content);

	public XhtmlScript newScript(String content);

	public XhtmlTable newTable();

	public XhtmlUl newUl();

	public XhtmlOl newOl();

	public XhtmlVideo newVideo(URI src, String alt);

}
