package fr.inra.moulon.xhtmlutils.interfaces;

import java.net.URI;

import org.w3c.dom.DOMException;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlAnchor;
import fr.inra.moulon.xhtmlutils.elements.XhtmlBr;
import fr.inra.moulon.xhtmlutils.elements.XhtmlImage;
import fr.inra.moulon.xhtmlutils.elements.XhtmlSpan;
import fr.inra.moulon.xhtmlutils.elements.XhtmlSup;

public interface XhtmlTextContainer {

	public void setTextContent(String textContent) throws DOMException;

	public void appendText(String content) ;

	public XhtmlSpan newSpan();

	public XhtmlBr newBr() ;

	public XhtmlSup newSup(String content);

	public XhtmlAnchor newAnchor(URI uri, String content);

	public XhtmlImage newImage(URI src, String alt) ;

	public void includeSvgDocument(SvgDocument mon_svg) throws DOMException, Exception;
}
