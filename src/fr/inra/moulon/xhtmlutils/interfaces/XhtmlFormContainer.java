package fr.inra.moulon.xhtmlutils.interfaces;

import java.net.URI;

import fr.inra.moulon.xhtmlutils.elements.XhtmlHeader;
import fr.inra.moulon.xhtmlutils.elements.XhtmlOl;
import fr.inra.moulon.xhtmlutils.elements.XhtmlUl;
import fr.inra.moulon.xhtmlutils.elements.XhtmlVideo;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlFieldset;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlFormDiv;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlFormP;
import fr.inra.moulon.xhtmlutils.elements.xhtmlform.XhtmlLabel;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;

public interface XhtmlFormContainer extends XhtmlTextContainer {

	public XhtmlLabel newLabel(String content);

	public XhtmlFieldset newFieldset(String legend);

	public XhtmlFormP newP(String content);

	public XhtmlFormDiv newDiv(String content);

	public XhtmlHeader newHeader1(String content);

	public XhtmlHeader newHeader2(String content);

	public XhtmlHeader newHeader3(String content);

	public XhtmlTable newTable();

	public XhtmlUl newUl();

	public XhtmlOl newOl();

	public XhtmlVideo newVideo(URI src, String alt);
}
