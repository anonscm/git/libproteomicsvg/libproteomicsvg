package fr.inra.moulon.xhtmlutils.XhtmlIonTable;

/**
 *   SvgSpectrum free java library to draw annotated MS spectrum
 Copyright (C) 2009  Olivier Langella

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 * 
 * @author Olivier Langella <olivier.langella@moulon.inra.fr
 * 
 */

import java.util.ArrayList;
import java.util.HashSet;

import fr.inra.moulon.svgutils.SvgDocument;
import fr.inra.moulon.svgutils.svgspectrum.SvgAaMod;
import fr.inra.moulon.svgutils.svgspectrum.SvgIonSpecies;
import fr.inra.moulon.svgutils.svgspectrum.SvgSpectrumDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlAnchor;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTableCell;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTableRow;

public class XhtmlIonModificationsTable extends XhtmlTable {

	// private XhtmlDocument xhtmlDoc;
	private HashSet<SvgAaMod> svgAaModSet;
	private SvgSpectrumDocument spectrum;

	public XhtmlIonModificationsTable(XhtmlGenericContainer div,
			SvgSpectrumDocument spectrum) throws Exception {
		super(div.getXhtmlDocument());

		this.spectrum = spectrum;
		this.setSvgSpectrum(spectrum);

		writeIonTable();

		div.appendChild(this);

	}

	protected void setSvgSpectrum(SvgSpectrumDocument spectrum) {

		this.svgAaModSet = spectrum.getSvgAaModSet();

		this.includeSvgCss(spectrum);
	}

	public void includeSvgCss(SvgDocument svgDoc) {
		/*
		 * HashMap<String, HashMap<String, String>> svgCss = svgDoc
		 * .getCssHashMap(); for (String cssLocation : svgCss.keySet()) {
		 * HashMap<String, String> cssProperty = svgCss.get(cssLocation); for
		 * (String cssPropertykey : cssProperty.keySet()) { // String
		 * visibilityHidden = //
		 * "visibility:hidden;opacity:0;fill-opacity:0;stroke-opacity:0;"; /* if
		 * (cssPropertykey.equals("opacity") &&
		 * cssProperty.get(cssPropertykey).equals("0")) break; if
		 * (cssPropertykey.equals("fill-opacity") &&
		 * cssProperty.get(cssPropertykey).equals("0")) break; if
		 * (cssPropertykey.equals("stroke-opacity") &&
		 * cssProperty.get(cssPropertykey).equals("0")) break;
		 * 
		 * if (cssPropertykey.equals("visibility")) break;
		 */
		/*
		 * this.getXhtmlDocument().setCssClass(cssLocation, cssPropertykey,
		 * cssProperty.get(cssPropertykey));
		 * 
		 * } }
		 */

		HashSet<SvgIonSpecies> svgIonSpeciesList = this.spectrum
				.getAssignedSvgIonSpecies();

		// this.getXhtmlDocument().setCssClass("table *", "color", "red");
		for (SvgIonSpecies svgIonSpecies : svgIonSpeciesList) {
			this.getXhtmlDocument().setCssClass(
					"td." + svgIonSpecies.getCssClassname(), "color",
					svgIonSpecies.getCssColor());
		}

	}

	protected void writeIonTable() throws Exception {

		this.setCaption("modifications table");
		/*
		 * $ion_table->add_headers();
		 */
		ArrayList<String> title = new ArrayList<String>(0);
		title.add("#");
		title.add("accession");
		title.add("name");
		title.add("diff mono");

		this.addHeaderLine(title);

		int i = 0;
		for (SvgAaMod aaMod : this.svgAaModSet) {
			i++;
			XhtmlTableRow tr = this.newTableRow();
			tr.newCell("[" + i + "]");
			XhtmlTableCell hrefCell = tr.newCell(null);
			if (aaMod.getURI() != null) {
				XhtmlAnchor anchor = new XhtmlAnchor(this.getXhtmlDocument(),
						aaMod.getURI(), aaMod.getPsiModAccession());
				anchor.setNewWindows();
				hrefCell.appendChild(anchor);
			} else {
				hrefCell.setTextContent(aaMod.getPsiModAccession());
			}
			tr.newCell(aaMod.getName());
			tr.newCell("" + aaMod.getMass());
		}

	}

}
