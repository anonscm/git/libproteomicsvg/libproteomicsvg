package fr.inra.moulon.xhtmlutils.XhtmlIonTable;

/**
 *   SvgSpectrum free java library to draw annotated MS spectrum
 Copyright (C) 2009  Olivier Langella

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 * 
 * @author Olivier Langella <olivier.langella@moulon.inra.fr
 * 
 */

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import fr.inra.moulon.svgutils.svgspectrum.SvgAaMod;
import fr.inra.moulon.svgutils.svgspectrum.SvgAminoAcid;
import fr.inra.moulon.svgutils.svgspectrum.SvgIon;
import fr.inra.moulon.svgutils.svgspectrum.SvgIonSpecies;
import fr.inra.moulon.svgutils.svgspectrum.SvgPeptideSequence;
import fr.inra.moulon.svgutils.svgspectrum.SvgSpectrumDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTable;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTableCell;
import fr.inra.moulon.xhtmlutils.elements.xhtmltable.XhtmlTableRow;

public class XhtmlIonTable extends XhtmlTable {

	// private XhtmlDocument xhtmlDoc;
	private SvgSpectrumDocument spectrum;
	// XhtmlTable xhtmlTable;
	private ArrayList<SvgIon> ionTable;
	private HashSet<SvgAaMod> svgAaModSet;

	public XhtmlIonTable(XhtmlGenericContainer div, SvgSpectrumDocument spectrum)
			throws Exception {
		super(div.getXhtmlDocument());

		this.setSvgSpectrum(spectrum);

		writeIonTable();

		div.appendChild(this);

	}

	protected int getRefNum(SvgAaMod svgAaMod) {
		int i = 0;
		for (SvgAaMod orderMod : svgAaModSet) {
			i++;
			if (orderMod.equals(svgAaMod)) {
				return i;
			}
		}
		return -1;

	}

	protected void setSvgSpectrum(SvgSpectrumDocument spectrum)
			throws Exception {

		if (spectrum == null) {
			throw new Exception("spectrum is null");
		}

		if (spectrum.getFragmentationType() == null) {
			throw new Exception("spectrum fragmentation type is null");
		}

		this.spectrum = spectrum;

		this.svgAaModSet = spectrum.getSvgAaModSet();

	}

	protected void writeIonTable() throws Exception {

		this.setCaption("ion table");
		/*
		 * $ion_table->add_headers();
		 */
		SvgPeptideSequence svgSequence = spectrum.getSvgPeptideSequence();
		if (svgSequence == null) {
			throw new Exception("spectrum SVG sequence is null");
		}

		XhtmlTableRow th = this.newHeaderLine();

		th.newCell("#");
		TreeSet<SvgIonSpecies> nterIonSpecies = svgSequence
				.getNterSvgIonSpeciesList(spectrum.getFragmentationType());
		XhtmlTableCell cell;
		for (SvgIonSpecies ionSpecies : nterIonSpecies) {
			cell = th.newCell(null);
			ionSpecies.printTo(cell);
			// set CSS class
			this.document.setCssClass("." + ionSpecies.getCssClassname(),
					"color", ionSpecies.getCssColor());
		}
		cell = th.newCell("Seq.");
		cell.newSup("[mods]");
		TreeSet<SvgIonSpecies> cterIonSpecies = svgSequence
				.getCterSvgIonSpeciesList(spectrum.getFragmentationType());
		for (SvgIonSpecies ionSpecies : cterIonSpecies) {
				cell = th.newCell(null);
			//	System.out.println("coucou " + ionSpecies.getCssClassname());
			ionSpecies.printTo(cell);
			// set CSS class
			this.document.setCssClass("." + ionSpecies.getCssClassname(),
					"color", ionSpecies.getCssColor());
		}
		th.newCell("#");

		// this.addHeaderLine(title);
		/*
		 * $ion_table->add_header('#'); $ion_table->add_header('a');
		 * $ion_table->add_header('b'); if ($precursor_z > 1)
		 * $ion_table->add_header('b++'); $ion_table->add_header('b*'); if
		 * ($precursor_z > 1) $ion_table->add_header('b*++');
		 * $ion_table->add_header('bO'); if ($precursor_z > 1)
		 * $ion_table->add_header('bO++'); $ion_table->add_header('Seq.[mods]');
		 * $ion_table->add_header('y'); if ($precursor_z > 1)
		 * $ion_table->add_header('y++'); $ion_table->add_header('y*'); if
		 * ($precursor_z > 1) $ion_table->add_header('y*++');
		 * $ion_table->add_header('yO'); if ($precursor_z > 1)
		 * $ion_table->add_header('yO++'); $ion_table->add_header('#');
		 */

		ionTable = svgSequence.getIonTable(spectrum.getFragmentationType());
		DecimalFormat formDecimal = (DecimalFormat) NumberFormat
				.getIntegerInstance();
		formDecimal.applyPattern("####0.00");

		int posOnSequence = 0;
		int sequenceLength = svgSequence.size();
		for (SvgAminoAcid aa : svgSequence) {
			posOnSequence++;
			XhtmlTableRow tr = this.newTableRow();
			tr.newCell("" + posOnSequence);
			Integer numPeak = null;
			for (SvgIonSpecies ionSpecies : nterIonSpecies) {

				SvgIon ion = getSvgIon(posOnSequence, ionSpecies);
				if (ion == null) {
					tr.newCell("");
				} else {
					cell = tr.newCell(formDecimal.format(ion.getMz()));
					numPeak = spectrum.getAssignedPeak(ion);
					if (numPeak != null) {
						try {
							cell.setCssClass(ionSpecies.getCssClassname());
						} catch (Exception e) {
							throw new Exception("ERROR in writeIonTable : "
									+ e.getMessage());
						}
					}
				}
			}
			String mod = "";
			if (aa.getSvgAaModList().size() > 0) {
				ArrayList<Integer> refList = new ArrayList<Integer>(0);
				for (SvgAaMod svgAaMod : aa.getSvgAaModList()) {
					refList.add(getRefNum(svgAaMod));
				}
				Collections.sort(refList);
				Iterator<Integer> iter = refList.iterator();
				mod = "" + iter.next();
				while (iter.hasNext()) {
					mod += "," + iter.next();
				}
				mod = " [" + mod + "]";
			}
			cell = tr.newCell(aa.toString());
			cell.newSup(mod);

			int number = sequenceLength - posOnSequence + 1;
			for (SvgIonSpecies ionSpecies : cterIonSpecies) {

				SvgIon ion = getSvgIon(number, ionSpecies);
				if (ion == null) {
					tr.newCell("");
				} else {
					cell = tr.newCell(formDecimal.format(ion.getMz()));
					numPeak = spectrum.getAssignedPeak(ion);
					if (numPeak != null) {
						cell.setCssClass(ionSpecies.getCssClassname());
					}
				}
			}
			tr.newCell("" + number);
		}

	}

	private SvgIon getSvgIon(int posOnSequence, SvgIonSpecies ionSpecies) {

		for (SvgIon ion : ionTable) {
			if (ion.getSvgIonSpecies() == ionSpecies) {
				if (ion.getNumber() == posOnSequence) {
					return ion;
				}
			}
		}
		return null;

	}

}
