/*******************************************************************************
 * Copyright (c) 2010 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.xhtmlutils;

import java.io.OutputStream;

import java.util.HashMap;

import org.w3c.dom.*;

import fr.inra.moulon.utils.XmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

//This is a small proof-of-concept graphics class that
// provides method calls for the creation of the following
// DOM tree nodes:
//  A simple element with no attributes.
//  A linear gradient element.
//  An ellipse
//  A circle

//Each method receives a reference to the overall document
// along with a reference to the parent for the new node.
//When the method returns, the new node has been appended
// to the parent node.
class XhtmlBody extends XhtmlGenericContainer {

	protected XhtmlBody(XhtmlDocument document) {
		super(document, "body", null);
	}

	protected Element getElement() {
		return this.element;
	}

	public void setOnLoad(String action) {
		if (action != null) {
			element.setAttributeNS(null, "onload", action);
		}
	}
}

public class XhtmlDocument extends XmlDocument {

	public String xhtmlNS = "http://www.w3.org/1999/xhtml";
	public static String xlinkNS = "http://www.w3.org/1999/xlink";
	public static String xmlNS = "http://www.w3.org/2000/xmlns/";

	private Element xhtmlroot;
	private Element style;
	private CDATASection cdataStyle;

	HashMap<String, HashMap<String, String>> cssClasses = new HashMap<String, HashMap<String, String>>(
			0);
	private XhtmlBody body;
	private boolean writeDocType = true;
	private boolean containsSvg = false;
	protected Element head;
	protected Element title;

	public XhtmlDocument() {

		// String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
		// DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
		// svgdoc = (Document) impl.createDocument(svgNS, "svg", null);

		xmldoc = createDocument();
		xhtmlroot = xmldoc.getDocumentElement();
		head = xmldoc.createElementNS(this.getNameSpace(), "head");
		xhtmlroot.appendChild(head);

		// <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		Element meta = xmldoc.createElementNS(this.getNameSpace(), "meta");
		meta.setAttributeNS(this.getNameSpace(), "http-equiv", "Content-Type");
		meta.setAttributeNS(this.getNameSpace(), "content",
				"text/html;charset=utf-8");
		head.appendChild(meta);

		title = xmldoc.createElementNS(this.getNameSpace(), "title");
		head.appendChild(title);
		this.setTitle(" ");
		/*
		 * <head> <meta http-equiv="Content-Type"
		 * content="text/html; charset=utf-8"/> <title>Bioinformatique </title>
		 * <link rel="stylesheet" href="../css/3ps.css" type="text/CSS"/> <link
		 * rel="stylesheet" href="../css/pappso_position.css" type="text/CSS"/>
		 * <link rel="shortcut icon" href="../images/logo/pappso_icon.png"
		 * type="images/x-icon"/> </head>
		 */
		// Element body = xhtmldoc.createElementNS(xhtmlNS, "body");
		// xhtmlroot.appendChild(body);
		body = new XhtmlBody(this);
		xhtmlroot.appendChild(body.getElement());

		// <style type="text/css"> ... </style>

		style = xhtmlStyle(xmldoc, head);
		// defs = svgDefs(svgdoc, svgroot);
		cdataStyle = this.xmldoc.createCDATASection(" ");
		style.appendChild(cdataStyle);

		/*
		 * this.setCssClass( "svg",
		 * "fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
		 * );
		 */

	}

	public void setTitle(String title) {
		if (title != null) {
			this.title.setTextContent(title);
		}
	}

	// ----------------------------------------------------//

	// This is a utility method that is used to execute code
	// that is the same regardless of the graphic image
	// being produced.
	public Document createDocument() {
		Document document = null;
		try {
			/*
			 * DocumentBuilderFactory factory = DocumentBuilderFactory
			 * .newInstance();
			 * 
			 * DocumentBuilder builder = factory.newDocumentBuilder(); document
			 * = builder.newDocument(); document.setXmlStandalone(false);
			 */

			/*
			 * <?xml version="1.0" encoding="UTF-8" standalone="no"?> <svg
			 * xmlns:xlink="http://www.w3.org/1999/xlink"
			 * xmlns="http://www.w3.org/2000/svg"
			 * contentScriptType="text/ecmascript" width="800.0"
			 * zoomAndPan="magnify" contentStyleType="text/css" height="600.0"
			 * preserveAspectRatio="xMidYMid meet" version="1.0">
			 */
			// We need a Document
			// String svgNS = "http://www.w3.org/2000/svg";
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			dbfac.setValidating(false);

			// <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
			// "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
			// DocumentType svgDocType = new DocumentType();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();

			document = docBuilder.newDocument();

			document.createAttributeNS(xlinkNS, "xlink");

			Element root = document
					.createElementNS(this.getNameSpace(), "html");
			document.appendChild(root);

			// root.createAttributeNS(xlinkNS, "xlink");
			// root.setAttributeNS(xmlNS, "xmlns", svgNS);
			// root.setPrefix("xlink");
			root.setAttributeNS(xmlNS, "xmlns:xlink", xlinkNS);
			// root.setAttributeNS(null, "contentScriptType",
			// "text/ecmascript");
			// root.setAttributeNS(null, "zoomAndPan", "magnify");
			// root.setAttributeNS(null, "contentStyleType", "text/css");
			// root.setAttributeNS(null, "preserveAspectRatio",
			// "xMidYMid meet");
			// root.setAttributeNS(xmlNS, "xmlns:xhtml", xhtmlNS);
			// root.setAttributeNS("", "version", "1.1");

			// <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

			// DOMImplementation impl =
			// SVGDOMImplementation.getDOMImplementation();
			// document = impl.createDocument(svgNS, "svg", null);

		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}// end catch
		return document;
	}// end getDocument

	// ----------------------------------------------------//

	protected static void docToOutputStream(XhtmlDocument xhtmlDocument,
			OutputStream out) {
		try {
			// Get a TransformerFactory object.
			TransformerFactory xformFactory = TransformerFactory.newInstance();

			// Get an XSL Transformer object.
			Transformer transformer = xformFactory.newTransformer();

			transformer = xhtmlDocument.modifyTransformer(transformer);
			// Get a DOMSource object that represents the
			// Document object

			DOMSource source = new DOMSource(xhtmlDocument.getDocument()
					.getDocumentElement());

			// Get a StreamResult object that points to the
			// screen. Then transform the DOM sending XML to
			// the screen.
			StreamResult scrResult = new StreamResult(out);
			transformer.transform(source, scrResult);

		}// end try block

		catch (Exception e) {
			e.printStackTrace(System.err);
		}// end catch
	}// end transformTheDom

	protected Transformer modifyTransformer(Transformer transformer) {
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.VERSION, "1.0");

		if (this.getWriteDocType()) {
			if (this.getContainsSvg()) {
				/*
				 * <!DOCTYPE html PUBLIC
				 * "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN"
				 * "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd"
				 * >
				 */
				transformer
						.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
								"-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN");
				transformer
						.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
								"http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd");
			} else {
				/*
				 * <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
				 * "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
				 */
				transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
						"-//W3C//DTD XHTML 1.1//EN");
				transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
						"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd");

				/*
				 * transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM ,
				 * "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" );
				 * transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC ,
				 * "-//W3C//DTD XHTML 1.0 Transitional//EN");
				 */
			}
		}
		return transformer;
	}

	/**
	 * @brief tells if we want to write the DOCTYPE in the resulting xml
	 *        document
	 * 
	 * @return
	 */
	protected boolean getWriteDocType() {
		return writeDocType;
	}

	public void setWriteDocType(boolean writeIt) {
		writeDocType = writeIt;
	}

	public void setCssClass(String cssKey, String cssProperty, String cssValue) {
		HashMap<String, String> the_css;
		if (cssClasses.containsKey(cssKey)) {
			the_css = cssClasses.get(cssKey);
		} else {
			the_css = new HashMap<String, String>(0);
			cssClasses.put(cssKey, the_css);
		}
		the_css.put(cssProperty, cssValue);

	}

	public void setCssClass(String classname, String classdefinition) {

		String[] props = classdefinition.split(";");
		HashMap<String, String> the_css = new HashMap<String, String>(0);
		cssClasses.put(classname, the_css);

		for (String css : props) {
			String[] property = css.split(":", 2);
			the_css.put(property[0], property[1]);
		}

		// this.cssClasses.put(classname, classdefinition);
	}

	protected void writeCss() {
		// Add a CDATA section to the root element
		StringBuilder csssyntax = new StringBuilder("\n");

		for (String classKey : this.cssClasses.keySet()) {

			csssyntax.append("\n" + classKey + " {");

			HashMap<String, String> cssClass = cssClasses.get(classKey);
			for (String propertyKey : cssClass.keySet()) {
				csssyntax.append(propertyKey + ":" + cssClass.get(propertyKey)
						+ ";");
			}

			csssyntax.append("}");

			// la clé de l'élement courant est e . getKey ( ) ;

			// la valeur de l'élement courant est e . getValue ( )

		}
		csssyntax.append("\n");
		// System.out.print("cestca: "+csssyntax);

		// cdataStyle.deleteData(0, cdataStyle.getLength());
		cdataStyle.setData(csssyntax.toString());

	}

	private Element xhtmlStyle(Document document, Element parent) {

		// create the rectangle
		Element rect = document.createElementNS(getNameSpace(), "style");
		parent.appendChild(rect);
		rect.setAttributeNS(null, "type", "text/css");
		return rect;
	}

	public XhtmlGenericContainer getBody() {
		return this.body;
	}

	public void setContainsSvg(boolean b) {
		this.containsSvg = b;
	}

	protected boolean getContainsSvg() {
		return this.containsSvg;
	}

	@Override
	protected void render() {

	}

	@Override
	protected void docToOutputStream(OutputStream out) {
		docToOutputStream(this, out);
		/*
		 * <style type="text/css">
		 * 
		 * .yO2 {color:orange;} table * {color:red;} .y1mPmP {color:red;}
		 * </style>
		 */
	}

	public String getNameSpace() {
		if (xhtmlNS.isEmpty())
			return null;
		return xhtmlNS;
	}

	public void setOnLoad(String action) {
		this.body.setOnLoad(action);
	}

}// end class SvgGraphics

