/*******************************************************************************
 * Copyright (c) 2011-06-28 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.xhtmlutils.xhtmlprotcover;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import fr.inra.moulon.xhtmlutils.elements.XhtmlDiv;
import fr.inra.moulon.xhtmlutils.elements.XhtmlGenericContainer;
import fr.inra.moulon.xhtmlutils.elements.XhtmlSpan;

public class XhtmlSequence extends XhtmlDiv {

	String sequence;
	List<Boolean> coverage = new ArrayList<Boolean>();

	public XhtmlSequence(XhtmlGenericContainer div, String proteinSequence) throws Exception {
		super(div.getXhtmlDocument(), proteinSequence);
		sequence = proteinSequence;

		for (int i = 0; i < sequence.length(); i++) {
			coverage.add(new Boolean(false));
		}
		this.setCssClass("protcover");
		div.getXhtmlDocument().setCssClass(".protcover", "word-wrap", "break-word");
		div.getXhtmlDocument().setCssClass(".protcover", "font-family", "monospace");
		//:"Times New Roman",Georgia,Serif;
		div.getXhtmlDocument().setCssClass(".protcover span", "color", "red");

		div.appendChild(this);
	}

	public XhtmlSpan newPeptideCover(int start, int stop, URI href) throws Exception {
		if ((start < 1) || (stop < start) ){
			throw new Exception("peptide cover out of bound");
		}
		if (stop > this.sequence.length()){
			throw new Exception("peptide cover out of bound");
		}
		
		this.removeAll();
		this.removeAll();

		XhtmlSpan spanToReturn = null;
		XhtmlSpan span = null;
		String chunck = new String();
		for (int i = 0; i < sequence.length(); i++) {
			Boolean here = coverage.get(i);
			if (((i + 1) >= start) && (i < stop)) {
				here = true;
				coverage.set(i, here);
			}
			if (here) {
				if (span == null) {
					this.appendText(chunck);
					span = this.newSpan();
					if (((i + 1) >= start) && (i <= stop)) {
						spanToReturn = span;
					}
					System.out.println("ancien pas couvert :" +chunck);
					chunck = "";
				}
			} else {
				if (span != null) {
					span.appendText(chunck);
					System.out.println("ancien couvert :" +chunck);
					span = null;
					chunck = "";
				}
			}
			chunck += sequence.subSequence(i, i + 1);
		}
		if (span != null) {
			span.appendText(chunck);
			span = null;
		} else {
			this.appendText(chunck);
		}
		return spanToReturn;
	}
}