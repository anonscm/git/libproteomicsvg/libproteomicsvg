/*******************************************************************************
 * Copyright (c) 2011-06-28 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of LibProteomicSvg.
 * 
 *     LibProteomicSvg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     LibProteomicSvg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with LibProteomicSvg.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 *     Benoit Valot < Benoit.Valot@moulon.inra.fr> - great help
 ******************************************************************************/

package fr.inra.moulon.xhtmlutils.xhtmlprotcover;

import fr.inra.moulon.xhtmlutils.XhtmlDocument;
import fr.inra.moulon.xhtmlutils.elements.XhtmlDiv;

public class XhtmlProtCover extends XhtmlDiv {
	protected XhtmlSequence xhtmlSequence;
	
	public XhtmlProtCover(XhtmlDocument document, String proteinSequence) throws Exception {
		super(document);
		
		xhtmlSequence = new XhtmlSequence(this, proteinSequence);
		// TODO Auto-generated constructor stub
	}
}